package thenotoriousrog.viddy.Backend;

import android.net.Uri;

/*
    This class is used to try to convert a regular file to a local path whenever possible.
    Not all files would be able to do this, however, most casues should allow for it.
 */
public class DocumentPathConverters {

    // replaces all of known percent encodings with the real character equivalent.
    private static String replacePercentEncodings(String str)
    {
        str = str.replace("%20", " ");
        str = str.replace("%22", "\"");
        str = str.replace("%25", "%");
        str = str.replace("%2D", "-");
        str = str.replace("%2E", ".");
        str = str.replace("%3C", "<");
        str = str.replace("%3E", ">");
        str = str.replace("%5E", "^");
        str = str.replace("%5C", "\\");
        str = str.replace("%5F", "_");
        str = str.replace("%60", "`");
        str = str.replace("%7B", "{");
        str = str.replace("%7C", "|");
        str = str.replace("%7D", "}");
        str = str.replace("%7E", "~");
        str = str.replace("%21", "!");
        str = str.replace("%23", "#");
        str = str.replace("%24", "$");
        str = str.replace("%26", "&");
        str = str.replace("%27", "'");
        str = str.replace("%28", "(");
        str = str.replace("%29", ")");
        str = str.replace("%2A", "*");
        str = str.replace("%2B", "+");
        str = str.replace("%2C", ",");
        str = str.replace("%2F", "/");
        str = str.replace("%3A", ":");
        str = str.replace("%3B", ";");
        str = str.replace("%3D", "=");
        str = str.replace("%3F", "?");
        str = str.replace("%40", "@");
        str = str.replace("%5B", "[");
        str = str.replace("%5D", "]");

        return str; // return the fixed string now!
    }

    /*
        Will attempt to convert a content Uri to a real file whenver possible
        Example file: content://com.android.externalstorage.documents/document/primary%3AVCutter%2Foh%20my%20god%20wow%20African%20guy%5BTrim%5D.mp4
    */
    public static String convertContentUri(String basePath, String uriPath)
    {
        String realPath = "";
        String[] splitter = uriPath.split("/"); // split on slashes. splitter[4] should hold the path that we want to convert to if its a content uri.

        for(int i = 0; i < splitter.length; i++)
        {
            System.out.println("splitter[" + i + "] = " + splitter[i]);
        }

        realPath = splitter[4]; // should be the path that we really want now.
        realPath = realPath.substring(10, realPath.length()); // remove the prefixed "primary%3A" from the path.
        realPath = basePath + "/" + realPath; // prefix the basePath with the real path
        realPath = replacePercentEncodings(realPath); // replace the encodings from the path.

        return realPath; // return the real path.
    }
}
