package thenotoriousrog.viddy.Backend;

/*
    Holds all the constants for various system level tasks that we need. Add to the constants as we need them.
 */
public class Constants {

    public interface INTENT {
        public static int PICK_FILE = 1000; // this tells the system that we want to open a file browser for the user to pick a file to use for their viddy.
        public static int GAIN_STORAGE_ACCESS = 2000; // tells the system that we are getting getting permissions for the user to select and edit files.
    }

    // actions for controlling the service.
    public interface ACTION {
        public static String STARTFOREGROUND_ACTION = "START_FOREGROUND_ACTION"; // tells the system to begin the foreground service.
        public static String STOPFOREGROUND_ACTION = "STOP_FOREGROUND_ACTION"; // tells the system to stop the foreground service.
        public static int REQUEST_PERMISSIONS = 1111; // asks the user for permissions.
        public static int STOP_VID_RIPPER_THREAD_SIGNAL = 2222; // sends the signal to the VidRipperService to stop the background thread.
        public static int STOP_BREAKING_NEWS_VID_RIPPER_THREAD_SIGNAL = 3333; // sends a signal to stop the BreakingNewsVidRipperService to stop the background thread.
    }

    // behavior for the handlers.
    public interface HANDLER {

        public static String VID_RIPPER_SERVICE_HANDLER = "VidRipperServiceHandler"; // tells the handler we are communicating with the vid ripper service handler.
        public static String BREAKING_NEWS_VID_RIPPER_SERVICE_HANDLER = "BreakingNewsVidRipperServiceHandler"; // tells the handler that we are communicating with the service.
        public static String UI_HANDLER = "UIHandler"; // tells the handler we are communicating with the UI handler.

    }

    // behavior for the UI.
    public interface UI {
        public static int UI_UPDATE_PROGRESS = 1000; // tells the UI to update the progress percentage.
        public static int UI_UPDATE_MESSAGE = 2000; // tells the UI to update the message relative to the progress percentage!
        public static int UI_FINISH_STEP3 = 3000; // tells the UI to finish and update the shared prefs for the main activity.
        public static int UI_SHOW_STEP3_FINISHED_DETAILS = 4000; // tells the UI to show the finished details of the step 3 activity.
        public static int UI_SHOW_VIDDY_STOPPED_MESSAGE = 5000; // tells the UI to show the error message if one exists for the user to see for whatever that may be.
    }

    public interface ACTIVITY {
        public static int NO_ACTIVITY = -1; // tells the system that the user is starting from the beginning. No activity is being set.
        public static int STEP2ACTIVITY = 1001; // tells the system where the user is currently at in the veme making progress.
        public static int STEP3ACTIVITY = 2002; // tells the system where the user is currently at in the veme making progress.
        public static int STEP4ACTIVITY = 3003; // I believe this should be correct ~ Steven
    }

    public interface GDPR_CONSENT {
        public static String NO_CONSENT = "NO_CONSENT"; // the user has chosen no consent for ads. We must always direct them to Viddy+
        public static String NON_PERSONAL_CONSENT = "NON_PERSONAL_CONSENT_ONLY"; // the user has chosen to only use non personalized ads.
        public static String AUTOMATIC_PERSONAL_CONSENT = "AUTOMATIC_PERSONAL_CONSENT"; // the user is not from the EU thus they can have the choice to select their own types of ads.
        public static String PERSONAL_CONSENT = "PERSONAL_CONSENT"; // the user has given their consent manually.
    }

}
