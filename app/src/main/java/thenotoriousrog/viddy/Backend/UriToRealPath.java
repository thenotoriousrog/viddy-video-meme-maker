package thenotoriousrog.viddy.Backend;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;

/* todo: THIS CLASS IS GOING TO BE REMOVED SO DO NOT USE IT.
    This entire class was found on this website: https://www.dev2qa.com/how-to-get-real-file-path-from-android-uri/
    And then modified for my use case.

    This class is a singleton class to be used statically from anywhere in the project.
 */
public class UriToRealPath {

    private static UriToRealPath instance = new UriToRealPath();
    public static UriToRealPath getInstance() {
        return instance;
    }


    // attempts to get the real path within android from the Uri.
    public String getRealPathFromURI(Context context, Uri contentUri)
    {
        Cursor cursor = null;
        String toReturn = ""; // the path that we need to return for the user.
        try
        {
            boolean doc = isDocumentUri(context, contentUri);
            if (doc)
            {
                String path = getDocPath(context, contentUri);

                System.out.println("PATH RETURNED FROM GETDOCPATH = " + path);

                return path;

            } else if (isContentUri(contentUri))
            { // todo: determine if there will need to be another check before the default else (isContentUri == true)

                if (isGooglePhotoDoc(contentUri.getAuthority())) {
                    System.out.println("GOOGLE PHOTO DOC FOUND!!");
                    toReturn = contentUri.getLastPathSegment();

                    InputStream input = context.getContentResolver().openInputStream(contentUri);
                    //byte[] bytes =

                    // create a new temp file to make use of the URI's
                    File root = new File(Environment.getExternalStorageDirectory(), "Viddy/.temp");
                    if (!root.exists()) {
                        root.mkdirs();
                    }


                    File chosenVideo = new File(root, "video");
                    FileOutputStream out = new FileOutputStream(chosenVideo);
                    //input.

                    return  toReturn;

                } else {
                    System.out.println("DEFAULTING TO CONTENT URI!!");
                    toReturn = getImageRealPath(context.getContentResolver(), contentUri, null);

                    return toReturn;

                }
            } else if (isFileUri(contentUri))
            {

            } else {

                boolean con = isContentUri(contentUri);
                System.out.println("IS URI DOCUMENT URI? " + doc);
                System.out.println("IS URI CONTENT URI? " + con);

                String documentId = DocumentsContract.getDocumentId(contentUri);
                // Get uri authority.
                String uriAuthority = contentUri.getAuthority();

                Uri externalContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                String data = MediaStore.Images.Media.DATA;

                System.out.println("externalContentUri: " + externalContentUri.toString());
                System.out.println("Media store data from Uri: " + data);
                System.out.println("documentId: " + documentId);
                System.out.println("uriAuthority: " + uriAuthority);

                String idArr[] = documentId.split(":");
                if (idArr.length == 2)
                {
                    // First item is document type.
                    String docType = idArr[0];

                    // Second item is document real id.
                    String realDocId = idArr[1];

                    // Get content uri by document type.
                    Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    if ("image".equals(docType)) {
                        mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(docType)) {
                        System.out.println("WE HAVE FOUND THE VIDEO BRANCH IN URI TO REAL PATH...");
                        mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(docType)) {
                        mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    System.out.println("Media content uri we are using: " + mediaContentUri);

                    // Get where clause with real document id.
                    String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;
                    toReturn = getImageRealPath(context.getContentResolver(), mediaContentUri, whereClause);

                    System.out.println("String we are returning: " + toReturn);

                    return toReturn;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found exception when trying to find real uri path");
            e.printStackTrace();

        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return toReturn;
    }

    /* Return uri represented document file real local path.*/
    private String getImageRealPath(ContentResolver contentResolver, Uri uri, String whereClause)
    {
        String ret = "";

        // Query the uri with condition.
        Cursor cursor = contentResolver.query(uri, null, whereClause, null, null);

        if(cursor!=null)
        {
            boolean moveToFirst = cursor.moveToFirst();
            if(moveToFirst)
            {

                // Get columns name by uri type.
                String columnName = MediaStore.Images.Media.DATA;

                if( uri==MediaStore.Images.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Images.Media.DATA;
                }else if( uri==MediaStore.Audio.Media.EXTERNAL_CONTENT_URI )
                {
                    columnName = MediaStore.Audio.Media.DATA;
                }else if( uri==MediaStore.Video.Media.EXTERNAL_CONTENT_URI )
                {
                    System.out.println("WE HAVE FOUND A VIDEO CONTENT URI");
                    columnName = MediaStore.Video.Media.DATA;
                }

                // Get column index.
                int imageColumnIndex = cursor.getColumnIndex(columnName);

                // Get column value which is the uri related file local path.


                String[] c = cursor.getColumnNames();
                for(int i = 0; i < c.length; i++)
                {
                    System.out.println("columnames = " + c[i]);
                }

                System.out.println("TEST");
                ret = cursor.getString(imageColumnIndex); // fixMe: this is super bad. Manually setting this here will not by any means be a quick completion of this problem! Must fix!
            }
        }

        return ret;
    }

    /* Check whether this uri is a file uri or not.
     *  file uri like file:///storage/41B7-12F1/DCIM/Camera/IMG_20180211_095139.jpg
     * */
    private boolean isFileUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("file".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this document is provided by google photos. */
    private boolean isGooglePhotoDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.google.android.apps.photos.contentprovider".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    private String getDocPath(Context ctx, Uri uri)
    {
        String ret = "";
        System.out.println("IS DOCUMENT URI...");
        // Get uri related document id.
        String documentId = DocumentsContract.getDocumentId(uri);

        System.out.println("The documentId = " + documentId);

        // Get uri authority.
        String uriAuthority = uri.getAuthority();
        System.out.println("URI authority = " + uriAuthority);

        if(isMediaDoc(uriAuthority))
        {
            System.out.println("IS MEDIA DOC!");
            String idArr[] = documentId.split(":");
            if(idArr.length == 2)
            {
                // First item is document type.
                String docType = idArr[0];

                // Second item is document real id.
                String realDocId = idArr[1];

                // Get content uri by document type.
                Uri mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                if("image".equals(docType))
                {
                    mediaContentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }else if("video".equals(docType))
                {
                    System.out.println("WE HAVE FOUND THE VIDEO BRANCH IN URI TO REAL PATH...");
                    mediaContentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                }else if("audio".equals(docType))
                {
                    mediaContentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                // Get where clause with real document id.
                String whereClause = MediaStore.Images.Media._ID + " = " + realDocId;

                ret = getImageRealPath(ctx.getContentResolver(), mediaContentUri, whereClause);
            }
        }
        else if (isDownloadDoc(uriAuthority))
        {
            System.out.println("ITS A DOWNLOAD DOC");
            // Build download uri.
            Uri downloadUri = Uri.parse("content://downloads/public_downloads");

            // Append download document id at uri end.
            Uri downloadUriAppendId = ContentUris.withAppendedId(downloadUri, Long.valueOf(documentId));

            ret = getImageRealPath(ctx.getContentResolver(), downloadUriAppendId, null);
        }
        else if(isExternalStoreDoc(uriAuthority))
        {
            String idArr[] = documentId.split(":");
            if(idArr.length == 2)
            {
                String type = idArr[0];
                String realDocId = idArr[1];

                if("primary".equalsIgnoreCase(type))
                {
                    ret = Environment.getExternalStorageDirectory() + "/" + realDocId;
                }
            }
        }

        return ret;
    }

    /* Check whether this document is provided by MediaProvider. */
    private boolean isMediaDoc(String uriAuthority)
    {
        boolean ret = false;

        System.out.println("URI AUTHORITY FOR URI: " + uriAuthority);

        if("com.android.providers.media.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this uri is a content uri or not.
     *  content uri like content://media/external/images/media/1302716
     *  */
    private boolean isContentUri(Uri uri)
    {
        boolean ret = false;
        if(uri != null) {
            String uriSchema = uri.getScheme();
            if("content".equalsIgnoreCase(uriSchema))
            {
                ret = true;
            }
        }
        return ret;
    }

    /* Check whether this document is provided by ExternalStorageProvider. */
    private boolean isExternalStoreDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.externalstorage.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this document is provided by DownloadsProvider. */
    private boolean isDownloadDoc(String uriAuthority)
    {
        boolean ret = false;

        if("com.android.providers.downloads.documents".equals(uriAuthority))
        {
            ret = true;
        }

        return ret;
    }

    /* Check whether this uri represent a document or not. */
    private boolean isDocumentUri(Context ctx, Uri uri)
    {
        boolean ret = false;
        if(ctx != null && uri != null) {
            ret = DocumentsContract.isDocumentUri(ctx, uri);
        }
        return ret;
    }


}
