package thenotoriousrog.viddy.Backend;

import android.content.Context;
import android.support.v4.content.ContextCompat;

import java.io.File;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsViddyActivity;
import thenotoriousrog.viddy.ViddyMeme.ViddyStep3Fragment;

// A quick library to maintain the state of Viddy.
public class ViddyUtils {

    private boolean step3Running = false; // tells the system that step 3 is already running and thus we should not run the background thread again.
    private String step3ProgressText = null; // holds the text that the progress is currently showing.
    private String step3ProgressMessageText = null; // holds the text that the user is currently being displayed on the app itself.
    private File createdViddy = null; // holds the viddy that the user has created for this instance.
    private File createdBreakingNewsViddy = null; // holds the breaking news viddy.
    private boolean finishedCreatingViddy = false; // tell us that the viddy has been created
    private boolean finishedCreatingBreakingNewsViddy = false; // tells use that the breaking new viddy has been created.
    private boolean breakingNewsViddyStep3Running = false; // tells us if the breaking news viddy is still running or not.
    private MainActivity mainActivity; // holds a copy of the main activity.
    private boolean inValidVideoFormat = false;
    private ViddyStep3Fragment viddyStep3Fragment; // the copy of the current step 3 fragment that we are working with.
    private BreakingNewsViddyActivity breakingNewsViddyActivity; // holds a copy of the breaking news viddy activity to ensure that step 3 is shown when the user is making a breaking news viddy.

    private static ViddyUtils instance = new ViddyUtils();

    public static ViddyUtils getInstance() {
        return instance;
    }

    // resets all values of ViddyUtils and creates a brand new instance.
    public void restartViddyUtils() {
        instance = new ViddyUtils();
    }

    // changes the state of step 3
    public void changeStep3State(boolean state) {
        step3Running = state;
    }

    public boolean isStep3Running()
    {
        return step3Running;
    }

    public void updateStep3ProgressText(String progress) {
        step3ProgressText = progress;
    }

    public void updateStep3ProgressMessageText(String message){
        step3ProgressMessageText = message;
    }

    public String getStep3ProgressText() {
        return step3ProgressText;
    }

    public String getStep3ProgressMessageText() {
        return step3ProgressMessageText;
    }

    // gets color using ContextCompat
    public int getColor(Context context, int color)
    {
        return ContextCompat.getColor(context, color);
    }

    // sets the viddy that the user has created.
    public void setCreatedViddy(File viddy) {
        this.createdViddy = viddy;
    }

    public void setCreatedBreakingNewsViddy(File breakingNewsViddy) {
        this.createdBreakingNewsViddy = breakingNewsViddy;
    }


    // returns the viddy the user has created.
    public File getCreatedViddy() {
        return createdViddy;
    }

    public File getCreatedBreakingNewsViddy() {
        return createdBreakingNewsViddy;
    }

    public void setFinishedCreatingViddyStatus(boolean status) {
        finishedCreatingViddy = status;
    }

    // tells the system if the viddy is finished being created or not.
    public boolean isFinishedCreatingViddy() {
        return finishedCreatingViddy;
    }

    public void setViddyStep3Fragment(ViddyStep3Fragment viddyStep3Fragment) {
        this.viddyStep3Fragment = viddyStep3Fragment;
    }

    public ViddyStep3Fragment getViddyStep3Fragment() {
        return viddyStep3Fragment;
    }

    public void setMainActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public MainActivity getMainActivity() {
        return this.mainActivity;
    }

    public void setBreakingNewsViddyActivity(BreakingNewsViddyActivity breakingNewsViddyActivity) {
        this.breakingNewsViddyActivity = breakingNewsViddyActivity;
    }

    public BreakingNewsViddyActivity getBreakingNewsViddyActivity() {
        return breakingNewsViddyActivity;
    }

    public void setFinishedCreatingBreakingNewsViddyStatus(boolean status) {
        finishedCreatingBreakingNewsViddy = status;
    }

    public boolean isFinishedCreatingBreakingNewsViddy() {
        return finishedCreatingBreakingNewsViddy;
    }

    public void changeBreakingNewsViddyRunningStatus(boolean status) {
        breakingNewsViddyStep3Running = status;
    }

    public boolean isBreakingNewsViddyStep3Running() {
        return breakingNewsViddyStep3Running;
    }
}
