package thenotoriousrog.viddy.BreakingNewsViddy.Handlers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsViddyStep3Fragment;

public class BreakingNewsUIHandler extends Handler {

    private BreakingNewsViddyStep3Fragment breakingNewsViddyStep3Fragment; // the fragment where we will be sending updates too.
    private static BreakingNewsUIHandler instance = null;

    public BreakingNewsUIHandler(BreakingNewsViddyStep3Fragment breakingNewsViddyStep3Fragment, Looper looper)
    {
        super(looper);
        this.breakingNewsViddyStep3Fragment = breakingNewsViddyStep3Fragment;
        instance = this;
    }

    public static BreakingNewsUIHandler getInstance() {
        return instance;
    }

    // updates the UI progress counter in the UIHandler.
    private void updateUIProgressCounter(Message msg)
    {
        String progress = msg.getData().getString("Progress");
        breakingNewsViddyStep3Fragment.setProgressCounterText(progress); // set the progress text.
    }

    private void updateUIProgressMessage(Message msg)
    {
        String message = msg.getData().getString("Message");
        breakingNewsViddyStep3Fragment.setProgressMessage(message);
    }

    // updates the UI causing the next activity to be shown!
    private void finishStep3()
    {
        breakingNewsViddyStep3Fragment.finishStep3();
    }

    // makes the step3Activity show the finished details.
    private void showFinishedDetails() {
        breakingNewsViddyStep3Fragment.showFinishedDetails();
    }

    // tells the step 3 activity that we have stopped their viddy and handles it accordingly.
    private void showStoppedDetails(Message msg) {
        String message = msg.getData().getString("ErrorMessage");
        breakingNewsViddyStep3Fragment.showStoppedDetails(message); // tells the step3Activity to show the stopped behavior instead of the finished behavior.
    }

    // All messages will be in here.
    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.arg1)
        {
            case Constants.UI.UI_UPDATE_PROGRESS:
                updateUIProgressCounter(msg);
                break;

            case Constants.UI.UI_UPDATE_MESSAGE:
                updateUIProgressMessage(msg);
                break;

            case Constants.UI.UI_FINISH_STEP3:
                finishStep3();
                break;

            case Constants.UI.UI_SHOW_STEP3_FINISHED_DETAILS:
                showFinishedDetails();
                break;

            case Constants.UI.UI_SHOW_VIDDY_STOPPED_MESSAGE:
                showStoppedDetails(msg);
                break;

        }
    }

}
