package thenotoriousrog.viddy.BreakingNewsViddy.Handlers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsVidRipperService;

public class BreakingNewsViddyVidRipperHandler extends Handler {

    private BreakingNewsVidRipperService breakingNewsVidRipperService;
    private static BreakingNewsViddyVidRipperHandler instance = null;

    public BreakingNewsViddyVidRipperHandler(BreakingNewsVidRipperService breakingNewsVidRipperService, Looper looper) {
        super(looper); // sends the looper to Handler superclass.
        this.breakingNewsVidRipperService = breakingNewsVidRipperService;
        instance = this;
    }

    public static BreakingNewsViddyVidRipperHandler getInstance() {
        return instance;
    }

    // sends the signal to stop the background thread in the viddy service.
    private void sendStopBackgroundThreadSignal() {
        System.out.println("SENDING THE STOP SIGNAL TO THE BNV BACKGROUND THREAD NOW!");
        breakingNewsVidRipperService.sendStopVidRipperBackgroundThreadSignal();
    }

    // Handles all of the messages from the messages that need to be sent to the vid ripper service.
    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.arg1)
        {
            case Constants.ACTION.STOP_BREAKING_NEWS_VID_RIPPER_THREAD_SIGNAL:
                sendStopBackgroundThreadSignal();
                break;
        }
    }

}
