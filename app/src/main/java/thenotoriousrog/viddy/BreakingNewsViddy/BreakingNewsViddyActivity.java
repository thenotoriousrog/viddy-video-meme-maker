package thenotoriousrog.viddy.BreakingNewsViddy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.michaelflisar.gdprdialog.GDPR;
import com.michaelflisar.gdprdialog.GDPRConsent;
import com.michaelflisar.gdprdialog.GDPRConsentState;
import com.michaelflisar.gdprdialog.GDPRDefinitions;
import com.michaelflisar.gdprdialog.GDPRSetup;
import com.michaelflisar.gdprdialog.helper.GDPRPreperationData;

import java.io.File;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Activities.SettingsActivity;
import thenotoriousrog.viddy.Activities.StartScreenActivity;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.RealPathUtil;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.BreakingNewsMeme.BreakingNewsMemeActivity;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeActivity;
import thenotoriousrog.viddy.QuoteMeme.QuoteMemeActivity;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.StandardMeme.StandardMemeActivity;
import thenotoriousrog.viddy.ViddyMeme.TrimVideoFragment;

public class BreakingNewsViddyActivity extends AppCompatActivity implements GDPR.IGDPRCallback {

    private VideoView videoView; // the video view that the user will see.
    private String videoPath = null; // holds the Uri of the video that the user has selected.
    private String realPath = null; // holds the real system level path for us to be able to find the actual video.
    private SharedPreferences preferences; // get the context that we need in order to sort these lists.
    private GDPRSetup gdprSetup; // the gdpr setup we are using for the overall system.
    private boolean finishedStartScreen = false; // tells the main activity that the user has finished the start screen.
    private AdView generalAdView; // the general ad for the Ad view.
    private ProgressBar progressBar; // the progress bar that we are going to be using
    private RelativeLayout mainLayout; // the main layout for the MainUIActivity.
    private DrawerLayout mainDrawerLayout; // the main drawer layout which everything is contained in.
    private boolean isDownloadedVideo = false; // tells the activity that the user has chosen to use a link. If they have we need to delete the video after they have used it.
    private boolean isDrawerOpen = false; // tells us if the drawer is open or not.
    private FragmentTransaction mainFragmentTransaction; // the fragment transaction that we will be using.
    private FrameLayout mainFrameLayout; // the frame layout that we will swap out fragment throughout the application.
    private LinearLayout fragmentIndicator; // this shows the steps on the stepper on the stop of the app.
    private BreakingNewsViddyStep1Fragment breakingNewsViddyStep1Fragment;
    private BreakingNewsViddyStep2Fragment breakingNewsViddyStep2Fragment;
    private Fragment currentFragment; // the current fragment that is open.
    private boolean canGoBack = false; // true when we can go back to a previous fragment, false when going back isn't possible.
    private NavigationView navigationView; // the navigation view used throughout the application.
    private boolean showEditClipNameButton = false; // tells the system to show the edit clip name button.
    private boolean showEditFileNameButton = false; // tells the system to show the edit button name very important!
    private TrimVideoFragment trimVideoFragment;


    // sets up the GDPR instance to be shown in a little bit.
    private void setupGDPR()
    {
        gdprSetup = new GDPRSetup(GDPRDefinitions.ADMOB)
                .withAllowNoConsent(true) // required by law. This should be here, people have the right to say no
                .withPaidVersion(true)
                .withExplicitAgeConfirmation(false) // if the user accepts they are agreeing to be of the age of 16
                .withCheckRequestLocation(true)
                .withCheckRequestLocationFallbacks(true, true)
                .withCheckRequestLocationTimeouts(3000, 3000) // fail after 3 seconds each time.
                .withBottomSheet(false)
                .withForceSelection(true)
                .withShortQuestion(true)
                .withLoadAdMobNetworks("pub-1014366431079942") // publisher ID for Viddy
                .withNoToolbarTheme(true);
    }

    // creates the fragment indicator on the top of the viddy creation.
    public void notifyFragmentIndicator(int itemSelected) {

        if (fragmentIndicator.getChildCount() > 0) {
            fragmentIndicator.removeAllViews();
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMarginEnd(15);
        // loop through creating the text views and setting the text. If the item is selected it shows a filled version of the background instead.
        for(int i = 0; i < 4; i++)
        {

            TextView textView = new TextView(this);
            textView.setText(Integer.toString(i+1));
            textView.setGravity(Gravity.CENTER);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
            if (i == itemSelected) {
                textView.setBackgroundResource(R.drawable.viddy_fragment_indicator_selected);
                textView.setTextColor(ContextCompat.getColor(this, R.color.white)); // make the text white.
            } else {
                textView.setBackgroundResource(R.drawable.viddy_fragment_indicator_unselected);
                textView.setTextColor(ContextCompat.getColor(this, R.color.black)); // make the text white.
            }

            fragmentIndicator.addView(textView, layoutParams);
        }

    }

    // builds and loads the ads.
    public void setupGeneralAd(boolean usePersonalizedAds)
    {
        if (!usePersonalizedAds) // if we are not using personalized ads i.e. non pesonal ads only, we must report this to google.
        {
            Bundle extras = new Bundle();
            extras.putString("npa", "1");

            // build request with the report to admob that they must not be personalized.
            AdRequest request = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();

            // find, build, and load general ad.
            generalAdView = findViewById(R.id.generalAd);
            generalAdView.loadAd(request);

        } else { // create ads like normal; we can use personalized ads.

            AdRequest request = new AdRequest.Builder().build();

            // find, build, and load general ad.
            generalAdView = findViewById(R.id.generalAd);
            generalAdView.loadAd(request);
        }
    }

    // sets the boolean to false so that the icon is hidden the next we start the list.
    public void hideEditFilenameIcon() {
        showEditFileNameButton = false;
    }


    // makes the general ad invisible and not clickable or focusable..
    public void disableGeneralAd() {
        generalAdView.setVisibility(View.INVISIBLE);
        generalAdView.setClickable(false);
        generalAdView.setFocusable(false);
    }

    // makes general ad visible and clickable and focusable.
    public void enableGeneralAd() {
        generalAdView.setVisibility(View.VISIBLE);
        generalAdView.setClickable(true);
        generalAdView.setFocusable(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mainDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_edit_trimmed_vid_name:
                if(trimVideoFragment.preventTrimFileName()) {
                    Snackbar snackbar = Snackbar.make(mainDrawerLayout, R.string.NotTrimmedWarning, Snackbar.LENGTH_SHORT);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(this, R.color.StandardMemeColorComplimentary));
                    sbView.setElevation(5);
                    snackbar.show();
                } else {
                    showEditClipDialog();
                }
                return true;

            case R.id.action_edit_file_name:
                showEditFileNameDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // inflates the menu and adds items in the menu upon starting the creation of the app.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        if(!showEditClipNameButton) { // if not showing the share icon, hide it!
            MenuItem item = menu.findItem(R.id.action_edit_trimmed_vid_name);
            item.setVisible(false); // no longer make this item visible.
        }

        if(!showEditFileNameButton) {
            MenuItem item = menu.findItem(R.id.action_edit_file_name);
            item.setVisible(false);
        }

        MenuItem item = menu.findItem(R.id.action_delete);
        item.setVisible(false); // hide delete since standard meme can't do anything with it!

        item = menu.findItem(R.id.action_share);
        item.setVisible(false); // hide the share button.

        return true;
    }

    private void showEditClipDialog() {

        TextInputEditText editText = new TextInputEditText(this);
        editText.setPadding(10,5,5,5);
        editText.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        editText.setTextColor(ContextCompat.getColor(this, R.color.black));
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);


        String videoClipName = trimVideoFragment.getVideoClipName();
        if(videoClipName == null || videoClipName.isEmpty()) {
            editText.setHint("Clip currently named " + "\"" +  "VideoClip" + "\"" + "...");
        } else  {
            videoClipName = "Clip currently named " + "\"" + videoClipName + "\"" + "...";
            editText.setHint(videoClipName);
        }

        new MaterialDialog.Builder(this)
                .title(R.string.ChangeVideoClipNameTitle)
                .customView(editText, false)
                .positiveText(R.string.Confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss(); // close the dialog.
                        trimVideoFragment.setVideoClipName(editText.getText().toString()); // set the video clip name.
                    }
                })
                .negativeText(R.string.Cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss(); // simply close the dialog if cancel is pressed.
                    }
                })
                .show();

    }

    private void showEditFileNameDialog() {

        TextInputEditText editText = new TextInputEditText(this);
        editText.setPadding(10,5,5,5);
        editText.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        editText.setTextColor(ContextCompat.getColor(this, R.color.black));
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        new MaterialDialog.Builder(this)
                .title(R.string.ChangeViddyFileName)
                .customView(editText, false)
                .positiveText(R.string.Confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss(); // close the dialog.
                        breakingNewsViddyStep2Fragment.setFilename(editText.getText().toString());
                    }
                })
                .negativeText(R.string.Cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss(); // simply close the dialog if cancel is pressed.
                    }
                })
                .show();
    }

    // sets up the drawer layout to be used throughout the rest of the app.
    public void setupDrawerLayout()
    {
        navigationView = findViewById(R.id.nav_view);
        navigationView.bringToFront();
        navigationView.setCheckedItem(R.id.CreateBreakingNewsViddy); // create viddy is initially selected.
       // navigationView.getHeaderView(0).setBackgroundColor(ContextCompat.getColor(this, R.color.BreakingNewsViddyColorPrimary));
        RelativeLayout navLayout = (RelativeLayout) navigationView.getHeaderView(0); // convert to a linear layout.
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        menuItem.setChecked(false); // set item as selected to persist highlight
                        mainDrawerLayout.closeDrawers(); // close drawer when item is tapped

                        if (menuItem.getTitle().toString().equalsIgnoreCase("Settings")) {

                            Intent settingsIntent = new Intent(BreakingNewsViddyActivity.this, SettingsActivity.class);
                            startActivity(settingsIntent);
                            menuItem.setChecked(true);
                        }
                        else if (menuItem.getTitle().toString().equalsIgnoreCase("Viddy")) {
                            //setTheme(R.style.AppTheme_StandardMeme); // set the new theme for the application.
                            Intent mainActivityIntent = new Intent(BreakingNewsViddyActivity.this, MainActivity.class);
                            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // removes all other activities in the back stack.
                            startActivity(mainActivityIntent); // show the layout for the original meme format!
                            menuItem.setChecked(true);
                            finish(); // kill the activity so that it does not exist below the other activities.
                        }
                        else if (menuItem.getTitle().toString().equalsIgnoreCase("Standard Meme")) {
                            //setTheme(R.style.AppTheme_StandardMeme); // set the new theme for the application.
                            Intent standardMemeIntent = new Intent(BreakingNewsViddyActivity.this, StandardMemeActivity.class);
                            standardMemeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // removes all other activities in the back stack.
                            startActivity(standardMemeIntent); // show the layout for the original meme format!
                            menuItem.setChecked(true);
                            finish(); // kill the activity so that it does not exist below the other activities.
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Floating Text Meme")) {
                            Intent floatingTextMemeIntent = new Intent(BreakingNewsViddyActivity.this, FloatingTextMemeActivity.class);
                            floatingTextMemeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(floatingTextMemeIntent);
                            menuItem.setChecked(true);
                            finish();
                        }
                        else if (menuItem.getTitle().toString().equalsIgnoreCase("Breaking News Viddy")) {
                            // do nothing, we are already in this fragment.
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Quote Meme")) {
                            Intent quoteMemeIntent = new Intent(BreakingNewsViddyActivity.this, QuoteMemeActivity.class);
                            quoteMemeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(quoteMemeIntent);
                            menuItem.setChecked(true);
                            finish();
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Breaking News Meme")) {
                            Intent breakingNewsMemeIntent = new Intent(BreakingNewsViddyActivity.this, BreakingNewsMemeActivity.class);
                            breakingNewsMemeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(breakingNewsMemeIntent);
                            menuItem.setChecked(true);
                            finish();
                        }

                        return true;
                    }
                });

        mainDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) { }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) { // change the state of the drawer being opened.
                isDrawerOpen = true;
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                isDrawerOpen = false;
            }

            @Override
            public void onDrawerStateChanged(int newState) { }
        });
    }

    // displays the second viddy fragment.
    public void displayViddyStep2Fragment(File trimmedVideo) {
        breakingNewsViddyStep2Fragment = new BreakingNewsViddyStep2Fragment();

        // if the trimmed video is not null set the new videopath and realpath for us to use in step2
        if(trimmedVideo != null) {
            videoPath = Uri.fromFile(trimmedVideo).toString();
            realPath = trimmedVideo.getAbsolutePath();
        }

        breakingNewsViddyStep2Fragment.setFields(this, videoPath, realPath);
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mainFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        mainFragmentTransaction.replace(R.id.mainFrameLayout, breakingNewsViddyStep2Fragment);
        mainFragmentTransaction.addToBackStack(null);
        mainFragmentTransaction.commit();
        currentFragment = breakingNewsViddyStep2Fragment;
        canGoBack = false;
        showEditFileNameButton = true; // we can edit the file name but not the clip name.
        showEditClipNameButton = false; // can no longer change the clip name.
        invalidateOptionsMenu(); // force the options menu to redraw showing the icons that user can interact with.
    }

    public void displayTrimVideoFragment() {
        trimVideoFragment = new TrimVideoFragment();
        trimVideoFragment.setFields(this, realPath, videoPath); // set the animations for the trimVideoFragment.
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mainFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        mainFragmentTransaction.replace(R.id.mainFrameLayout, trimVideoFragment);
        mainFragmentTransaction.addToBackStack(null);
        mainFragmentTransaction.commit();
        currentFragment = trimVideoFragment;
        showEditClipNameButton = true;
        showEditFileNameButton = false; // not changing the file name here.
        canGoBack = true;
        invalidateOptionsMenu(); // force the options menu to redraw showing more actions
    }

    // All UI elements that are used in main activity should be grabbed and setup here in onCreate!
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setTheme(R.style.AppTheme_BreakingNewsViddy); // set the theme for this activity.
        super.onCreate(savedInstanceState);

        GDPR.getInstance().init(this);

        // This is our AdMob ID. This is needed to display ads throughout the application.
        MobileAds.initialize(this, "ca-app-pub-1014366431079942~9743624356"); // our real admob id.

        ViddyUtils.getInstance().setCreatedViddy(null); // set null to ensure that the viddy utils gets the correct state for this viddy instance.
        ViddyUtils.getInstance().setBreakingNewsViddyActivity(this); // needed to ensure that the app opens directly to step 3 for the breaking news viddy.

        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); // get the context that we need in order to sort these lists.
        //SharedPreferences.Editor editor = preferences.edit(); // editor to edit the shared preferences.

        // todo: check to see if this is the first use of the app and then display the startup layout if this is the case.
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); // get the context that we need in order to sort these lists.
        int currentActivity = preferences.getInt("CurrentActivity", Constants.ACTIVITY.NO_ACTIVITY); // default activity is no activity i.e. starting with the main activity.

        System.out.println("Is step 3 activity running?? " + ViddyUtils.getInstance().isStep3Running());
        // System.out.println("The number of ad technologies: " + consentInformation.getAdProviders().size());

        setContentView(R.layout.breaking_news_viddy_activity); // this is the layout for creating a new Viddy.
        // No other activity in progress, we may continue on.

        mainDrawerLayout = findViewById(R.id.mainDrawerLayout);
        mainFrameLayout = findViewById(R.id.mainFrameLayout);
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();  // the fragment transaction that we are using throughout the rest of the application.
        fragmentIndicator = findViewById(R.id.fragmentsIndicatorLayout);
        notifyFragmentIndicator(0); // makes the first item selected.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("Breaking News Viddy");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        setupGDPR();
        setupGeneralAd(false); // initally setup the general to show non personal ads so that no data is collected during start up.
        disableGeneralAd(); // disable the general ad so that the user does not see ads by accident until after they have chosen their ad preferences.
        setupDrawerLayout(); // sets up the drawer layout to handle the other UI behavior.


        finishedStartScreen = preferences.getBoolean("FinishedStartScreen", false);
        if(!finishedStartScreen) // if not shown start screen restart the activity. This is where permissions will be granted and set the initial grant of the permissions for the user
        {
            Intent startScreenIntent = new Intent(this, StartScreenActivity.class);
            startActivity(startScreenIntent);

        } else { // the start screen has been shown. We need to check if their has been a change to their preferences.

            String userConsent = preferences.getString("Consent", null);

            // fixme: the user should be immediately taken to viddy+ and not show any ads.
            if (userConsent != null  && userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT) ) // if not null and no consent, we need to direct the user to Viddy+
            {
                setupGeneralAd(false); // setup the ad to not show personal ads to prevent any data collection.
                disableGeneralAd(); // disable the general ad so that the user cannot click on it and it's not visible.
                Toast.makeText(this, "Viddy+ is under developement, when available, we will take you immediately to it when you open the app!", Toast.LENGTH_SHORT).show();
                finish(); // kill the activity
            }
            else if (userConsent != null && userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // ads are granted, but non personal only.
                setupGeneralAd(false); // ensure that all ads that are created are now personalized.
                enableGeneralAd(); // make the ad visible and clickable now.
            }
            // user has agreed to show personal ads, or they are not from the EU thus personal ads are automatically granted.
            else if (userConsent != null && (userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT) || userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT))) {
                setupGeneralAd(true); // setup the general ad to show personalized ads.
                enableGeneralAd(); // make the ad visible and clickable.
            }

            GDPR.getInstance().checkIfNeedsToBeShown(this, gdprSetup); // check if we have to show the GDPR again for the user or not.
        }

        if(ViddyUtils.getInstance().isBreakingNewsViddyStep3Running()) // if step 3 is running and the step 3 viddy instance is not null.
        {
            // TODO: fix this to handle switching to the step3fragment when this is true. As of right now it doesn't work.
            System.out.println("RESTARTING STEP 3 ACTIVITY");

            String savedVidPath = preferences.getString("VideoPath", null);
            String savedVemeText = preferences.getString("VemeText", null);
            boolean withWatermark = preferences.getBoolean("WithWatermark", true); // default is true.
            int savedTextSize = preferences.getInt("TextSize", 30); // default value is 30sp

            Bundle args = new Bundle();
            args.putString("VideoPath", savedVidPath); // the path of the video that the user has selected.
            args.putString("RealPath", realPath);
            args.putString("VemeText", savedVemeText); // put the meme text that the user has written.
            args.putBoolean("WithWatermark",withWatermark); // make the meme with the watermark, default value is true.
            args.putFloat("TextSize", savedTextSize);

            BreakingNewsViddyStep3Fragment breakingNewsViddyStep3Fragment = new BreakingNewsViddyStep3Fragment();
            breakingNewsViddyStep3Fragment.setFields(this, savedVidPath, realPath, "","",  "",
                    "",  withWatermark, "BNVViddy");
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.mainFrameLayout, breakingNewsViddyStep3Fragment); // use the instance of the step3fragment to display information.
            transaction.commit();
            currentFragment = breakingNewsViddyStep3Fragment;
            canGoBack = false;
            notifyFragmentIndicator(2); // highlight the last indicator on
        }
        else
        {
            ViddyUtils.getInstance().restartViddyUtils(); // restarts with clean slate.
            ViddyUtils.getInstance().setCreatedBreakingNewsViddy(null); // set null to ensure that the viddy utils gets the correct state for this viddy instance.
            ViddyUtils.getInstance().setBreakingNewsViddyActivity(this); // set a copy of the main activity to be used
            breakingNewsViddyStep1Fragment = new BreakingNewsViddyStep1Fragment();
            breakingNewsViddyStep1Fragment.setFields(this); // set the main activity for the viddy step1 fragment.
            mainFragmentTransaction.addToBackStack("BreakingNewsViddyStep1Fragment");
            mainFragmentTransaction.replace(R.id.mainFrameLayout, breakingNewsViddyStep1Fragment);
            mainFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
            mainFragmentTransaction.commit(); // commit the changes to show the very first fragment immediately.
            currentFragment = breakingNewsViddyStep1Fragment;
            canGoBack = false;
        }
    }

    // Checks the extension of the video format and determines if it's a valid format that can be played with video view.
    // Returns true if it is. Returns false if not.
    private boolean isSupportedContent(String realPath) {

        String ext = realPath.substring(realPath.length()-4, realPath.length()); // check for the standard 3 letter extension.

        System.out.println("The 3 letter extension we're looking at: " + ext);

        // if any of these return true
        if(ext.equalsIgnoreCase(".3gp") || ext.equalsIgnoreCase(".mp4") || ext.equalsIgnoreCase(".mkv") || ext.equalsIgnoreCase(".bin")) {
            return true; // return true we have truly real content that we can use!
        }

        ext = realPath.substring(realPath.length()-5, realPath.length()); // check for non-standard 4 letter valid extensions.

        System.out.println("The 4 letter extension we're looking at: " + ext);

        // .webm is a valid format created by Google. Thus, return true.
        if(ext.equalsIgnoreCase(".webm")) {
            return true;
        }

        return false; // if we have reached here then we have not returned found a real format and thus we can return false.
    }

    // when the user returns from picking a veme we can begin displaying our video!
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentResult)
    {
        if(intentResult == null) {
            return; // immediately leave this method, the user did not select a file to use.
        }

        if(requestCode == Constants.INTENT.PICK_FILE) // the user has selected a file.
        {
            final Uri videoUri = intentResult.getData(); // get the video uri that the user has chosen.
            videoPath = videoUri.toString(); // set the video Uri that the user has chosen to make use of.
            isDownloadedVideo = false; // make false so that the video is not deleted after the viddy is created.

            System.out.println("Video uri path = " + videoPath);


            try {
                realPath = RealPathUtil.getRealPath(this, videoUri); //UriToRealPath.getInstance().getRealPathFromURI(this, videoUri); // get the real true system level path...
            } catch(NumberFormatException nfe) { // this occurs when we get something super weird.
                nfe.printStackTrace();
                realPath = null; // note: writing this to be null as it will be caught later
            }

            if(realPath != null && isSupportedContent(realPath)) { // if realPath == null, then some exception was caught while trying to convert to real path. Mark as error.
                System.out.println("WE HAVE FOUND CONTENT THAT IS SUPPORTED FOR VIDEO VIEW!");

                SharedPreferences.Editor editor =  preferences.edit(); // create an editor to make changes to what is saved to main memory.
                editor.putString("VideoPath", videoPath); // send in the string
                editor.putString("RealPath", realPath);
                editor.commit();

                System.out.println("RIGHT BEFORE THE VIDEO IS SELECTED");
                System.out.println("THIS IS THE VIDEO PATH: "+videoPath);
                breakingNewsViddyStep1Fragment.setVideoUri(videoUri);
                breakingNewsViddyStep1Fragment.setVideoPath(videoUri.toString());
                breakingNewsViddyStep1Fragment.startVideoView(); // starts the video view in the fragment

            } else {
                System.out.println("WE HAVE NOT FOUND CONTENT THAT IS SUPPORTED FOR VIDEO VIEW");

                // reset the values so that when the user presses the wrong the thing it won't allow them to go forward.
                realPath = "";
                videoPath = "";

                Snackbar snackbar = Snackbar.make(mainDrawerLayout, R.string.UnsupportedVideoContentWarning, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(this, R.color.BreakingNewsViddyColorComplimentary));
                snackbar.show();
            }


        }
    }

    @Override
    public void onConsentNeedsToBeRequested(GDPRPreperationData gdprPreperationData) {
        // we need to get consent, so we show the dialog here
        GDPR.getInstance().showDialog(this, gdprSetup, gdprPreperationData.getLocation());
    }

    @Override
    public void onConsentInfoUpdate(GDPRConsentState gdprConsentState, boolean isNewState) {

        System.out.println("CONSENT WAS CHANGED IN MAIN ACTIVITY!! isNewState? " + isNewState);

        System.out.println("GDPR consent that we have retrieved = " + gdprConsentState.logString());

        SharedPreferences.Editor editor = preferences.edit();

        if (isNewState && finishedStartScreen) // if in a new state and the start screen has been shown, we need to then update the user's information.
        {
            GDPRConsent consent = gdprConsentState.getConsent();
            if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT)) { // The user has chosen to get the paid version of the app instead.
                // todo: add a link to send the user to Viddy+
                Toast.makeText(this, "Viddy+ is under development and will be available soon!", Toast.LENGTH_SHORT).show();
                System.out.println("USER GAVE NO CONSENT.");

                editor.putString("Consent", Constants.GDPR_CONSENT.NO_CONSENT);
                editor.commit();
                finish(); // close the main activity, we cannot show any ads at all thus we need to redirect the user to viddy+ and thus this app is no longer functional.

                // TODO: handle this. If the user chooses this, we need to then close the app and prevent the user from opening it again unless they buy viddy+, maybe direct them to the website.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // The user has agreed to ads, but not the good kind.

                editor.putString("Consent", Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT);
                editor.commit();

                setupGeneralAd(false); // setup general ad without showing personal consent, very important.
                enableGeneralAd();

                // NOTE: we may be able to set the user's data manually by sending the information back to the MainActivity. This is pretty crucial since we want the activity to have access to this even after the start tutorial.
                System.out.println("USER GAVE NON-PERSONAL CONSENT.");
            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT)) { // if this is reached the user is likely not in the EU and thus GDPR does not matter to these users, tell them that the default settings have been selected for them.

                editor.putString("Consent", Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT);
                editor.commit();

                setupGeneralAd(true); // show personalized ads
                enableGeneralAd(); // enable the ads.

                new MaterialDialog.Builder(this)
                        .title(R.string.DefaultConsentTitle)
                        .content(R.string.DefaultConsentContent)
                        .positiveText(R.string.DefaultConsentPositive)
                        .positiveColor(ContextCompat.getColor(this, R.color.BreakingNewsViddyColorComplimentary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            // closes the dialog and moves to the section permissions section.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                dialog.dismiss();
                            }
                        })
                        .show(); // show the dialog to the user.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT)) { // user gave personal consent we can show personalized ads.

                setupGeneralAd(true); // show personalized ads.
                enableGeneralAd(); // enable the ads
                editor.putString("Consent", Constants.GDPR_CONSENT.PERSONAL_CONSENT);
                editor.commit();

                // we may begin to do things as normal!
                System.out.println("USER GAVE PERSONAL CONSENT.");

            } else { // not sure what this would be.
                System.out.println("A DIFFERENT TYPE OF CONSENT IS PROVIDED HERE.");
                System.out.println(gdprConsentState.logString());
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (isDrawerOpen) {
            mainDrawerLayout.closeDrawer(GravityCompat.START);
        } else if (!canGoBack) { // if we can't go back kill the application.
            finish();
        } else if (currentFragment instanceof TrimVideoFragment) {
            System.out.println("We have an instance of viddy step 2 fragment!!!");
            notifyFragmentIndicator(0); // make the indicator go back to the first fragment.
            getSupportFragmentManager().popBackStack();
            canGoBack = false; // can longer go back. Kill the app if pressed twice!
            showEditClipNameButton = false;
            invalidateOptionsMenu();
            // super.onBackPressed();
        }
        else { // likely the user is on a fragment where going back is not possible. Thus we have to kill the app itself.
            super.onBackPressed();
            finish(); // kill the main activity.
        }
    }
}
