package thenotoriousrog.viddy.BreakingNewsViddy;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.gelitenight.waveview.library.WaveView;
import com.michaelflisar.gdprdialog.GDPR;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.BreakingNewsViddy.Handlers.BNVHandlerController;
import thenotoriousrog.viddy.BreakingNewsViddy.Handlers.BreakingNewsUIHandler;
import thenotoriousrog.viddy.ViddyMeme.Handlers.HandlerController;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.ViddyMeme.RippleLayout;

public class BreakingNewsViddyStep3Fragment extends Fragment {

    private BreakingNewsViddyActivity breakingNewsViddyActivity;
    private BreakingNewsVidRipperService breakingNewsVidRipperService; // the vid ripper service for the breaking news viddy.
    private Intent serviceIntent; // this is set by notification handler that starts the foreground service.
    private String videoPath = ""; // the video Uri passed in from MainActivity.
    private String customWatermark = ""; // the user's watermark.
    private String breakingNewsText = ""; // the breaking news text itself.
    private String breakingNewsTimeText = ""; // the time of the breaking news.
    private String breakingNewsSubText = ""; // the subtext that describes the breaking news to the user.
    private String realPath = ""; // the real path of the actual system structure.
    private String filename; // the filename for the video for the user to work with.
    private WaveView loadingWaveView; // the wave view that shows the animation.
    private ImageView checkmark; // the checkmark when the viddy is complete.
    private ImageView moreInfo; // the more info button.
    private ImageView stopButton; // button that the user can press to stop making a viddy whenever the user wishes.
    private RippleLayout rippleLayout; // the layout that will show the ripple effect.
    private Button makeNewViddyButton; // the button to create a new viddy.
    private boolean withWatermark = true; // if with watermark we need to generate the meme with the proper watermark.
    ObjectAnimator waterLevelUpAnim; // the wave up animation that looks like it's filling up the view.
    ObjectAnimator waterLevelDownAnim; // makes the waves flow down, normally when resetting the wait.
    private TextView progressCounter; // the percentage of completion for the layout.
    private TextView progressMessage; // the message telling the user what is being worked on now.
    private BreakingNewsUIHandler breakingNewsUIHandler; // the UI handler for breaking news viddy.

    public void setFields(BreakingNewsViddyActivity breakingNewsViddyActivity, String videoPath, String realPath,
                          String breakingNewsText, String breakingNewsTimeText, String breakingNewsSubText, String customWatermark, boolean withWatermark, String filename)
    {
        this.breakingNewsViddyActivity = breakingNewsViddyActivity;
        this.videoPath = videoPath;
        this.realPath = realPath;
        this.breakingNewsText = breakingNewsText;
        this.breakingNewsTimeText = breakingNewsTimeText;
        this.breakingNewsSubText = breakingNewsSubText;
        this.customWatermark = customWatermark;
        this.withWatermark = withWatermark;
        this.filename = filename;
    }

    private ServiceConnection breakingNewsVidRipperServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder iBinder)
        {
            System.out.println("WE ARE SPEAKING WITH THE UI NOW!!!!!");
            BreakingNewsVidRipperService.ServiceBinder serviceBinder = (BreakingNewsVidRipperService.ServiceBinder) iBinder; // convert the binder to the service binder for the VidRipperService
            breakingNewsVidRipperService = serviceBinder.getService(); // gets the instance of the vid ripper service in here.
            breakingNewsVidRipperService.setSpeakWithUIState(true); // begin speaking with the UI again!
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            System.out.println("WE ARE NO LONGER SPEAKING WITH THE UI");
            breakingNewsVidRipperService.setSpeakWithUIState(false); // we lost connection, stop sending updates to the UI i.e. this activity.
            breakingNewsVidRipperService.cancelNotification();
            breakingNewsVidRipperService = null; // set the vid ripper service to null, the UI is about to be destroyed.
        }
    };

    // This method starts the vid ripper service to do the work in the background away from the UI.
    private void startVidRipperService()
    {
        System.out.println("Recreating the breaking news vid ripper service again!!!");
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());

        serviceIntent = new Intent(getActivity(), BreakingNewsVidRipperService.class); // the intent that will start the PlayerService.
        Bundle args = new Bundle();
        args.putString("VideoPath", videoPath);
        args.putString("RealPath", realPath);
        args.putString("BreakingNewsText", breakingNewsText);
        args.putString("BreakingNewsTimeText", breakingNewsTimeText);
        args.putString("BreakingNewsSubText", breakingNewsSubText);
        args.putString("CustomWatermark", customWatermark);
        args.putBoolean("WithWatermark", withWatermark);
        args.putString("filename", filename);
        serviceIntent.putExtras(args); // send in the arguments to the VidRipperService.
        serviceIntent.setAction(Constants.ACTION.STARTFOREGROUND_ACTION); // the action tells the service to begin.

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) // only in oreo can we start a foreground service this way.
        {
            System.out.println("STARTING VID RIPPER SERVICE!");
            breakingNewsViddyActivity.startForegroundService(serviceIntent); // starts the actual foreground service.
            breakingNewsViddyActivity.bindService(serviceIntent, breakingNewsVidRipperServiceConnection, Context.BIND_AUTO_CREATE); // allows us to bind a service and sends our activity to be started again if killed.
        }
        else
        {
            System.out.println("STARTING VID RIPPER SERVICE!");
            breakingNewsViddyActivity.startService(serviceIntent); // start the service.
            breakingNewsViddyActivity.bindService(serviceIntent, breakingNewsVidRipperServiceConnection, Context.BIND_AUTO_CREATE); // allows us to bind a service and sends our activity to be started again if killed.
        }
    }

    // simply shows that the viddy is finished with the new UI details.
    public void showFinishedDetails()
    {
        loadingWaveView.setVisibility(View.GONE); // make the view gone.
        stopButton.setVisibility(View.GONE); // remove the stop viddy button.
        rippleLayout.setColor("Success"); // this will display the success color i.e. green.
        Animation zoomIn = AnimationUtils.loadAnimation(breakingNewsViddyActivity.getBaseContext(), R.anim.fab_scale_up);
        zoomIn.setDuration(500); // half a second.
        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                rippleLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rippleLayout.startRippleAnimation();
            }

            // nothing needed here.
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        checkmark.setVisibility(View.VISIBLE);
        checkmark.startAnimation(zoomIn);

        moreInfo.setVisibility(View.VISIBLE);
        moreInfo.startAnimation(zoomIn);

        makeNewViddyButton.setVisibility(View.VISIBLE);
        makeNewViddyButton.startAnimation(zoomIn);
    }

    // Writes that the activity is finished and the next activity (currently display main activity) is shown.
    public void finishStep3()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        SharedPreferences.Editor editor =  preferences.edit(); // create an editor to make changes to what is saved to main memory.
        editor.putInt("CurrentActivity", Constants.ACTIVITY.NO_ACTIVITY); // puts the id for now id telling main activity to display it's initial setup.
        editor.commit();
    }

    // sets the progress counter.
    public void setProgressCounterText(String percentage)
    {
        progressCounter.setText(percentage);
    }

    // sets the message of the new thing being worked on...
    public void setProgressMessage(String message)
    {
        progressMessage.setText(message);
    }

    // This method is called if viddy encounters an error be it that the user has stopped the viddy or something else caused the error to occur.
    public void showStoppedDetails(String errorMessage) {
        loadingWaveView.setVisibility(View.GONE); // remove the loading view.
        checkmark.setImageResource(R.drawable.ic_cancel_red_500dp);
        stopButton.setVisibility(View.GONE);

        if(errorMessage.equalsIgnoreCase(getString(R.string.UserStoppedViddyError))) {
            rippleLayout.setColor("Stopped");
        } else {
            rippleLayout.setColor("Error");
        }

        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                rippleLayout.stopRippleAnimation();
                if (ViddyUtils.getInstance().getCreatedViddy() != null)
                {
                    new MaterialDialog.Builder(getContext())
                            .title(R.string.BreakingNewsViddyNotCompleteTitle)
                            .content(errorMessage)
                            .positiveText(R.string.CompletedViddyClose)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show(); // show the dialog
                }
                else { // no viddy available.

                    new MaterialDialog.Builder(getContext())
                            .title(R.string.BreakingNewsViddyNotCompleteTitle)
                            .content(errorMessage)
                            .positiveText(R.string.CompletedViddyClose)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show(); // show the dialog
                }
            }
        });

        // values below are used for the animation that we need to show the buttons.
        Animation zoomIn = AnimationUtils.loadAnimation(getContext(), R.anim.fab_scale_up);
        zoomIn.setDuration(500); // half a second.
        zoomIn.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                progressCounter.setText("");
                progressMessage.setText(getString(R.string.StoppedMessage));
                rippleLayout.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                rippleLayout.startRippleAnimation();
            }

            // nothing needed here.
            @Override
            public void onAnimationRepeat(Animation animation) { }
        });

        checkmark.setVisibility(View.VISIBLE);
        checkmark.startAnimation(zoomIn);

        moreInfo.setVisibility(View.VISIBLE);
        moreInfo.startAnimation(zoomIn);
        makeNewViddyButton.setVisibility(View.VISIBLE);
        makeNewViddyButton.startAnimation(zoomIn);

    }

    // sets the properties of the wave view i.e. animations and colors.
    private void setWaveViewProperties(WaveView waveView)
    {
        waveView.setShowWave(true);
        waveView.setWaveColor(ContextCompat.getColor(getContext(), R.color.BreakingNewsViddyColorAccent), ContextCompat.getColor(getContext(), R.color.BreakingNewsViddyColorPrimary));

        // horizontal animation.
        // wave waves infinitely.
        ObjectAnimator waveShiftUpAnim = ObjectAnimator.ofFloat(
                waveView, "waveShiftRatio", 0f, 1f);
        waveShiftUpAnim.setRepeatCount(ValueAnimator.INFINITE);
        waveShiftUpAnim.setDuration(500);
        waveShiftUpAnim.setInterpolator(new LinearInterpolator());
        waveShiftUpAnim.start();

        // vertical animation.
        // water level increases from 0 to center of WaveView
        waterLevelUpAnim = ObjectAnimator.ofFloat(
                waveView, "waterLevelRatio", 0f, 0.5f);
        waterLevelUpAnim.setDuration(10000);
        waterLevelUpAnim.setInterpolator(new DecelerateInterpolator());
        waterLevelUpAnim.start();

        // vertical down animation.
        waterLevelDownAnim = ObjectAnimator.ofFloat(waveView, "waterLevelRatio", 0.5f, 0f);
        waterLevelDownAnim.setDuration(1000); // one second.
        waterLevelDownAnim.setInterpolator(new AccelerateInterpolator()); // decrease the water fast.

        // amplitude animation.
        // wave grows big then grows small, repeatedly
        ObjectAnimator amplitudeAnim = ObjectAnimator.ofFloat(
                waveView, "amplitudeRatio", 0f, 0.03f);
        amplitudeAnim.setRepeatCount(ValueAnimator.INFINITE);
        amplitudeAnim.setRepeatMode(ValueAnimator.REVERSE);
        amplitudeAnim.setDuration(2000);
        amplitudeAnim.setInterpolator(new LinearInterpolator());
        amplitudeAnim.start();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.breaking_news_viddy_step3fragment, container, false); // inflate step1 fragment
        String userConsent = GDPR.getInstance().getConsentState().getConsent().name(); // get the consent type from the user.

        breakingNewsUIHandler = new BreakingNewsUIHandler(this, Looper.getMainLooper()); // setup the BreakingNewsUIHandler.
        loadingWaveView = mainView.findViewById(R.id.loadingWaveView); // the cool wave view that makes the app look oh so awesome!
        setWaveViewProperties(loadingWaveView); // set the properties of the wave view itself!
        progressCounter = mainView.findViewById(R.id.progressCounter);
        progressMessage = mainView.findViewById(R.id.progressMessage);

        System.out.println("IS BREAKING NEWS VIDDY STEP 3 RUNNING? " + ViddyUtils.getInstance().isBreakingNewsViddyStep3Running());
        System.out.println("IS FINISHED CREATING BREAKING NEWS VIDDY? " + ViddyUtils.getInstance().isFinishedCreatingBreakingNewsViddy());

        // if the ViddyUtils are holding onto the state, then update the counters for the user to view Very important
        if(ViddyUtils.getInstance().isBreakingNewsViddyStep3Running() && !ViddyUtils.getInstance().isFinishedCreatingBreakingNewsViddy())
        {
            // todo: decide if it's possible to keep this text the same so that I don't have to redo it again.
            if(ViddyUtils.getInstance().getStep3ProgressText() != null){
                progressCounter.setText(ViddyUtils.getInstance().getStep3ProgressText());
            }
            if(ViddyUtils.getInstance().getStep3ProgressMessageText() != null) {
                progressMessage.setText(ViddyUtils.getInstance().getStep3ProgressMessageText());
            }
        }

        rippleLayout = mainView.findViewById(R.id.rippleLayout);
        checkmark = mainView.findViewById(R.id.finishedCheckmark);
        moreInfo = mainView.findViewById(R.id.moreInfoImage);
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                rippleLayout.stopRippleAnimation(); // stop the ripple animation no matter what when clicked.

                if (ViddyUtils.getInstance().getCreatedBreakingNewsViddy() != null)
                {
                    new MaterialDialog.Builder(getContext())
                            .title(R.string.CompletedBreakingNewsViddyTitle)
                            .content(ViddyUtils.getInstance().getCreatedBreakingNewsViddy().getAbsolutePath())
                            .positiveText(R.string.CompletedViddyClose)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    rippleLayout.stopRippleAnimation();
                                }
                            })
                            .show(); // show the dialog
                }
                else { // no viddy available.

                    new MaterialDialog.Builder(getContext())
                            .title(R.string.CompletedBreakingNewsViddyTitle)
                            .content(R.string.CompletedBreakingNewsViddyNotAvailable)
                            .positiveText(R.string.CompletedViddyClose)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    rippleLayout.stopRippleAnimation();
                                }
                            })
                            .show(); // show the dialog
                }
            }
        });

        stopButton = mainView.findViewById(R.id.stopViddyImage); // the stop button that the user can push to stop the behavior of the viddy image. Very important!
        stopButton.setOnClickListener(new View.OnClickListener() {

            // sends a stop signal to the viddy ripper background thread and begins stopping the viddy as soon as possible.
            @Override
            public void onClick(View v) {

                new MaterialDialog.Builder(breakingNewsViddyActivity)
                        .title(R.string.StopViddyTitle)
                        .content(R.string.StopBreakingNewsViddyContent)
                        .positiveText(R.string.StopViddyPositiveText)
                        .positiveColor(ContextCompat.getColor(getContext(), R.color.BreakingNewsViddyColorComplimentary))
                        .negativeText(R.string.StopViddyNegativeText)
                        .negativeColor(ContextCompat.getColor(getContext(), R.color.BreakingNewsViddyColorComplimentary))
                        .onNegative(new MaterialDialog.SingleButtonCallback() {
                            // simply dismiss the dialog.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss();
                            }
                        })
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            // send the stop signal, immediately update the UI to show the action has been started, dimiss the dialog.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                progressCounter.setText("");
                                progressMessage.setText(getString(R.string.StoppingMessage));
                                dialog.dismiss();

                                BNVHandlerController controller = new BNVHandlerController(Looper.getMainLooper());

                                // create the message that we will send to the handler controller to control the logic for the stop message.
                                Message msg = new Message();
                                msg.arg1 = Constants.ACTION.STOP_BREAKING_NEWS_VID_RIPPER_THREAD_SIGNAL;
                                controller.sendMessage(msg);
                            }
                        })
                        .show();
            }
        });

        // simply restart to the main activity if the user chooses to press this button.
        makeNewViddyButton = mainView.findViewById(R.id.makeNewViddyButton);
        makeNewViddyButton.setOnClickListener(new View.OnClickListener() {

            // Remove all state from the last created viddy so that the user can create another viddy without it immediately showing the green checkmark.
            @Override
            public void onClick(View v)
            {
                ViddyUtils.getInstance().restartViddyUtils(); // restarts the viddy utils to be brand new.
                breakingNewsViddyActivity.finish();
                Intent restartActivity = new Intent(breakingNewsViddyActivity, BreakingNewsViddyActivity.class);
                startActivity(restartActivity); // restart the main activity.
            }
        });

        // if the viddyUtils holds a copy of the created viddy then we need to show the finished stats. Otherwise, we can be sure that the activity needs to be reset.
        if (ViddyUtils.getInstance().isFinishedCreatingBreakingNewsViddy()) {
            showFinishedDetails();
        } else {

            System.out.println("Real path = " + realPath);
            System.out.println("Video path = " + videoPath);

            System.out.println("STARTING THE BREAKING NEWS VID RIPPER SERVICE!");
            startVidRipperService(); // begin the vid ripper service now!
        }

        mainView.setFocusableInTouchMode(true);
        mainView.requestFocus();
        mainView.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    breakingNewsViddyActivity.finish(); // kill the activity if the user presses the back button here.
                    return true;
                }
                return false;
            }
        });

        return mainView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (breakingNewsVidRipperService != null) {
            breakingNewsViddyActivity.unbindService(breakingNewsVidRipperServiceConnection);
        }

    }

}
