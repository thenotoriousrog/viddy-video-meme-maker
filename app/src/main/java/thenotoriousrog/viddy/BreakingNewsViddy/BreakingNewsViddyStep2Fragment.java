package thenotoriousrog.viddy.BreakingNewsViddy;

import android.Manifest;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.michaelflisar.gdprdialog.GDPR;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;

public class BreakingNewsViddyStep2Fragment extends Fragment {

    private BreakingNewsViddyActivity breakingNewsViddyActivity; // the main activity that we are using.
    private String videoPath = ""; // the video Uri passed in from MainActivity.
    private String customWatermark = ""; // the user's custom watermark.
    private String breakingNewsText = ""; // the breaking news text itself.
    private String breakingNewsTimeText = ""; // the time of the breaking news.
    private String breakingNewsSubText = ""; // the subtext that describes the breaking news to the user.
    private String realPath = ""; // the real system path of the video itself.
    private TextView breakingNewsTextView; // the breaking news text view.
    private TextView breakingNewsTimeTextView; // the text view showing the time of the text for the user.
    private TextView breakingNewsSubTextView; // the text view of the breaking news description or subtext.
    private TextInputEditText breakingNewsTextField; // where the user can enter the breaking news text.
    private TextInputEditText breakingNewsTimeTextField; // where the user will enter the time of the breaking news text.
    private TextInputEditText breakingNewsSubTextField; // where the user will enter the description or subtext of the breaking news.
    private int textSizeVal = 10; // the starting value is always 10
    private boolean removeWatermarkAdWatched = false; // this will be set true upon watching the ad then they can add their watermark!
    private boolean addCustomWatermarkAdWatched = false; // tells us if the user has watched an add to add their own custom watermark!
    private RewardedVideoAd rewardedVideoAd; // the add that the user can watch in order to unlock features within this activity.
    private View mainView;
    private VideoView videoView;
    private boolean readyForStep3 = false; // tells the fragment that we are ready to go to step 3.
    private TextView customWatermarkTextPlaceholder; // the placeholder value for the textview.
    private TextView customWatermarkText; // the actual text of the user's watermark.
    private TextInputEditText customWatermarkEditText; // the user can add their watermark in here.
    private boolean isRewarded = false; // tells the system if the user has watched an ad or not.
    private String filename = null; // the filename that the user chooses.


    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFields(BreakingNewsViddyActivity breakingNewsViddyActivity, String videoPath, String realPath) {
        this.breakingNewsViddyActivity = breakingNewsViddyActivity;
        this.videoPath = videoPath;
        this.realPath = realPath;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    // sets up the listener to be able to watch the rewarded video ad.
    private void setupRewardedVideoAd(View mainView)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // get the context that we need in order to sort these lists.
        SharedPreferences.Editor editor = preferences.edit();

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getContext()); // set our ad.
        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewarded(RewardItem reward) {
                isRewarded = true;
            }

            @Override
            public void onRewardedVideoAdLeftApplication() { }

            @Override
            public void onRewardedVideoAdClosed() {
                // Toast.makeText(getBaseContext(), "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();

                if(isRewarded) {
                    Toast toast = new Toast(getContext());
                    View toastView = getLayoutInflater().inflate(R.layout.bnv_watermark_not_added_toast, (ViewGroup) mainView.findViewById(R.id.watermarkNotAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
                    toast.setView(toastView);
                    toast.show();

                    BreakingNewsViddyStep3Fragment breakingNewsViddyStep3Fragment = new BreakingNewsViddyStep3Fragment();
                    breakingNewsViddyStep3Fragment.setFields(breakingNewsViddyActivity, videoPath, realPath,  breakingNewsText, breakingNewsTimeText, breakingNewsSubText,
                            customWatermark, false, filename);
                    FragmentTransaction transaction = breakingNewsViddyActivity.getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.mainFrameLayout, breakingNewsViddyStep3Fragment);
                    transaction.commit();
                    breakingNewsViddyActivity.notifyFragmentIndicator(3); // make the third icon lightup.
                    breakingNewsViddyActivity.hideEditFilenameIcon();
                    breakingNewsViddyActivity.invalidateOptionsMenu();
                }
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                CoordinatorLayout snackbarLocation = mainView.findViewById(R.id.snackbarLocation);
                Snackbar snackbar = Snackbar.make(snackbarLocation, R.string.Ad_Not_Loading, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(getContext(), R.color.BreakingNewsViddyColorComplimentary));
                sbView.setElevation(5);
                snackbar.show();
            }

            // listens for when the ad is loaded.
            @Override
            public void onRewardedVideoAdLoaded() {
                rewardedVideoAd.show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                isRewarded = true;
            }

            @Override
            public void onRewardedVideoStarted() { }

            @Override
            public void onRewardedVideoCompleted() {
                if(removeWatermarkAdWatched) { // if this is true and the user finishes the ad we can proceed to step 3.
                    readyForStep3 = true;
                }
            }
        });
    }

    private void setupStep3Button(View mainView, String userConsent) {
        final Button step3Button = mainView.findViewById(R.id.step3Button);
        step3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // if they choose no, display a full screen ad, after the ad the activity will generate the meme without a watermark.
                // if they choose to keep the watermark then immediately generate the meme with the watermark of the app. Very important!

                String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                if(EasyPermissions.hasPermissions(getContext(), perms)) // permissions available, proceed as normal.
                {

                    // replace this fragment with the new fragment.
                    BreakingNewsViddyStep3Fragment breakingNewsViddyStep3Fragment = new BreakingNewsViddyStep3Fragment();
                    breakingNewsViddyStep3Fragment.setFields(breakingNewsViddyActivity, videoPath, realPath,  breakingNewsText, breakingNewsTimeText, breakingNewsSubText,
                            customWatermark, false, filename);
                    FragmentTransaction transaction = breakingNewsViddyActivity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainFrameLayout, breakingNewsViddyStep3Fragment);
                    transaction.commit();
                    breakingNewsViddyActivity.notifyFragmentIndicator(3); // make the third icon lightup.
                    breakingNewsViddyActivity.hideEditFilenameIcon();
                    breakingNewsViddyActivity.invalidateOptionsMenu();

                } else { // permissions not granted, we need to get them as soon as possible.

                    String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}; // just need to read/write storage android.
                    EasyPermissions.requestPermissions(
                            new PermissionRequest.Builder(breakingNewsViddyActivity, Constants.ACTION.REQUEST_PERMISSIONS, permissions)
                                    .setRationale(R.string.PermissionsRationale)
                                    .setPositiveButtonText(R.string.PermissionRationaleOk)
                                    .setNegativeButtonText(R.string.PermissionRationaleCancel)
                                    .build());
                }
            }
        });
    }

    /*
        * Sets the breaking news text field and also sets the TextWatcher behavior to update the UI in real time.
     */
    private void setupBreakingNewsTextField(View mainView) {
        breakingNewsTextField = mainView.findViewById(R.id.breakingNewsText);
        breakingNewsTextField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            // update the text as the user types.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                breakingNewsText = s.toString();
                breakingNewsTextView.setText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    /*
     * Sets the breaking news time text field and also sets the TextWatcher behavior to update the UI in real time.
     */
    private void setupBreakingNewsTimeTextField(View mainView) {
        breakingNewsTimeTextField = mainView.findViewById(R.id.breakingNewsTimeText);
        breakingNewsTimeTextField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            // update the text as the user types.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                breakingNewsTimeText = s.toString();
                breakingNewsTimeTextView.setText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    /*
     * Sets the breaking news sub text field and also sets the TextWatcher behavior to update the UI in real time.
     */
    private void setupBreakingNewsSubTextField(View mainView) {
        breakingNewsSubTextField = mainView.findViewById(R.id.breakingNewsSubText);
        breakingNewsSubTextField.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            // update the text as the user types.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                breakingNewsSubText = s.toString();
                breakingNewsSubTextView.setText(s.toString());
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    private void setupCustomWatermark() {
        customWatermarkText = mainView.findViewById(R.id.customWatermarkText);
        customWatermarkEditText = mainView.findViewById(R.id.customWatermark);
        customWatermarkEditText.setVisibility(View.VISIBLE); // show the placeholders
        customWatermarkEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            // update the text values as the user begins typing their watermark.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                customWatermarkText.setText(s.toString());
                customWatermark = s.toString();
            }

            @Override
            public void afterTextChanged(Editable s) {}
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.breaking_news_viddy_step2fragment, container, false); // inflate step1 fragment
        this.mainView = mainView;

        String userConsent = GDPR.getInstance().getConsentState().getConsent().name(); // get the consent type from the user.

        //setupDiscreteSeekBar(mainView);
        setupRewardedVideoAd(mainView);
        setupStep3Button(mainView, userConsent);

        System.out.println("The path of the video that we want to play: " + videoPath);

        RelativeLayout breakingNewsLayout = mainView.findViewById(R.id.breakingNewsLayout);
        RelativeLayout viddyPreviewStep2 = mainView.findViewById(R.id.viddyPreviewStep2);
        videoView = mainView.findViewById(R.id.videoViewStep2);
        //videoView.setVideoURI(Uri.fromFile(new File(videoPath)));
        videoView.stopPlayback();
        videoView.setVideoPath(videoPath);
        videoView.start();
       // videoView.setLayoutParams(new RelativeLayout.LayoutParams(200, 1));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                System.out.println("THE ON PREPARED LISTENER IS WORKING NOW!!!!");
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            // is in charge of restarting the video over and over again.
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                // stop playback, reset vid path, start the video again.
                videoView.stopPlayback(); // stop the playback if completed.
                videoView.setVideoPath(videoPath);
                videoView.start();
            }
        });

        breakingNewsTextView = mainView.findViewById(R.id.breakingNewsHeadline); // the text that the user's watermark will go.
        breakingNewsTimeTextView = mainView.findViewById(R.id.breakingNewsTime); // the time field of the text that we are editing.
        breakingNewsSubTextView = mainView.findViewById(R.id.breakingNewsDescription); // the description or subtext of the breaking news.

        setupBreakingNewsTextField(mainView); // setup the breaking news text field including text watchers.
        setupBreakingNewsTimeTextField(mainView); // setup the breaking news time text field including text watchers.
        setupBreakingNewsSubTextField(mainView); // setup the breaking news subtext (or description) including text watchers.
        //setupCustomWatermarkTextPlaceHolderListener(mainView, userConsent); // setup the custom watermark text placeholder listener.
        setupCustomWatermark();

        return mainView;
    }

}
