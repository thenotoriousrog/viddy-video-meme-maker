package thenotoriousrog.viddy.BreakingNewsViddy;

import android.os.Environment;

import java.io.File;
import java.util.concurrent.TimeUnit;

import nl.bravobit.ffmpeg.FFcommandExecuteResponseHandler;
import nl.bravobit.ffmpeg.FFmpeg;
import thenotoriousrog.viddy.Activities.MainActivity;

public class BNVTrimVidBackgroundThread implements Runnable {

    private BreakingNewsViddyActivity bnvActivity;
    private int startMilliseconds;
    private int endMilliseconds;
    private String realPath; // the real path of the video in the user's phone.
    private String clipName; // the name of the clip.
    private boolean trimming = true; // tells the thread that the user's video is still trimming.
    private Thread backgroundThread; // the background thread.

    public BNVTrimVidBackgroundThread(BreakingNewsViddyActivity bnvActivity, int startMilliseconds, int endMilliseconds, String realPath, String clipName) {
        this.bnvActivity = bnvActivity;
        this.startMilliseconds = startMilliseconds;
        this.endMilliseconds = endMilliseconds;
        this.realPath = realPath;
        this.clipName = clipName;

        if(this.clipName == null || this.clipName.isEmpty()) {
            this.clipName = "VideoClip";
        }

        backgroundThread = new Thread(this);
    }

    public void startBackgroundThead() {
        backgroundThread.start();
    }

    // converts the time needed in FFMPEG format with whatevere seconds are passed in.
    private String getFFMPEGTime(int milliseconds) {

        String ffmpegFormat = "00:"; // the ffmpeg format that we are expecting for the video time.

        int minutes = (int) TimeUnit.MILLISECONDS.toMinutes(milliseconds);
        int seconds = (int)TimeUnit.MILLISECONDS.toSeconds(milliseconds) % 60; // get the number of seconds and mod by 60 to remove minutes.

        System.out.println("Minutes calculated: " + minutes);
        System.out.println("Seconds calculated: " + seconds);

        if(minutes < 10) { // prepend a 0 if the seconds are less than 10.
            System.out.println("LESS THAN 10 MINUTES!");
            ffmpegFormat += "0" + minutes + ":";
        } else {
            ffmpegFormat += minutes + ":";
        }

        if(seconds < 10) { // keeps a x:0x look for the seconds which looks better to me lol
            System.out.println("LESS THAN 10 SECONDS");
            ffmpegFormat += "0" + seconds + ".000";
        } else {
            ffmpegFormat += seconds + ".000";
        }

        System.out.println("THE FFMPEG FORMAT THAT WE GENERATED IS " + ffmpegFormat);

        return ffmpegFormat;
    }

    private File trimVideo() {

        File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/TrimmedVideos"); // a temporary folder to hold video audio.
        if(!root.exists()) {
            root.mkdirs(); // make all the directories.
        } else {
            root.delete();
            root.mkdirs();
        }

        // todo: the command using this video could be wrong. I need to get it wo match this format if it doesn't work: 00:00:00.00 // hours, minutes, seconds.millisecons I'm guessing.

        File trimmedVideo = new File(realPath); // recreate a newFile with the real path of the video that we're trying to work with.
        String trimmedVideoName = "/" + clipName + ".mp4"; // todo: this may cause problems, I may have to change this as not all videos are .mp4

//        String[] cmd = {"-y", "-i", "-ss", "00:00:00.000", trimmedVideo.getAbsolutePath(), "-vcodec", "copy", "-acodec", "copy",
//               "-t", "00:00:02.000", "-strict", "-2", root.getAbsolutePath() + trimmedVideoName}; // command to trim a video

        String filePrefix = trimmedVideo.getAbsolutePath().substring(trimmedVideo.getAbsolutePath().lastIndexOf("."));

        // todo: use this link: https://stackoverflow.com/questions/40121065/android-ffmpeg-video-cut to handle different formats for videos. Very important!
        // todo: we need to handle other video types this is very very very important!
        if(!filePrefix.equals(".mp4")) {
            return null;
        }

        String[] cmd = {"-y", "-i",trimmedVideo.getAbsolutePath(), "-ss", getFFMPEGTime(startMilliseconds), "-vcodec", "copy", "-acodec", "copy",
                "-t", getFFMPEGTime(endMilliseconds-startMilliseconds), "-strict", "-2", root.getAbsolutePath() + trimmedVideoName}; // command to trim a video

        String[] cmdSlow = {"-y", "-i", trimmedVideo.getAbsolutePath(), "-preset", "ultrafast", "-ss", getFFMPEGTime(startMilliseconds), "-t", getFFMPEGTime(endMilliseconds-startMilliseconds), "-async", "1", root.getAbsolutePath() + trimmedVideoName};

        FFmpeg.getInstance(bnvActivity).execute(cmdSlow, new FFcommandExecuteResponseHandler() {
            @Override
            public void onSuccess(String message) { trimming = false; }
            @Override
            public void onProgress(String message) { }
            @Override
            public void onFailure(String message) {
                System.out.println("FAILURE TRYING TO CUT VIDEO!!");
                trimming = false;
            }
            @Override
            public void onStart() { }
            @Override
            public void onFinish() { trimming = false; }
        });

        return new File(root.getAbsolutePath() + trimmedVideoName); // creates a new file with the path that should lead to the created video.
    }

    @Override
    public void run()
    {

        if(FFmpeg.getInstance(bnvActivity).isSupported()) {
            System.out.println("FFMPEG IS SUPPORTED!");
        } else {
            System.out.println("FFMPEG IS NOT SUPPORTED!");
        }

        File trimmedVideo = trimVideo(); // trims video and returns the file where the trim video is located.

        while(trimming) {} // while trimming do nothing.

        bnvActivity.displayViddyStep2Fragment(trimmedVideo); // display step 2 fragment with the path of the real video.
        bnvActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                bnvActivity.notifyFragmentIndicator(2); // this new thread allows us to easily update the UI when everything is finished.
            }
        });
        //mainActivity.notifyFragmentIndicator(2); // show the 4th button to be highlighted not the 3rd.
    }

}
