package thenotoriousrog.viddy.BreakingNewsViddy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.VideoView;

import com.google.android.gms.ads.AdView;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;

public class BreakingNewsViddyStep1Fragment extends Fragment {

    private VideoView videoView; // the video view that the user will see.
    private String videoPath = null; // holds the Uri of the video that the user has selected.
    private String realPath = null; // holds the real system level path for us to be able to find the actual video.
    private Uri videoUri = null; // holds the video uri received from the system.
    private SharedPreferences preferences; // get the context that we need in order to sort these lists.
    private boolean finishedStartScreen = false; // tells the main activity that the user has finished the start screen.
    private DrawerLayout mainDrawerLayout; // the main drawer layout which everything is contained in.
    private boolean isDownloadedVideo = false; // tells the activity that the user has chosen to use a link. If they have we need to delete the video after they have used it.
    private boolean isDrawerOpen = false; // tells us if the drawer is open or not.
    private BreakingNewsViddyActivity breakingNewsViddyActivity; // a copy of the main activity that I am working with.
    private AdView generalAdView; // the general ad for the Ad view.
    private View mainView; // the main view created by this fragment.

    // Makeshift constructor to handle the various events that need to take place.
    public void setFields(BreakingNewsViddyActivity breakingNewsViddyActivity)
    {
        this.breakingNewsViddyActivity = breakingNewsViddyActivity;
    }

    public void setVideoUri(Uri videoUri) {
        this.videoUri = videoUri;
    }

    public void setVideoPath(String videoPath) {
        this.videoPath = videoPath;
    }


    // starts the video view with the path of the video that we are using.
    public void startVideoView() {

        System.out.println("Starting video view now!");
        videoView.setVideoURI(videoUri);
        videoView.requestFocus();
        videoView.start(); // start playing the video for the user.
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            // is in charge of restarting the video over and over again.
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                // stop playback, reset vid path, start the video again.
                videoView.stopPlayback(); // stop the playback if completed.
                videoView.setVideoURI(videoUri);
                videoView.start();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private void setupFAB(View mainView)
    {
        CoordinatorLayout fabCoordLayout = mainView.findViewById(R.id.fabCoordLayout);
        CoordinatorLayout.Behavior behavior = new CoordinatorLayout.Behavior() {
            @Override
            public void onAttachedToLayoutParams(@NonNull CoordinatorLayout.LayoutParams params) {
                super.onAttachedToLayoutParams(params);

                params.dodgeInsetEdges = Gravity.BOTTOM;
            }
        };

        FloatingActionButton fab = mainView.findViewById(R.id.createViddyButton);
        fab.setRippleColor(ViddyUtils.getInstance().getColor(getContext(), R.color.white));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent openFileManagerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                openFileManagerIntent.setType("*/*"); // allow a file of any type. todo: change this to only be m3u
                breakingNewsViddyActivity.startActivityForResult(openFileManagerIntent, Constants.INTENT.PICK_FILE);
            }
        });
    }


    private void setupStep2Button(View mainView)
    {
        final Button step2Button = mainView.findViewById(R.id.step2Button);
        step2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(videoPath != null) // the uri isn't null thus we can make use of the video.
                {
                    //Intent step2Intent = new Intent(mainActivity, Step2Activity.class); // the intent to start step2
                    Bundle args = new Bundle();

                    System.out.println("Video path being sent to step 2: " + videoPath);

                    args.putString("VideoPath", videoPath); // put the video Uri for the activity to use in order to display the video again.
                    args.putString("RealPath", realPath); // set the real path to be used throughout the app.
                    //step2Intent.putExtras(args); // set the args for the activity to make use of on the app itself.
                    //breakingNewsViddyActivity.displayViddyStep2Fragment();
                    breakingNewsViddyActivity.displayTrimVideoFragment();
                    breakingNewsViddyActivity.notifyFragmentIndicator(1); // make the second indicator light now.
                    //startActivity(step2Intent); // start the step 2 activity.
                }
                else { // the user pressed proceed to step 2 without selecting a video, ask them to pick a video first, very important!

                    // todo: we need to handle this error here. The user pressed step 2 before they have actually selected a video to use.
                }

                // TODO: setup the behavior to start a new activity for step 2 where the user can select the text of their video.
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.breaking_news_viddy_step1fragment, container, false); // inflate step1 fragment

        this.mainView = mainView;
        videoView = mainView.findViewById(R.id.videoView);
        setupFAB(mainView);
        setupStep2Button(mainView);

        return mainView;
    }
}
