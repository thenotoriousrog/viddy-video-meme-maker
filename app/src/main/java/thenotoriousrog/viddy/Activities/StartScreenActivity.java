package thenotoriousrog.viddy.Activities;

import android.Manifest;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.michaelflisar.gdprdialog.GDPR;
import com.michaelflisar.gdprdialog.GDPRConsent;
import com.michaelflisar.gdprdialog.GDPRConsentState;
import com.michaelflisar.gdprdialog.GDPRDefinitions;
import com.michaelflisar.gdprdialog.GDPRLocation;
import com.michaelflisar.gdprdialog.GDPRSetup;
import com.michaelflisar.gdprdialog.helper.GDPRPreperationData;

import java.util.ArrayList;
import java.util.List;

import pub.devrel.easypermissions.EasyPermissions;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.Tutorial.AdPermissionFragment;
import thenotoriousrog.viddy.Tutorial.PermissionsFragment;
import thenotoriousrog.viddy.Tutorial.TutorialFragment;
import thenotoriousrog.viddy.Tutorial.TutorialPagerAdapter;
import thenotoriousrog.viddy.Tutorial.TutorialViewPager;

/*
    * This activity allows us to give the user's a run down of what the app is like and what you can use the app for.
    * We are going to ask the user's for permissions to show ads.
    * We are going to ask for permission in the app itself.
    * DO NOT SHOW ADS IN THIS ACTIVITY.
 */
public class StartScreenActivity extends AppCompatActivity implements View.OnClickListener, GDPR.IGDPRCallback, EasyPermissions.PermissionCallbacks {

    private int selectedIndicator = R.drawable.circle_black;
    private int indicator = R.drawable.circle_white;
    private TutorialPagerAdapter tutorialPagerAdapter;
    private ArrayList<Fragment> fragments = new ArrayList<>(); // the fragments that we are creating for the overall system.
    private TutorialViewPager viewPager;
    private int currPosition = 0; // holds the current position of the view pager.
    private Button prev, next;
    private LinearLayout indicatorLayout;
    private FrameLayout frameLayout;
    private RelativeLayout relativeLayout;
    private String prevText, nextText, finishText, cancelText, adPermsText, permsText; // all of the different types of text that we are currently on for the user.
    private GDPRSetup gdprSetup; // the setup for GDPR consent dialog. This will be used to display a dialog once the user selects something.

    private boolean hasSelectedAdPreferences = false; // tells the activity that the user has selected their  add preferences.
    private boolean hasGrantedPermissions = false; // tells the activity that the user has granted permissions for the user.
   // private CurrentFragmentListener currentFragmentListener;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        setTheme(R.style.StartScreenStyle); // set the theme for the start screen style
        super.onCreate(savedInstanceState);
        GDPR.getInstance().init(ViddyUtils.getInstance().getMainActivity()); // initialize the GDPR instance to the copy of the main activity set by the viddy utils.
        setContentView(R.layout.startscreen);

        viewPager = findViewById(R.id.viewPager);
        frameLayout = findViewById(R.id.containerLayout);
        relativeLayout = findViewById(R.id.buttonContainer);
        relativeLayout.setBackgroundColor(ContextCompat.getColor(this, R.color.transparent));
        indicatorLayout = findViewById(R.id.indicatorLayout);
        prev = findViewById(R.id.prev);
        next = findViewById(R.id.next);
        next.setText(getString(R.string.NextButtonText)); // initial setup
        prev.setText(getString(R.string.CancelButtonText)); // initial setup

        // set click listeners for the buttons
        next.setOnClickListener(this);
        prev.setOnClickListener(this);

        // set the text needed for the buttons.
        nextText = getString(R.string.NextButtonText);
        prevText = getString(R.string.BackButtonText);
        finishText = getString(R.string.FinishButtonText);
        cancelText = getString(R.string.CancelButtonText);
        permsText = getString(R.string.GrantButtonText);
        adPermsText = getString(R.string.AdPreferencesText);

        setup();
    }

    // begins setting up the activity with the proper views and other things.
    private void setup()
    {
        setupGDPR(); // sets up our GDPR dialog.
        createFragments();
        tutorialPagerAdapter = new TutorialPagerAdapter(getSupportFragmentManager(), fragments);
        viewPager.setAdapter(tutorialPagerAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) { }

            @Override
            public void onPageSelected(int position)
            {
                // we are on the third tab, but the user has not selected an ad preference
                if (position == fragments.size() - 4 && !hasSelectedAdPreferences) {
                    //updateColors(position);
                    currPosition = position;
                    viewPager.disablePaging();
                    controlPosition(position);
                    return;
                }
                else if(position == fragments.size() - 3 && !hasGrantedPermissions) { // the the user is on the permissions tab but has not granted permissions, ignore the input.
                    //updateColors(position);
                    currPosition = position;
                    viewPager.disablePaging();
                    controlPosition(position);
                    return;

                } else { // anything in between.
                    //updateColors(position);
                    // allowSwiping();
                    viewPager.enablePaging();
                    currPosition = position;
                    controlPosition(position); // update the buttons with the proper text.
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) { }
        });
    }

    // sets up the GDPR instance to be shown in a little bit.
    private void setupGDPR()
    {
        gdprSetup = new GDPRSetup(GDPRDefinitions.ADMOB)
                .withAllowNoConsent(true) // required by law. This should be here, people have the right to say no
                .withPaidVersion(true)
                .withExplicitAgeConfirmation(false) // if the user accepts they are agreeing to be of the age of 16
                .withCheckRequestLocation(true)
                .withCheckRequestLocationFallbacks(true, true)
                .withCheckRequestLocationTimeouts(3000, 3000) // fail after 3 seconds each time.
                .withBottomSheet(false)
                .withForceSelection(true)
                .withShortQuestion(true)
                .withLoadAdMobNetworks("pub-1014366431079942") // publisher ID for Viddy
                .withNoToolbarTheme(true);
    }

    // to notify the indicator to show the views for the items.
    public void notifyIndicator() {
        if (indicatorLayout.getChildCount() > 0)
            indicatorLayout.removeAllViews();

        for (int i = 0; i < fragments.size(); i++) {
            ImageView imageView = new ImageView(this);
            imageView.setPadding(8, 8, 8, 8);
            int drawable = indicator;
            if (i == currPosition)
                drawable = selectedIndicator;

            imageView.setImageResource(drawable);

            indicatorLayout.addView(imageView);
        }
    }

    // updates the status bar and the background color as well.
    private void updateColors(int position)
    {
        System.out.println("The current position in the tutorial is: " + position);

        if (fragments.get(position) instanceof TutorialFragment) {
            TutorialFragment frag = (TutorialFragment) fragments.get(position);
            frameLayout.setBackgroundColor(frag.getBackgroundColor());
            changeStatusBarColor(frag.getBackgroundColor());
        }
        else if (fragments.get(position) instanceof AdPermissionFragment) {
            AdPermissionFragment frag = (AdPermissionFragment) fragments.get(position);
            frameLayout.setBackgroundColor(frag.getBackgroundColor());
            changeStatusBarColor(frag.getBackgroundColor());
        } else { // must be a permissions fragment.
            PermissionsFragment frag = (PermissionsFragment) fragments.get(position);
            frameLayout.setBackgroundColor(frag.getBackgroundColor());
            changeStatusBarColor(frag.getBackgroundColor());
        }
    }

    // helps control the layout for the activity.
    private void controlPosition(int position) {
        notifyIndicator();

        if (position == fragments.size() - 1) {
            next.setText(finishText);
            prev.setText("");
        } else if (position == 0) {
            prev.setText(cancelText);
            next.setText(nextText);
        } else if (position == fragments.size() - 3) { // the permissions section of the tutorial.
            prev.setText("");
            next.setText(permsText);
        } else if (position == fragments.size() - 4) { // the ad permissions section of the tutorial
            prev.setText("");
            next.setText(adPermsText);
        } else { // everything else in between.
            prev.setText(prevText);
            next.setText(nextText);
        }

        updateColors(position);
    }


    // creates the fragments to be used in the TutorialFragmentAdapter
    private void createFragments()
    {
        TutorialFragment welcomeTut = new TutorialFragment();
        int bgColor = ContextCompat.getColor(this, R.color.WelcomeColor); // the welcome color.
        int divider = ContextCompat.getColor(this, R.color.WelcomeDividerColor);
        // update the items to show the proper colors for the first tutorial
       // relativeLayout.setBackgroundColor(divider);
        frameLayout.setBackgroundColor(bgColor);
        changeStatusBarColor(divider);
        welcomeTut.setFields(R.drawable.official_logo_white, getString(R.string.WelcomeTitle), getString(R.string.WelcomeContent), getString(R.string.WelcomeSummary),
                bgColor, divider, this);

        TutorialFragment tut2 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial2Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial2DividerColor);
        tut2.setFields(R.drawable.tut_video_camera, getString(R.string.Tutorial2Title), getString(R.string.Tutorial2Content), getString(R.string.Tutorial2Summary),
                bgColor, divider, this);

        TutorialFragment tut3 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial3Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial3DividerColor);
        tut3.setFields(R.drawable.tut_standard_meme, getString(R.string.Tutorial3Title), getString(R.string.Tutorial3Content), getString(R.string.Tutorial3Summary),
                bgColor, divider, this);

        TutorialFragment tut4 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial4Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial4DividerColor);
        tut4.setFields(R.drawable.tut_floating_text_meme, getString(R.string.Tutorial4Title), getString(R.string.Tutorial4Content), getString(R.string.Tutorial4Summary),
                bgColor, divider, this);

        TutorialFragment tut5 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial5Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial5DividerColor);
        tut5.setFields(R.drawable.tut_breaking_news_meme, getString(R.string.Tutorial5Title), getString(R.string.Tutorial5Content), getString(R.string.Tutorial5Summary),
                bgColor, divider, this);

        TutorialFragment tut6 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial6Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial6DividerColor);
        tut6.setFields(R.drawable.tut_video_camera, getString(R.string.Tutorial6Title), getString(R.string.Tutorial6Content), getString(R.string.Tutorial6Summary),
                bgColor, divider, this);

        TutorialFragment tut7 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial7Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial7DividerColor);
        tut7.setFields(R.drawable.tut_quote_meme, getString(R.string.Tutorial7Title), getString(R.string.Tutorial7Content), getString(R.string.Tutorial7Summary),
                bgColor, divider, this);

        AdPermissionFragment tutAdPermissions = new AdPermissionFragment();
        bgColor = ContextCompat.getColor(this, R.color.AdPermissionColor);
        divider = ContextCompat.getColor(this, R.color.AdPermissionDividerColor);
        tutAdPermissions.setFields(this, bgColor, divider);

        PermissionsFragment tutPermissions = new PermissionsFragment();
        bgColor = ContextCompat.getColor(this, R.color.PermissionsColor);
        divider = ContextCompat.getColor(this, R.color.PermissionsDividerColor);
        tutPermissions.setFields(this, bgColor, divider);

        TutorialFragment tut8 = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.Tutorial8Color); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.Tutorial8DividerColor);
        tut8.setFields(R.drawable.tut_signature, getString(R.string.Tutorial8Title), getString(R.string.Tutorial8Content), getString(R.string.Tutorial8Summary),
                bgColor, divider, this);

//        TutorialFragment tut9 = new TutorialFragment();
//        bgColor = ContextCompat.getColor(this, R.color.Tutorial9Color); // the welcome color.
//        divider = ContextCompat.getColor(this, R.color.Tutorial9DividerColor);
//        tut9.setFields(R.drawable.inverted_viddy_icon, getString(R.string.Tutorial9Title), getString(R.string.Tutorial9Content), getString(R.string.Tutorial9Summary),
//                bgColor, divider, this);

        TutorialFragment tutFinished = new TutorialFragment();
        bgColor = ContextCompat.getColor(this, R.color.FinishedColor); // the welcome color.
        divider = ContextCompat.getColor(this, R.color.FinishedDividerColor);
        tutFinished.setFields(R.drawable.official_logo_white, getString(R.string.FinishedTitle), getString(R.string.FinishedContent), getString(R.string.FinishedSummary),
                bgColor, divider, this);

        // add all of the fragments into their proper location.
        fragments.add(welcomeTut);
        fragments.add(tut2);
        fragments.add(tut3);
        fragments.add(tut4);
        fragments.add(tut5);
        fragments.add(tut6);
        fragments.add(tut7);
        fragments.add(tutAdPermissions); // custom behavior needs to be handled
        fragments.add(tutPermissions); // custom behavior needs to be handled.
        fragments.add(tut8);
       // fragments.add(tut9);
        fragments.add(tutFinished);

        notifyIndicator(); // tell the indicator that there are new items to keep track of.
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void changeStatusBarColor(int color) {
        Window window = getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(color);
    }


    private void changeFragment(boolean isNext) {
        int item = currPosition;
        if (isNext) {
            item++;
        } else {
            item--;
        }

        if (item < 0 || item == fragments.size()) {
            finish();
        } else
            viewPager.setCurrentItem(item, true);
    }

    @Override
    public void onBackPressed()
    {
        if (currPosition == 0) {
            super.onBackPressed();

            finish(); // finish this activity
            ViddyUtils.getInstance().getMainActivity().finish(); // finish the main activity causing the whole app to finish.

        } else if (!viewPager.isCurrentlyPaging()) { // if not currently paging ignore the back button.
            // todo: tell the user that if they press the back button again then the tutorial will close. Nothing should be saved so that they can open up the tutorial again.
        }
        else {
            changeFragment(false);
        }
    }

    // Handles all click actions. Only pays attention to those of our buttons.
    @Override
    public void onClick(View v)
    {
        if (v.getId() == R.id.next) // user hit the next button (Sometimes called, Configure, Grant, and Finish)
        {
            if (next.getText().toString().equalsIgnoreCase(nextText)) { // a simple next behavior.
                changeFragment(true);

            } else if (next.getText().toString().equalsIgnoreCase(adPermsText)) { // we need to configure the Ad consent form

                GDPR.getInstance().checkIfNeedsToBeShown(this, gdprSetup);
                 //GDPR.getInstance().checkIfNeedsToBeShown(ViddyUtils.getInstance().getMainActivity(), gdprSetup); // checks if we need to run a check on permissions.

            } else if(next.getText().toString().equalsIgnoreCase(permsText)) { // we need to ask the user for permissions.
                String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}; // just need to read/write storage android.
                requestPermissions(permissions, Constants.ACTION.REQUEST_PERMISSIONS); // request permissions now.

            } else if(next.getText().toString().equalsIgnoreCase(finishText)) { // the user must have reached the end of the tutorial. Have the tutorial be killed.

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor editor = preferences.edit();

                editor.putBoolean("FinishedStartScreen", true); // tell the system that we have setup the startup screen for the user.
                editor.apply(); // write the shared prefs in the background thread, note: if we experience problems we need to reset the system immediately.

                // fixme: This needs to be updated to take the user to Viddy+ plus from the app every time from now on.
                if (GDPR.getInstance().getConsentState().getConsent().name().equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT)) // if the user chose no consent, we need to take the user to viddy+ from now on.
                {
                    Toast.makeText(this, "Viddy+ is in development and will be available soon!", Toast.LENGTH_LONG).show();
                    finish(); // finish the startup screen.
                    ViddyUtils.getInstance().getMainActivity().finish(); // finish the main activity causing the app to crash if the user
                }
                else { // behave as normal. Simply close the tutorial
                    finish();
                }

            } else {} // if its anything else ignore it.
        }
        else if (v.getId() == R.id.prev) // user hit previous. Handle the data accordingly.
        {
            if(prev.getText().toString().equalsIgnoreCase(prevText)) {
                changeFragment(false); // simply go back one.

            } else if (prev.getText().toString().equalsIgnoreCase(cancelText)) {
                finish(); // finish this activity
                ViddyUtils.getInstance().getMainActivity().finish(); // finish the main activity causing the whole app to finish.

            } else {} // if it's anything else ignore it.
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this); // sets the permissions for the application.
    }

    @Override
    public void onConsentNeedsToBeRequested(GDPRPreperationData gdprPreperationData) {
        System.out.println("The gdprPreperationData = " + gdprPreperationData.logString());

        GDPR.getInstance().showDialog(this, gdprSetup, gdprPreperationData.getLocation()); // we use this activity to show the dialog to the user here in the start screen activity.

        // todo: we need to get the consent and see if this matters for the user
        //GDPR.getInstance().showDialog(ViddyUtils.getInstance().getMainActivity(), gdprSetup, gdprPreperationData.getLocation()); // force the user to choose their consent choices whether they are in the EU or not.

    }

    @Override
    public void onConsentInfoUpdate(GDPRConsentState gdprConsentState, boolean isNewState) {

        //GDPR.getInstance().showDialog(this, gdprSetup, GDPRLocation.IN_EAA_OR_UNKNOWN); // used for testing. This will ensure that the dialog is shown for our testing purposes.

        System.out.println("GDPR consent that we have retrieved = " + gdprConsentState.logString());
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = preferences.edit();

        //if(isNewState) // if in a new state we need to update the user's privacy information. Extemely important!
        //{
            GDPRConsent consent = gdprConsentState.getConsent();
            System.out.println("Consent name: " + consent.name());
            if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT)) { // The user has chosen to get the paid version of the app instead.
                // todo: add a link to send the user to Viddy+
                Toast.makeText(this, "Viddy Supreme is under development", Toast.LENGTH_SHORT).show();
                ViddyUtils.getInstance().getMainActivity().setupGeneralAd(false); // recreate the ads on the main activity to not store personal information.
                ViddyUtils.getInstance().getMainActivity().disableGeneralAd(); // disable the general ads
                editor.putString("Consent", Constants.GDPR_CONSENT.NO_CONSENT); // gets the name of the user's consent.
                editor.commit(); // commit changes immediately.
                System.out.println("USER GAVE NO CONSENT.");

            } else if(consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // The user has agreed to ads, but not the good kind.
                // todo: figure out if we can retrieve the correct consent from the user or if we have to save the consent to SharedPreferences.
                // NOTE: we may be able to set the user's data manually by sending the information back to the MainActivity. This is pretty crucial since we want the activity to have access to this even after the start tutorial.

                ViddyUtils.getInstance().getMainActivity().setupGeneralAd(false); // recreate general ad to use general ads only.
                ViddyUtils.getInstance().getMainActivity().enableGeneralAd(); // enable general ads for the user.
                editor.putString("Consent", Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT);
                editor.commit(); // commit changes immediately.

                System.out.println("USER GAVE NON-PERSONAL CONSENT.");
                hasSelectedAdPreferences = true; // the user has selected their ad preferences and thus we no longer need to block the access to the app.
                adPermsText = "Next"; // set the text to be next so they know they have successfully selected their preferences.
                viewPager.enablePaging();
                changeFragment(true);
                viewPager.disablePaging();

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT)) { // if this is reached the user is likely not in the EU and thus GDPR does not matter to these users, tell them that the default settings have been selected for them.

                ViddyUtils.getInstance().getMainActivity().setupGeneralAd(true); // use personal ads.
                ViddyUtils.getInstance().getMainActivity().enableGeneralAd(); // make general ads visable and clickable.
                editor.putString("Consent", Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT);
                editor.commit(); // commit changes immediately.

                new MaterialDialog.Builder(this)
                        .title(R.string.DefaultConsentTitle)
                        .content(R.string.DefaultConsentContent)
                        .positiveText(R.string.DefaultConsentPositive)
                        .positiveColor(ContextCompat.getColor(this, R.color.colorComplimentary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            // closes the dialog and moves to the section permissions section.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                hasSelectedAdPreferences = true; // the user has selected their ad preferences and thus we no longer need to block the access to the app.
                                adPermsText = "Next"; // set the text to be next so they know they have successfully selected their preferences.
                                viewPager.enablePaging();
                                changeFragment(true); // moves to the next fragment.
                                viewPager.disablePaging();
                                dialog.dismiss();
                            }
                        })
                        .show(); // show the dialog to the user.

            }
            else if(consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT)) { // user has been given consent to use the application.

                ViddyUtils.getInstance().getMainActivity().setupGeneralAd(true); // use personal ads.
                ViddyUtils.getInstance().getMainActivity().enableGeneralAd(); // make general ads visable and clickable.
                editor.putString("Consent", Constants.GDPR_CONSENT.PERSONAL_CONSENT);
                editor.commit(); // commit changes immediately.

                // we may begin to do things as normal!
                System.out.println("USER GAVE PERSONAL CONSENT.");
                hasSelectedAdPreferences = true; // the user has selected their ad preferences and thus we no longer need to block the access to the app.
                adPermsText = "Next"; // set the text to be next so they know they have successfully selected their preferences.
                viewPager.enablePaging();
                changeFragment(true);
                viewPager.disablePaging();

            }
            else { // not sure what this would be.
                System.out.println("A DIFFERENT TYPE OF CONSENT IS PROVIDED HERE.");
                System.out.println(gdprConsentState.logString());
            }

        //}
       // GDPRConsent consent = gdprConsentState.getConsent();
        System.out.println("is personal consent = " + consent.isPersonalConsent());
        System.out.println("Consent name = " + consent.name());


        //GDPR.getInstance().showDialog(this, gdprSetup, GDPRLocation.IN_EAA_OR_UNKNOWN); // force the user to choose their consent choices whether they are in the EU or not.
    }

    // if permissions are enabled we need to go to the next area in the app.
    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        hasGrantedPermissions = true; // the user has chosen an option for the permissions, we no longer have to disable swiping.
        permsText = "Next"; // change the permissions text so that the user knows that they have successfully selected the permissions for the app.
        viewPager.enablePaging();
        changeFragment(true);
        //viewPager.disablePaging();
    }

    // if permissions are not enabled we need to still allow the user to finish but warn the app that we will need to ask for permissions if the user tries make a viddy without these.
    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {
        hasGrantedPermissions = true; // the user has chosen an option for the permissions, we no longer have to disable swiping.
        permsText = "Next"; // change the permissions text so that the user knows that they have successfully selected the permissions for the app.
        viewPager.enablePaging();
        changeFragment(true);
        //viewPager.disablePaging();
    }
}
