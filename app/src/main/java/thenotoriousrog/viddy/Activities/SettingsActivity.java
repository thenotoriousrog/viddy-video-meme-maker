package thenotoriousrog.viddy.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.michaelflisar.gdprdialog.GDPR;
import com.michaelflisar.gdprdialog.GDPRConsent;
import com.michaelflisar.gdprdialog.GDPRConsentState;
import com.michaelflisar.gdprdialog.GDPRSetup;
import com.michaelflisar.gdprdialog.helper.GDPRPreperationData;

import mehdi.sakout.aboutpage.AboutPage;
import mehdi.sakout.aboutpage.Element;
import nl.dionsegijn.steppertouch.OnStepCallback;
import nl.dionsegijn.steppertouch.StepperTouch;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;

public class SettingsActivity extends AppCompatActivity implements GDPR.IGDPRCallback {

    private Spinner videoQualitySpinner; // the spinnder that the user will use to select their video quality.
    private boolean userSelectedSpinner = true; // tells us that the user has selected the spinner.
    private RelativeLayout settingMainLayout; // the main layout that holds all of the settings pieces.
    private LinearLayout faq; // the layout that listens for click events from the user.
    private LinearLayout about; // the layout for the about section of the app.
    private LinearLayout feedback; // the layout that the user can click to send feedback
    private LinearLayout rateUs; // the layout for the user to rate us
    private LinearLayout adSettings; // the layout for the ad settings configuration
    private SharedPreferences preferences; // get the context that we need in order to sort these lists.
    private GDPRSetup gdprSetup = null; // the GDPR setup used for the settings activity


    // makes the dialog and shows it when possible to the user.
    public void buildMaterialDialog(int stringsRef) {
        new MaterialDialog.Builder(this)
                .title(R.string.VidQualityDialogTitle)
                .content(stringsRef)
                .positiveText(R.string.VidQualityDialogPositiveText)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss();
                    }
                })
                .positiveColor(ContextCompat.getColor(this, R.color.colorComplimentary))
                .show();
    }

    // sets all aspects of the drop down menu
    public void setupDropDownMenu()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor = preferences.edit(); // creates a shared preferences editor to edit the values within settings.

        int startingVideoQuality = preferences.getInt("VideoQuality", 35); // grab the video quality from the shared preferences.
        System.out.println("Starting video quality is " + startingVideoQuality);

        videoQualitySpinner = findViewById(R.id.videoQualitySpinner);
        // these are the qualities that the user will be able to use.
        String[] qualityTypes = {getString(R.string.VidQualityLowest), getString(R.string.VidQualityLow), getString(R.string.VidQualityMedium),
                getString(R.string.VidQualityHigh), getString(R.string.VidQualityHighest)};
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, qualityTypes);
        spinnerAdapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        videoQualitySpinner.setAdapter(spinnerAdapter);

        userSelectedSpinner = false;
        // set the spinner's starting spot based on the selection from the user's history.
        if (startingVideoQuality == 20) {
            videoQualitySpinner.setSelection(0);
        } else if (startingVideoQuality == 35) {
            videoQualitySpinner.setSelection(1);
        } else if (startingVideoQuality == 50) {
            System.out.println("We are manually setting the spinner selection now!");
            videoQualitySpinner.setSelection(2);
        } else if (startingVideoQuality == 75) {
            videoQualitySpinner.setSelection(3);
        } else if (startingVideoQuality == 100) {
            videoQualitySpinner.setSelection(4);
        } else {
            videoQualitySpinner.setSelection(1);
        }


        // displays a dialog explaining the impacts of each quality setting. Also sets the vid quality in the shared preferences to be used throughout the application.
        videoQualitySpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                if(userSelectedSpinner) { // if the user has changed the selection, change the item.
                    switch (position) {
                        case 0:
                            buildMaterialDialog(R.string.VidQualityLowestDiagContent);
                            editor.putInt("VideoQuality", 20); // set the video quality to be 20%
                            editor.apply(); // write the info in a background thread.
                            break;
                        case 1:
                            buildMaterialDialog(R.string.VidQualityLowDiagContent);
                            editor.putInt("VideoQuality", 35); // set the video quality to be 35%
                            editor.apply(); // write the info in a background thread.
                            break;
                        case 2:
                            buildMaterialDialog(R.string.VidQualityMedDiagContent);
                            editor.putInt("VideoQuality", 50); // set the video quality to be 50%
                            editor.apply(); // write the info in a background thread.
                            break;
                        case 3:
                            buildMaterialDialog(R.string.VidQualityHighDiagContent);
                            editor.putInt("VideoQuality", 75); // set the video quality to be 75%
                            editor.apply(); // write the info in a background thread.
                            break;
                        case 4:
                            buildMaterialDialog(R.string.VidQualityHighestDiagContent);
                            editor.putInt("VideoQuality", 100); // set the video quality to be 100%
                            editor.apply(); // write the info in a background thread.
                            break;

                    }
                } else { // after this case any other time that we enter this method (unless settings are started again) the user has changed the selection.
                    userSelectedSpinner = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                editor.putInt("VideoQuality", startingVideoQuality); // reset the video quality
                editor.apply();
            }
        });
    }

    /*
        * Opens the FAQ Dialog when the user clicks the layout.
     */
    private void setupFAQListener() {
        faq = findViewById(R.id.FAQ);
        faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LinearLayout faqView = new LinearLayout(SettingsActivity.this);
                faqView.setPadding(15,15,15,15); // padding of 10 all around the linear layout.
                faqView.setOrientation(LinearLayout.VERTICAL);
                TextView faqText = new TextView(SettingsActivity.this);

                String faq1 = getString(R.string.FAQ1) + "\n";
                faqText.setText(faq1);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText);

                String faq1a = getString(R.string.FAQ1A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq1a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq2 = getString(R.string.FAQ2) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq2);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq2a = getString(R.string.FAQ2A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq2a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq3 = getString(R.string.FAQ3) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq3);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq3a = getString(R.string.FAQ3A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq3a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq4 = getString(R.string.FAQ4) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq4);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq4a = getString(R.string.FAQ4A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq4a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq5 = getString(R.string.FAQ5) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq5);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq5a = getString(R.string.FAQ5A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq5a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq6 = getString(R.string.FAQ6) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq6);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq6a = getString(R.string.FAQ6A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq6a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq7 = getString(R.string.FAQ7) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq7);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq7a = getString(R.string.FAQ7A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq7a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq8 = getString(R.string.FAQ8) + "\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq8);
                faqText.setTextColor(ContextCompat.getColor(SettingsActivity.this, R.color.black));
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.

                String faq8a = getString(R.string.FAQ8A) + "\n\n";
                faqText = new TextView(SettingsActivity.this);
                faqText.setText(faq8a);
                faqView.addView(faqText); // add the faqText to the faqView for it to be displayed in our material dialog.


                new MaterialDialog.Builder(SettingsActivity.this)
                        .title(R.string.FAQTitle)
                        .customView(faqView, true) // wrap the view in a scroll view if it's too large.
                        .positiveColor(ContextCompat.getColor(SettingsActivity.this, R.color.colorComplimentary))
                        .positiveText(R.string.Close)
                        .onPositive(new MaterialDialog.SingleButtonCallback() {
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                dialog.dismiss(); // close the dialog.
                            }
                        })
                        .show(); // show the dialog.

            }
        });
    }

    /*
        * Opens the About dialog to see credits as well as get useful app information to the user!
     */
    private void setupAboutListener() {
        about = findViewById(R.id.about);
        about.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                try {
                    Element versionElement = new Element();
                    PackageInfo info = SettingsActivity.this.getPackageManager().getPackageInfo(SettingsActivity.this.getPackageName(), 0);
                    versionElement.setTitle(getString(R.string.Version) + " " + info.versionName); // create the version along with the current version name for the element.

                    Element freePikShoutout = new Element();
                    freePikShoutout.setTitle(getString(R.string.FreePikShoutout));
                    freePikShoutout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.freepik.com/")));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                anfe.printStackTrace();
                            }
                        }
                    });

                    Element designDeskShoutout = new Element();
                    designDeskShoutout.setTitle(getString(R.string.DesignDeskShoutout));
                    designDeskShoutout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.fiverr.com/design_desk?source=Order+page+seller+link")));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                anfe.printStackTrace();
                            }
                        }
                    });

                    View aboutPage = new AboutPage(SettingsActivity.this)
                            .isRTL(false)
                            .setImage(R.mipmap.official_icon) // todo: change this icon to show the better logo once it's finished!
                            .setDescription(getString(R.string.ViddyAboutDescription1) + "\n\n" + getString(R.string.ViddyAboutDescription2) + "\n\n" + getString(R.string.ViddyAboutDescription3))
                            .addItem(versionElement) // show the current version of the application.

                            .addEmail("viddysupreme@gmail.com") // our email
                            .addItem(freePikShoutout)
                            .addItem(designDeskShoutout)
                            .create(); // create the about page.

                    new MaterialDialog.Builder(SettingsActivity.this)
                            .title(getString(R.string.About))
                            .customView(aboutPage, true)
                            .positiveText(R.string.Close)
                            .positiveColor(ContextCompat.getColor(SettingsActivity.this, R.color.colorComplimentary))
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                }
                            })
                            .show();
                } catch (PackageManager.NameNotFoundException e) {

                    e.printStackTrace(); // print out the error.

                    // display the notification for the user to know that something went wrong.
                    Snackbar snackbar = Snackbar.make(settingMainLayout, R.string.UnexpectedExceptionMessage, Snackbar.LENGTH_SHORT);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(SettingsActivity.this, R.color.colorComplimentary));
                    sbView.setElevation(5);
                    snackbar.show();
                }

            }
        });
    }

    private void setupFeedbackListener() {
        feedback = findViewById(R.id.feedback);
        feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
                emailIntent.setData(Uri.parse("mailto:viddysupreme@gmail.com"));

                try {
                    startActivity(emailIntent);
                } catch (ActivityNotFoundException ex) {

                    Snackbar snackbar = Snackbar.make(settingMainLayout, R.string.NoEmailAppFoundWarning, Snackbar.LENGTH_SHORT);
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(SettingsActivity.this, R.color.colorComplimentary));
                    sbView.setElevation(5);
                    snackbar.show();

                }


            }
        });
    }

    private void setupRateUsListener() {
        rateUs = findViewById(R.id.rateUs);
        rateUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=thenotoriousrog.viddyz"))); // take the user to our app.
                } catch (android.content.ActivityNotFoundException anfe) { // this should only occur if the user does not have an active internet connection for some reason.
                    anfe.printStackTrace();

                    Snackbar snackbar = Snackbar.make(settingMainLayout, R.string.UnexpectedExceptionMessage, Snackbar.LENGTH_SHORT); // tell the user that they should try again.
                    View sbView = snackbar.getView();
                    sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(SettingsActivity.this, R.color.colorComplimentary));
                    snackbar.show();
                }
            }
        });
    }


    private void setupAdSettingsListener() {
        adSettings = findViewById(R.id.AdSettings);
        adSettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MainActivity mainActivity = ViddyUtils.getInstance().getMainActivity(); // get the main activity instance created at startup.
                gdprSetup = mainActivity.reselectGDPROptions(); // this resets the gdpr setup in main activity and allows the user to check to see if any new information needs to be displayed.
                GDPR.getInstance().checkIfNeedsToBeShown(SettingsActivity.this, gdprSetup);
            }
        });
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_ActionBar); // set the theme with the back button for the action bar.
        super.onCreate(savedInstanceState);

        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); // get the context that we need in order to sort these lists.

        setContentView(R.layout.settings);
        getSupportActionBar().setTitle(getString(R.string.Settings)); // set the title to say settings.
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        settingMainLayout = findViewById(R.id.settingsMainLayout); // set the main layout for the settings to create the settings for the user.
        setupDropDownMenu();
        setupFAQListener();
        setupAboutListener();
        setupFeedbackListener();
        setupRateUsListener();
        setupAdSettingsListener();
        // todo: @Roger, setup the GDPR layout to be blurred out in non-EU countries and to ensure that the EU countries can change the type of meme that they use. This is very important!!
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        finish();
        super.onBackPressed();
    }

    @Override
    public void onConsentNeedsToBeRequested(GDPRPreperationData gdprPreperationData) {
        // the gdpr setup can be null if the user never selects to change their ad preferences therefore we have to make sure that if the gdpr setup is null we don't show the dialog
        if (gdprSetup != null) {
            GDPR.getInstance().showDialog(this, gdprSetup, gdprPreperationData.getLocation());
        }

    }

    @Override
    public void onConsentInfoUpdate(GDPRConsentState gdprConsentState, boolean isNewState)
    {
        SharedPreferences.Editor editor = preferences.edit();

        if (isNewState) // if in a new state and the start screen has been shown, we need to then update the user's information.
        {
            GDPRConsent consent = gdprConsentState.getConsent();
            if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT)) { // The user has chosen to get the paid version of the app instead.
                // todo: add a link to send the user to Viddy+
                Toast.makeText(this, "Viddy+ is under development and will be available soon!", Toast.LENGTH_SHORT).show();
                System.out.println("USER GAVE NO CONSENT.");

                editor.putString("Consent", Constants.GDPR_CONSENT.NO_CONSENT);
                editor.commit();
                finish(); // close the main activity, we cannot show any ads at all thus we need to redirect the user to viddy+ and thus this app is no longer functional.
                ViddyUtils.getInstance().getMainActivity().finish(); // kill the main activity thus closing the app altogether.
                // TODO: handle this. If the user chooses this, we need to then close the app and prevent the user from opening it again unless they buy viddy+, maybe direct them to the website.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // The user has agreed to ads, but not the good kind.

                editor.putString("Consent", Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT);
                editor.commit();

                // NOTE: we may be able to set the user's data manually by sending the information back to the MainActivity. This is pretty crucial since we want the activity to have access to this even after the start tutorial.
                System.out.println("USER GAVE NON-PERSONAL CONSENT.");
            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT)) { // if this is reached the user is likely not in the EU and thus GDPR does not matter to these users, tell them that the default settings have been selected for them.

                editor.putString("Consent", Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT);
                editor.commit();

                new MaterialDialog.Builder(this)
                        .title(R.string.DefaultConsentTitle)
                        .content(R.string.DefaultConsentContent)
                        .positiveText(R.string.DefaultConsentPositive)
                        .positiveColor(ContextCompat.getColor(this, R.color.colorComplimentary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            // closes the dialog and moves to the section permissions section.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                dialog.dismiss();
                            }
                        })
                        .show(); // show the dialog to the user.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT)) { // user gave personal consent we can show personalized ads.

                editor.putString("Consent", Constants.GDPR_CONSENT.PERSONAL_CONSENT);
                editor.commit();

                // we may begin to do things as normal!
                System.out.println("USER GAVE PERSONAL CONSENT.");

            } else { // not sure what this would be.
                System.out.println("A DIFFERENT TYPE OF CONSENT IS PROVIDED HERE.");
                System.out.println(gdprConsentState.logString());
            }
        }
    }
}
