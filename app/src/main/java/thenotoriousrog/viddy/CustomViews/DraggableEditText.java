package thenotoriousrog.viddy.CustomViews;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.RectF;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.AppCompatEditText;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import thenotoriousrog.viddy.Listeners.RotationGestureDetector;
import thenotoriousrog.viddy.Listeners.ScaleListener;
import thenotoriousrog.viddy.R;

/*
    * This is a custom draggable edit text that will allow us to better control the draggable text behavior and have a custom view that we can create.
 */
public class DraggableEditText extends AppCompatEditText{

    private float dX; // holds this draggable text's dX value from user's touch response
    private float dY; // holds this draggable text's dY value from user's touch response.
    private int lastAction; // holds the last action from user's touch response.
    private float scaleFactor = 16.f; // the scale factor to be used for this draggable edit text.
    private float rotationAngle; // the angle at which the view was rotated.
    private ScaleGestureDetector scaleGestureDetector; // the gesture detector for this view.
    private static final int INVALID_POINTER_ID = -1;
    private float fX, fY, sX, sY;
    private int ptrID1, ptrID2;

    public DraggableEditText(Context context) {
        super(context);
        init();
    }

    public DraggableEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DraggableEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    // initializes all of the values for the fields.
    private void init() {
        scaleGestureDetector = new ScaleGestureDetector(getContext(), new ScaleListener(this, scaleFactor));
    }

    // gets the angle between two lines. Very important for rotation.
    private float angleBetweenLines (float fX, float fY, float sX, float sY, float nfX, float nfY, float nsX, float nsY)
    {
        float angle1 = (float) Math.atan2( (fY - sY), (fX - sX) );
        float angle2 = (float) Math.atan2( (nfY - nsY), (nfX - nsX) );

        float angle = ((float)Math.toDegrees(angle1 - angle2)); // % 360;
        if (angle < -180.f) {
            angle += 360.0f;
        }

        if (angle > 180.f) {
            angle -= 360.0f;
        }
        return angle;
    }

    // I could override the on touch event for this drag view and still have it behave in the same way that we would like it too! Todo: test this out and see if we really are able to do that.
    @Override
    public boolean onTouchEvent(MotionEvent event) {

        scaleGestureDetector.onTouchEvent(event); // let the scale gesture listener inspect all events to determine if the user is scaling the edit text or not.

        switch (event.getActionMasked())
        {
            case MotionEvent.ACTION_DOWN:
                System.out.println("TOUCH DETECTED!");

                dX = getX() - event.getRawX();
                dY = getY() - event.getRawY();
                lastAction = MotionEvent.ACTION_DOWN;
                setBackground(ContextCompat.getDrawable(getContext(), R.drawable.viewhighlight));
                ptrID1 = event.getPointerId(event.getActionIndex());
                break;

            case MotionEvent.ACTION_POINTER_DOWN:
                ptrID2 = event.getPointerId(event.getActionIndex());
                sX = event.getX(event.findPointerIndex(ptrID1));
                sY = event.getY(event.findPointerIndex(ptrID1));
                fX = event.getX(event.findPointerIndex(ptrID2));
                fY = event.getY(event.findPointerIndex(ptrID2));
                break;

            case MotionEvent.ACTION_MOVE:
                System.out.println("MOVING DETECTED!");
                System.out.println("dY = " + dY);
                System.out.println("dX = " + dX);

                int newX = (int)(getX() - event.getRawX());
                int newY = (int)(getY() - event.getRawY());

                System.out.println("newX = " + newX);
                System.out.println("newY = " + newY);

                setY(event.getRawY() + dY);
                setX(event.getRawX() + dX);

                System.out.println("RawX = " + event.getRawX());
                System.out.println("RawY = " + event.getRawY());

                int tempX = (int)(Math.abs(newX) - Math.abs(dX));
                int tempY = (int)(Math.abs(newY) - Math.abs(dY));

                System.out.println("tempX = " + tempX);
                System.out.println("tempY = " + tempY);


                // make it easier for the user to trigger the keyboard even with micro moves.
                if(tempX > 5 || tempY > 5) {
                    lastAction = MotionEvent.ACTION_MOVE;
                }


                if(ptrID1 != INVALID_POINTER_ID && ptrID2 != INVALID_POINTER_ID && event.getPointerCount() >= 2) { // only listen for rotation when they're are more then two or more touch pointers.

                    float nfX, nfY, nsX, nsY;
                    nsX = event.getX(event.findPointerIndex(ptrID1));
                    nsY = event.getY(event.findPointerIndex(ptrID1));
                    nfX = event.getX(event.findPointerIndex(ptrID2));
                    nfY = event.getY(event.findPointerIndex(ptrID2));

                    float newRotationAngle = angleBetweenLines(fX, fY, sX, sY, nfX, nfY, nsX, nsY); // get the angle rotation for the view.
                    rotationAngle = rotationAngle - newRotationAngle; // adjust the new angle from the old angle to get the most accurate rotation of this new view.

                    fX = nfX;
                    fY = nfY;
                    sX = nfX;
                    sY = nfY;

                    setRotation(rotationAngle); // set the view to show the proper rotation.
                }
                break;

            case MotionEvent.ACTION_UP:
                System.out.println("USER RELEASED TOUCH!");
                setBackground(null); // remove the background since the user is no longer touching the text field.

                ptrID1 = INVALID_POINTER_ID;

                if (lastAction == MotionEvent.ACTION_DOWN) {
                    // if last action was down then we have received a click response. We need to handle this behavior to the best of our ability in here including starting a edit text option.
                    System.out.println("CLICK DETECTED!");
                    requestFocus();
                    performClick();

                    // todo: this controls clicking and we should show the soft touch keyboard here
                    // this is needed because there is something stupid happening where the edit text field doesn't trigger the keyboard. So we manually do it.
                    InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                    if (imm != null ) { // if not null, have the soft keyboard show up.
                        imm.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT);
                    }
                }


                break;

            case MotionEvent.ACTION_POINTER_UP:
                ptrID2 = INVALID_POINTER_ID;
                break;
            case MotionEvent.ACTION_CANCEL:
                ptrID1 = INVALID_POINTER_ID;
                ptrID2 = INVALID_POINTER_ID;
                break;

            default:
                return false;
        }
        return true;
    }

    @Override
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        canvas.save();
        canvas.scale(scaleFactor, scaleFactor);
        canvas.rotate(rotationAngle);

        setTextSize(scaleFactor);

        // any additional onDraw() code would go here if we wanted to create a more custom edit text.

        canvas.restore();
    }

    public void setScaleFactor(float scaleFactor) {
        this.scaleFactor = scaleFactor;
    }
}
