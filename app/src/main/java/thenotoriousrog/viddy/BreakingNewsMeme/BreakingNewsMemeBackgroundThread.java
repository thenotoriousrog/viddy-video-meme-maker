package thenotoriousrog.viddy.BreakingNewsMeme;

import android.graphics.Bitmap;
import android.os.Environment;
import android.widget.RelativeLayout;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import thenotoriousrog.viddy.BreakingNewsMeme.BreakingNewsMemeStep2Fragment;
import thenotoriousrog.viddy.R;

public class BreakingNewsMemeBackgroundThread implements Runnable {

    private RelativeLayout preview; // the preview with all of our text fields.
    private BreakingNewsMemeStep2Fragment breakingNewsMemeStep2Fragment;
    private Thread backgroundThread;
    private String filename;

    public BreakingNewsMemeBackgroundThread(RelativeLayout preview, BreakingNewsMemeStep2Fragment breakingNewsMemeStep2Fragment, String filename)
    {
        this.preview = preview;
        this.breakingNewsMemeStep2Fragment = breakingNewsMemeStep2Fragment;
        backgroundThread = new Thread(this);

        if(filename == null || filename.trim().isEmpty()) {
            this.filename = "BreakingNewsMeme";
        } else {
            this.filename = filename;
        }

    }

    public void startBackgroundThread() {
        backgroundThread.start();
    }

    /*
     * Saves a bitmap to internal storage.
     * Code retrieved from stackoverflow: https://stackoverflow.com/questions/17674634/saving-and-reading-bitmaps-images-from-internal-memory-in-android -> by Brijesh Thakur.
     * Modified to fit our use case.
     */
    private File saveToInternalStorage(Bitmap breakingNewsMeme) {
        File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/"); // a temporary folder to hold video frames.
        if (!root.exists()) {
            root.mkdirs(); // make all the directories.
        } else {
            root.delete(); // the directory exists... remove it
            root.mkdirs(); // remake the directories.
        }

        System.out.println("The filename used to create the breaking news meme is: " + filename);

        // Create standard meme path.
        File path = new File(root, filename + ".jpg"); // create the new file and append with .jpg so that the image is properly setup.

        FileOutputStream fos = null;
        try {

            // create the file if it doesnt exist otherwise delete the file and create the file.
            if(!path.exists()) {
                path.createNewFile();
            } else {
                path.delete();
                path.createNewFile();
            }

            fos = new FileOutputStream(path);

            // Use the compress method on the BitMap object to write image to the OutputStream
            breakingNewsMeme.compress(Bitmap.CompressFormat.JPEG, 100, fos); // we always want to write the image with 100% video quality.

        } catch (Exception e) { // todo: add error catching in here so that we can alert the user if something went wrong.
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path; // return the path of the meme
    }

    @Override
    public void run() {

        Bitmap standardMeme = preview.getDrawingCache(); // build the layout using the drawing cache.
        File memeFile = saveToInternalStorage(standardMeme); // write the meme to internal storage.

        String errorMsg = ""; // no error msg at all.
        if (memeFile == null) { // an error occurred.
            errorMsg = breakingNewsMemeStep2Fragment.getString(R.string.FloatingTextNullMemeFile);
            breakingNewsMemeStep2Fragment.displayBreakingNewsMemeStep3Fragment(memeFile.getAbsolutePath(), errorMsg); // display the final fragment.
        } else { // no error
            breakingNewsMemeStep2Fragment.displayBreakingNewsMemeStep3Fragment(memeFile.getAbsolutePath(), errorMsg); // display the final fragment.
        }

    }

}

