package thenotoriousrog.viddy.QuoteMeme;

import android.Manifest;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.michaelflisar.gdprdialog.GDPR;

import java.util.ArrayList;

import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.CustomViews.DraggableEditText;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeActivity;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeBackgroundThread;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeStep2Fragment;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeStep3Fragment;
import thenotoriousrog.viddy.R;

public class QuoteMemeStep2Fragment extends Fragment {

    private QuoteMemeActivity quoteMemeActivity;
    private ImageView imageView;
    private String photoPath;
    private String realPath;
    private int textSizeVal = 12; // the starting value is always 12
    private boolean withWatermark = true; // add the watermark to the image.
    private boolean removeWatermarkAdWatched = false; // this will be set true upon watching the ad then they can add their watermark!
    private volatile boolean addAdditionalTextFieldsAdWatched = false; // this tell the system that the user has watched an Ad to add more text fields.
    private boolean finishedAddAdditionalTextFieldsAd = false; // tells the system that the user has finished watching an ad. It's no longer necessary to ensure the user hasn't stopped the ad early.
    private volatile int textFieldCount = 0; // the number of active floating texts that the user has for this meme.
    private ArrayList<DraggableEditText> textFields = new ArrayList<>();
    private RewardedVideoAd rewardedVideoAd; // the add that the user can watch in order to unlock features within this activity.
    private View mainView;
    private RelativeLayout mainContentLayout; // the layout that holds all of the goods.
    private boolean readyForStep3 = false; // tells the fragment that we are ready to go to step 3.
    private ProgressBar progressBar; // the progress bar to show that the app is running.
    private RelativeLayout preview; // the meme preview for the image that the user is looking at.
    private TextView viddyWatermarkText; // the viddy watermark itself.
    private Switch invertTextColorSwitch; // the switch that the users can do to invert the colors of the text.
    private boolean isRewarded = false; // tells the system if they successfully watched an ad or not.
    private String filename;

    public void setFields(QuoteMemeActivity quoteMemeActivity, String photoPath, String realPath) {
        this.quoteMemeActivity = quoteMemeActivity;
        this.photoPath = photoPath;
        this.realPath = realPath;
        quoteMemeActivity.displayEditFilenameIcon();
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // increments our text field counter.
    private synchronized void incrementTextFieldCount() {
        textFieldCount++;
    }

    // hides the main content and shows the progress loader.
    public void showProgressBar() {
        mainContentLayout.setVisibility(View.GONE); // remove the main layout.
        progressBar.setVisibility(View.VISIBLE); // make the progress bar visible.
    }

    public void displayQuoteMemeStep3Fragment(String quoteMemePath, String errorMsg) {

        quoteMemeActivity.changeCanGoBackState(false);
        QuoteMemeStep3Fragment quoteMemeStep3Fragment = new QuoteMemeStep3Fragment();

        if (errorMsg.isEmpty()) { // if empty, no error to worry about. Send in an empty error which shows the correct file location.
            quoteMemeStep3Fragment.setFields(quoteMemeActivity, quoteMemePath, "");
        } else {
            quoteMemeStep3Fragment.setFields(quoteMemeActivity, quoteMemePath, errorMsg);
        }

        //standardMemeStep3Fragment.setFields(standardMemeActivity, standardMemePath, "");
        FragmentTransaction transaction = quoteMemeActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.mainFrameLayout, quoteMemeStep3Fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        quoteMemeActivity.displayShareIcon(quoteMemePath); // display the share icon and set the path to the created meme!
    }

    // sets up the listener to be able to watch the rewarded video ad.
    // todo: I need to add the behavior for showing an add after the user wants to have more than 2 floating text entries!
    private void setupRewardedVideoAd(View mainView) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // get the context that we need in order to sort these lists.
        SharedPreferences.Editor editor = preferences.edit();

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getContext()); // set our ad.
        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewarded(RewardItem reward) {

                isRewarded = true; // user finished watching an ad.


            }

            @Override
            public void onRewardedVideoAdLeftApplication() { }

            @Override
            public void onRewardedVideoAdClosed() {
                // Toast.makeText(getBaseContext(), "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();

                if(isRewarded) {
                    Toast toast = new Toast(getContext());
                    View toastView = getLayoutInflater().inflate(R.layout.qm_watermark_not_added_toast, (ViewGroup) mainView.findViewById(R.id.watermarkNotAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
                    toast.setView(toastView);
                    toast.show();

                    viddyWatermarkText.setVisibility(View.GONE); // the user finished the ad, we can now remove the watermark so they can make their meme.

                    // create meme without the Viddy watermark.
                    QuoteMemeBackgroundThread backgroundThread = new QuoteMemeBackgroundThread(preview, QuoteMemeStep2Fragment.this, filename);
                    showProgressBar();
                    backgroundThread.startBackgroundThread();
                    quoteMemeActivity.notifyFragmentIndicator(2); // show the third item to be highlighted.
                    quoteMemeActivity.changeCanGoBackState(false); // the user cannot go back now.
                }

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                CoordinatorLayout snackbarLocation = mainView.findViewById(R.id.snackbarLocation);
                Snackbar snackbar = Snackbar.make(snackbarLocation, R.string.Ad_Not_Loading, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(getContext(), R.color.QuoteMemeColorComplimentary));
                sbView.setElevation(5);
                snackbar.show();
            }

            // listens for when the ad is loaded.
            @Override
            public void onRewardedVideoAdLoaded() {
                rewardedVideoAd.show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                isRewarded = true;
            }

            @Override
            public void onRewardedVideoStarted() {}

            @Override
            public void onRewardedVideoCompleted() { }
        });
    }

    private void setupStep3Button(View mainView, String userConsent) {
        final Button step3Button = mainView.findViewById(R.id.step3Button);
        step3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // if they choose no, display a full screen ad, after the ad the activity will generate the meme without a watermark.
                // if they choose to keep the watermark then immediately generate the meme with the watermark of the app. Very important!
                String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                if (EasyPermissions.hasPermissions(getContext(), perms)) // permissions available, proceed as normal.
                {
                    QuoteMemeBackgroundThread backgroundThread = new QuoteMemeBackgroundThread(preview, QuoteMemeStep2Fragment.this, filename);
                    showProgressBar();
                    backgroundThread.startBackgroundThread();
                    quoteMemeActivity.notifyFragmentIndicator(2); // show the third item to be highlighted.
                    quoteMemeActivity.changeCanGoBackState(false); // the user cannot go back now.

                } else { // permissions not granted, we need to get them as soon as possible.

                    String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}; // just need to read/write storage android.
                    EasyPermissions.requestPermissions(
                            new PermissionRequest.Builder(quoteMemeActivity, Constants.ACTION.REQUEST_PERMISSIONS, permissions)
                                    .setRationale(R.string.PermissionsRationale)
                                    .setPositiveButtonText(R.string.PermissionRationaleOk)
                                    .setNegativeButtonText(R.string.PermissionRationaleCancel)
                                    .build());
                }
            }
        });
    }

    private void setupAddFloatingTextButton(View mainView, String userConsent) {
        FloatingActionButton addFloatingTextButton = mainView.findViewById(R.id.addFloatingTextButton);
        addFloatingTextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                System.out.println("Was an ad watched to allow for unlimited text fields? " + addAdditionalTextFieldsAdWatched);

                incrementTextFieldCount(); // increments our text field counter.
                DraggableEditText newTextField = new DraggableEditText(getContext());
                RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                newTextField.setLayoutParams(params); // ensure's that the edit text field is made to wrap content and what not.
                newTextField.setId(View.generateViewId()); // generate a unique id for this view.
                newTextField.setFocusableInTouchMode(true);
                newTextField.setHint(getString(R.string.TextFieldHint));
                newTextField.setFitsSystemWindows(true);
                if(invertTextColorSwitch.isChecked()) {
                    newTextField.setTextColor(ContextCompat.getColor(quoteMemeActivity, R.color.white));
                }
                newTextField.setTypeface(newTextField.getTypeface(), Typeface.BOLD_ITALIC);
                //Typeface font = quoteMemeActivity.getResources().getFont(R.font.journal); // *** removed this as we need a more compat way to get the font for API < 26 ~ Roger
                Typeface font = Typeface.createFromAsset(quoteMemeActivity.getAssets(), "fonts/journal.ttf");
                newTextField.setTypeface(font);

                textFields.add(newTextField); // add text field to the list of fields.
                preview.addView(newTextField); // add text field to the view.

                // redraw the preview to show the new Edit Text Field.
                preview.invalidate();
                preview.requestLayout();

            }
        });
    }

    // sets up the behavior of the invert color switch.
    private void setupInvertTextColorSwitch(View mainView) {
        invertTextColorSwitch = mainView.findViewById(R.id.invertTextColorSwitch);
        invertTextColorSwitch.setOnClickListener(new View.OnClickListener() {

            // if checked, turn all the text colors to white, if unchecked, turn them all black.
            @Override
            public void onClick(View v) {

                for (int i = 0; i < textFields.size(); i++) {

                    if(invertTextColorSwitch.isChecked()) {
                        System.out.println("MAKING ALL TEXT FIELDS WHITE");
                        textFields.get(i).setTextColor(ContextCompat.getColor(quoteMemeActivity, R.color.white)); // set the text to be white.
                    } else {
                        System.out.println("MAKING ALL TEXT FIELDS BLACK!!");
                        textFields.get(i).setTextColor(ContextCompat.getColor(quoteMemeActivity, R.color.black)); // set the text to be black.
                    }
                }
            }
        });
    }

    // Deletes the newest text field that was last created.
    public void deleteLastField() {

        // if not empty, remove the latest text fields.
        if(!textFields.isEmpty()) {

            preview.removeView(textFields.get(textFields.size()-1)); // remove the view from the meme preview.
            textFields.remove(textFields.size()-1); // remove the last text field.
        }
        else { // alert the user that there are no more text fields to delete if they try pressing the button.
            CoordinatorLayout snackbarLocation = mainView.findViewById(R.id.snackbarLocation);
            Snackbar snackbar = Snackbar.make(snackbarLocation, R.string.EmptyTextFieldsMessage, Snackbar.LENGTH_SHORT);
            View sbView = snackbar.getView();
            sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(getContext(), R.color.QuoteMemeColorComplimentary));
            sbView.setElevation(5);
            snackbar.show();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.quote_meme_step2fragment, container, false); // inflate step1 fragment

        String userConsent = GDPR.getInstance().getConsentState().getConsent().name(); // get the consent type from the user.

        setupRewardedVideoAd(mainView);
        setupStep3Button(mainView, userConsent);
        setupAddFloatingTextButton(mainView, userConsent);
        setupInvertTextColorSwitch(mainView);

        progressBar = mainView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE); // ensure that the progress bar is gone

        mainContentLayout = mainView.findViewById(R.id.quoteMemeStep2MainLayout);
        preview = mainView.findViewById(R.id.memePreviewStep2); // the meme preview for the image.
        preview.setDrawingCacheEnabled(true);

        imageView = mainView.findViewById(R.id.memeViewStep2);
        imageView.setImageURI(Uri.parse(photoPath));

        viddyWatermarkText = mainView.findViewById(R.id.viddyWatermarkText);

        return mainView;
    }
}
