package thenotoriousrog.viddy.QuoteMeme;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.michaelflisar.gdprdialog.GDPR;
import com.michaelflisar.gdprdialog.GDPRConsent;
import com.michaelflisar.gdprdialog.GDPRConsentState;
import com.michaelflisar.gdprdialog.GDPRDefinitions;
import com.michaelflisar.gdprdialog.GDPRSetup;
import com.michaelflisar.gdprdialog.helper.GDPRPreperationData;

import java.io.File;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Activities.SettingsActivity;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.RealPathUtil;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.BreakingNewsMeme.BreakingNewsMemeActivity;
import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsViddyActivity;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeActivity;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeStep1Fragment;
import thenotoriousrog.viddy.FloatingTextMeme.FloatingTextMemeStep2Fragment;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.StandardMeme.StandardMemeActivity;

public class QuoteMemeActivity extends AppCompatActivity implements GDPR.IGDPRCallback {

    private ImageView imageView; // the video view that the user will see.
    private String photoPath = null; // holds the Uri of the video that the user has selected.
    private String realPath = null; // holds the real system level path for us to be able to find the actual video.
    private SharedPreferences preferences; // get the context that we need in order to sort these lists.
    private GDPRSetup gdprSetup; // the gdpr setup we are using for the overall system.
    private boolean finishedStartScreen = false; // tells the main activity that the user has finished the start screen.
    private AdView generalAdView; // the general ad for the Ad view.
    private ProgressBar progressBar; // the progress bar that we are going to be using
    private RelativeLayout mainLayout; // the main layout for the MainUIActivity.
    private DrawerLayout mainDrawerLayout; // the main drawer layout which everything is contained in.
    private boolean isDownloadedVideo = false; // tells the activity that the user has chosen to use a link. If they have we need to delete the video after they have used it.
    private boolean isDrawerOpen = false; // tells us if the drawer is open or not.
    private FragmentTransaction mainFragmentTransaction; // the fragment transaction that we will be using.
    private FrameLayout mainFrameLayout; // the frame layout that we will swap out fragment throughout the application.
    private LinearLayout fragmentIndicator; // this shows the steps on the stepper on the stop of the app.
    private Fragment currentFragment; // the current fragment that is open.
    private QuoteMemeStep1Fragment quoteMemeStep1Fragment;
    private QuoteMemeStep2Fragment quoteMemeStep2Fragment;
    private boolean canGoBack = false; // true when we can go back to a previous fragment, false when going back isn't possible.
    private Uri finalProduct; // the final product for this meme.
    private boolean showShareIcon = false; // tells the system that we can show the share icon now!
    private boolean showEditFileNameButton = false;

    // sets up the GDPR instance to be shown in a little bit.
    private void setupGDPR()
    {
        gdprSetup = new GDPRSetup(GDPRDefinitions.ADMOB)
                .withAllowNoConsent(true) // required by law. This should be here, people have the right to say no
                .withPaidVersion(true)
                .withExplicitAgeConfirmation(false) // if the user accepts they are agreeing to be of the age of 16
                .withCheckRequestLocation(true)
                .withCheckRequestLocationFallbacks(true, true)
                .withCheckRequestLocationTimeouts(3000, 3000) // fail after 3 seconds each time.
                .withBottomSheet(false)
                .withForceSelection(true)
                .withShortQuestion(true)
                .withLoadAdMobNetworks("pub-1014366431079942") // publisher ID for Viddy
                .withNoToolbarTheme(true);
    }

    // creates the fragment indicator on the top of the viddy creation.
    public void notifyFragmentIndicator(int itemSelected) {

        if (fragmentIndicator.getChildCount() > 0) {
            fragmentIndicator.removeAllViews();
        }

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        layoutParams.setMarginEnd(15);
        // loop through creating the text views and setting the text. If the item is selected it shows a filled version of the background instead.
        for(int i = 0; i < 3; i++)
        {
            TextView textView = new TextView(this);
            textView.setText(Integer.toString(i+1));
            // textView.setTextColor(ContextCompat.getColor(this, R.color.white)); // make the text white.
//            textView.setPadding(4,0,4,0);
            textView.setGravity(Gravity.CENTER);
            textView.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);
            if (i == itemSelected) {
                textView.setBackgroundResource(R.drawable.viddy_fragment_indicator_selected);
                textView.setTextColor(ContextCompat.getColor(this, R.color.white)); // make the text white.
            } else {
                textView.setBackgroundResource(R.drawable.viddy_fragment_indicator_unselected);
                textView.setTextColor(ContextCompat.getColor(this, R.color.black)); // make the text white.
            }

            fragmentIndicator.addView(textView, layoutParams);
        }
    }

    // if true, can go back, false, otherwise.
    public void changeCanGoBackState(boolean state) {
        canGoBack = state;
    }

    // makes the general ad invisible and not clickable or focusable..
    public void disableGeneralAd() {
        generalAdView.setVisibility(View.INVISIBLE);
        generalAdView.setClickable(false);
        generalAdView.setFocusable(false);
    }

    // makes general ad visible and clickable and focusable.
    public void enableGeneralAd() {
        generalAdView.setVisibility(View.VISIBLE);
        generalAdView.setClickable(true);
        generalAdView.setFocusable(true);
    }

    public void displayEditFilenameIcon() {
        showEditFileNameButton = true;
        invalidateOptionsMenu();
    }

    public void hideEditFilenameIcon() {
        showEditFileNameButton = false;
        invalidateOptionsMenu();
    }

    // builds and loads the ads.
    public void setupGeneralAd(boolean usePersonalizedAds)
    {
        if (!usePersonalizedAds) // if we are not using personalized ads i.e. non pesonal ads only, we must report this to google.
        {
            Bundle extras = new Bundle();
            extras.putString("npa", "1");

            // build request with the report to admob that they must not be personalized.
            AdRequest request = new AdRequest.Builder()
                    .addNetworkExtrasBundle(AdMobAdapter.class, extras)
                    .build();

            // find, build, and load general ad.
            generalAdView = findViewById(R.id.generalAd);
            generalAdView.loadAd(request);

        } else { // create ads like normal; we can use personalized ads.

            AdRequest request = new AdRequest.Builder().build();

            // find, build, and load general ad.
            generalAdView = findViewById(R.id.generalAd);
            generalAdView.loadAd(request);
        }
    }

    // sets up the drawer layout to be used throughout the rest of the app.
    public void setupDrawerLayout()
    {
        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.bringToFront();
        //navigationView.getHeaderView(0).setBackgroundColor(ContextCompat.getColor(this, R.color.QuoteMemeColorPrimary));
        RelativeLayout navLayout = (RelativeLayout) navigationView.getHeaderView(0); // convert to a linear layout.
        //ImageView navImage = navLayout.findViewById(R.id.viddyHeaderIcon);
        // navImage.setImageResource(R.drawable.viddy_logo_purple); // set's the purple logo for the image.
        navigationView.setCheckedItem(R.id.CreateQuoteMeme); // create Quote meme is initially selected.
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        menuItem.setChecked(false); // set item as selected to persist highlight
                        mainDrawerLayout.closeDrawers(); // close drawer when item is tapped

                        if (menuItem.getTitle().toString().equalsIgnoreCase("Settings")) {

                            Intent settingsIntent = new Intent(QuoteMemeActivity.this, SettingsActivity.class);
                            startActivity(settingsIntent);

                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Viddy")) {

                            Intent mainActivityIntent = new Intent(QuoteMemeActivity.this, MainActivity.class);
                            mainActivityIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK); // removes all activities from the back stack.
                            startActivity(mainActivityIntent);
                            menuItem.setChecked(true);
                            finish(); // kill the activity so that it doesn't show up when we select a different meme format.

                            //finish(); // Note: this isn't obvious but since the MainActivity is the very first activity, but killing this activity we revert back to the original item only.
                        }
                        else if (menuItem.getTitle().toString().equalsIgnoreCase("Standard Meme")) {
                            Intent standardMemeActivity = new Intent(QuoteMemeActivity.this, StandardMemeActivity.class);
                            standardMemeActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(standardMemeActivity);
                            menuItem.setChecked(true);
                            finish(); // kill this activity.
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Floating Text Meme")) {

                            Intent floatingTextMemeActivity = new Intent(QuoteMemeActivity.this, FloatingTextMemeActivity.class);
                            floatingTextMemeActivity.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(floatingTextMemeActivity);
                            menuItem.setChecked(true);
                            finish(); // kill this activity.
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Breaking News Viddy")) {
                            Intent breakingNewsViddyIntent = new Intent(QuoteMemeActivity.this, BreakingNewsViddyActivity.class);
                            breakingNewsViddyIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(breakingNewsViddyIntent);
                            menuItem.setChecked(true);
                            finish();
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Quote Meme"))
                        {
                            // Activity is already open, nothing is going to happen
                        }
                        else if(menuItem.getTitle().toString().equalsIgnoreCase("Breaking News Meme")) {
                            Intent breakingNewsMemeIntent = new Intent(QuoteMemeActivity.this, BreakingNewsMemeActivity.class);
                            breakingNewsMemeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(breakingNewsMemeIntent);
                            menuItem.setChecked(true);
                            finish();
                        }

                        return true;
                    }
                });

        mainDrawerLayout.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) { }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) { // change the state of the drawer being opened.
                isDrawerOpen = true;
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {
                isDrawerOpen = false;
            }

            @Override
            public void onDrawerStateChanged(int newState) { }
        });
    }

    // redraws the options showing the share icon.
    public void displayShareIcon(String createdProduct) {

        // Note: the below two lines are needed for sharing. I'm not sure why, but somehow this grants access to the file system allowing the user to share stuff quite quickly!
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        finalProduct = Uri.fromFile(new File(createdProduct));
        showShareIcon = true;
        invalidateOptionsMenu(); // forces the options menu to redraw showing our share icon!
    }

    private void showEditFileNameDialog() {

        TextInputEditText editText = new TextInputEditText(this);
        editText.setPadding(10,5,5,5);
        editText.setBackgroundColor(ContextCompat.getColor(this, R.color.white));
        editText.setTextColor(ContextCompat.getColor(this, R.color.black));
        editText.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);

        new MaterialDialog.Builder(this)
                .title(R.string.ChangeMemeFileName)
                .customView(editText, false)
                .positiveText(R.string.Confirm)
                .onPositive(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        quoteMemeStep2Fragment.setFilename(editText.getText().toString());
                        dialog.dismiss(); // close the dialog.

                    }
                })
                .negativeText(R.string.Cancel)
                .onNegative(new MaterialDialog.SingleButtonCallback() {
                    @Override
                    public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                        dialog.dismiss(); // simply close the dialog if cancel is pressed.
                    }
                })
                .show();
    }

    // inflates the menu and adds items in the menu upon starting the creation of the app.
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);

        if(!canGoBack) { // if we can't go back, then hide the delete button. Nothing to delete!
            MenuItem item = menu.findItem(R.id.action_delete);
            item.setVisible(false); // no longer make this item visible.
        }

        if(!showShareIcon) { // if not showing the share icon, hide it!
            MenuItem item = menu.findItem(R.id.action_share);
            item.setVisible(false); // no longer make this item visible.
        }

        if(!showEditFileNameButton) {
            MenuItem item = menu.findItem(R.id.action_edit_file_name);
            item.setVisible(false);

        }

        MenuItem item = menu.findItem(R.id.action_edit_trimmed_vid_name);
        item.setVisible(false);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mainDrawerLayout.openDrawer(GravityCompat.START);
                return true;

            case R.id.action_delete: // the user wants to delete a floating text field.
                quoteMemeStep2Fragment.deleteLastField(); // delete the last created floating text field that the user created.
                return true;

            case R.id.action_edit_file_name:
                showEditFileNameDialog();
                return true;

            case R.id.action_share:
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.putExtra(Intent.EXTRA_STREAM, finalProduct);
                shareIntent.setType("image/jpg");
                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.SendTo)));
                return true;

        }
        return super.onOptionsItemSelected(item);
    }


    // displays the second viddy fragment.
    public void displayQuoteMemeStep2Fragment() {
        quoteMemeStep2Fragment = new QuoteMemeStep2Fragment();
        quoteMemeStep2Fragment.setFields(this, photoPath, realPath);
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mainFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        mainFragmentTransaction.replace(R.id.mainFrameLayout, quoteMemeStep2Fragment);
        mainFragmentTransaction.addToBackStack(null);
        mainFragmentTransaction.commit();
        currentFragment = quoteMemeStep2Fragment;
        canGoBack = true;
        if (getSupportActionBar() != null) { // have the action bar be set as up enabled.
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            invalidateOptionsMenu(); // force the options menu to redraw. The reason for this is to cause the menu to redraw for us to hide the item
        }
    }

    public void restartQuoteMemeStep1Fragment() {
        quoteMemeStep1Fragment = new QuoteMemeStep1Fragment();
        quoteMemeStep1Fragment.setFields(this);
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();
        mainFragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        mainFragmentTransaction.replace(R.id.mainFrameLayout, quoteMemeStep1Fragment);
        mainFragmentTransaction.addToBackStack(null);
        mainFragmentTransaction.commit();
        notifyFragmentIndicator(0); // re-highlight the step 1 fragment.
        currentFragment = quoteMemeStep1Fragment;
        canGoBack = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        setTheme(R.style.AppTheme_QuoteMeme); // Green style theme
        super.onCreate(savedInstanceState);

        GDPR.getInstance().init(this);

        // This is our AdMob ID. This is needed to display ads throughout the application.
        MobileAds.initialize(this, "ca-app-pub-1014366431079942~9743624356"); // our real admob id.

        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext()); // get the context that we need in order to sort these lists.

        setContentView(R.layout.quote_meme_activity); // this is the layout for creating a new Viddy.
        // No other activity in progress, we may continue on.

        mainDrawerLayout = findViewById(R.id.mainDrawerLayout);
        mainFrameLayout = findViewById(R.id.mainFrameLayout);
        mainFragmentTransaction = getSupportFragmentManager().beginTransaction();  // the fragment transaction that we are using throughout the rest of the application.
        fragmentIndicator = findViewById(R.id.fragmentsIndicatorLayout);
        notifyFragmentIndicator(0); // makes the first item selected.
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("Quote Meme");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_white_24dp);

        setupGDPR();
        setupGeneralAd(false); // initally setup the general to show non personal ads so that no data is collected during start up.
        disableGeneralAd(); // disable the general ad so that the user does not see ads by accident until after they have chosen their ad preferences.
        setupDrawerLayout(); // sets up the drawer layout to handle the other UI behavior.

        String userConsent = preferences.getString("Consent", null);

        // fixme: the user should be immediately taken to viddy+ and not show any ads.
        if (userConsent != null  && userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT) ) // if not null and no consent, we need to direct the user to Viddy+
        {
            setupGeneralAd(false); // setup the ad to not show personal ads to prevent any data collection.
            disableGeneralAd(); // disable the general ad so that the user cannot click on it and it's not visible.
            Toast.makeText(this, "Viddy+ is under developement, when available, we will take you immediately to it when you open the app!", Toast.LENGTH_SHORT).show();
            finish(); // kill the activity
        }
        else if (userConsent != null && userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // ads are granted, but non personal only.
            setupGeneralAd(false); // ensure that all ads that are created are now personalized.
            enableGeneralAd(); // make the ad visible and clickable now.
        }
        // user has agreed to show personal ads, or they are not from the EU thus personal ads are automatically granted.
        else if (userConsent != null && (userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT) || userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT))) {
            setupGeneralAd(true); // setup the general ad to show personalized ads.
            enableGeneralAd(); // make the ad visible and clickable.
        }

        GDPR.getInstance().checkIfNeedsToBeShown(this, gdprSetup); // check if we have to show the GDPR again for the user or not.

        quoteMemeStep1Fragment = new QuoteMemeStep1Fragment();
        quoteMemeStep1Fragment.setFields(this); // pass in the QuoteMemeActivity field.
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrameLayout, quoteMemeStep1Fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }

    // Checks the extension of the video format and determines if it's a valid format that can be played with ImageView.
    // Returns true if it is. Returns false if not.
    private boolean isSupportedContent(String realPath) {

        String ext = realPath.substring(realPath.length()-4, realPath.length()); // check for the standard 3 letter extension.

        System.out.println("The 3 letter extension we're looking at: " + ext);

        // if any of these return true
        if(ext.equalsIgnoreCase(".bmp") || ext.equalsIgnoreCase(".jpg") || ext.equalsIgnoreCase(".png") ||
                ext.equalsIgnoreCase(".gif") || ext.equalsIgnoreCase(".bin")) {
            return true; // return true we have truly real content that we can use!
        }

        ext = realPath.substring(realPath.length()-5, realPath.length()); // check for non-standard 4 letter valid extensions.

        System.out.println("The 4 letter extension we're looking at: " + ext);

        // .webm is a valid format created by Google. Thus, return true.
        if (ext.equalsIgnoreCase(".webp") || ext.equalsIgnoreCase(".jpeg")) {
            return true;
        }

        return false; // if we have reached here then we have not returned found a real format and thus we can return false.
    }

    // when the user returns from picking a veme we can begin displaying our video!
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intentResult)
    {
        if(intentResult == null) {
            return; // immediately leave this method, the user did not select a file to use.
        }

        if(requestCode == Constants.INTENT.PICK_FILE) // the user has selected a file.
        {
            final Uri photoUri = intentResult.getData(); // get the video uri that the user has chosen.
            photoPath = photoUri.toString(); // set the video Uri that the user has chosen to make use of.
            isDownloadedVideo = false; // make false so that the video is not deleted after the viddy is created.

            try {
                realPath = RealPathUtil.getRealPath(this, photoUri);
            } catch(NumberFormatException nfe) { // this occurs when we get something super weird.
                nfe.printStackTrace();
                realPath = null; // note: writing this to be null as it will be caught later
            }

            System.out.println("True real Path: " + realPath);

            if(realPath != null && isSupportedContent(realPath)) { // if realPath == null, then some exception was caught while trying to convert to real path. Mark as error.
                System.out.println("WE HAVE FOUND CONTENT THAT IS SUPPORTED FOR VIDEO VIEW!");

                SharedPreferences.Editor editor =  preferences.edit(); // create an editor to make changes to what is saved to main memory.
                editor.putString("PhotoPath", photoPath); // send in the string
                editor.putString("RealPath", realPath);
                editor.commit();

                // todo: make a call to the standard meme step 1 fragment to set the photo url. Very important!
                quoteMemeStep1Fragment.setImageViewImage(photoUri);
                quoteMemeStep1Fragment.setPhotoPath(photoPath);
                quoteMemeStep1Fragment.setRealPath(realPath);


            } else {
                System.out.println("WE HAVE NOT FOUND CONTENT THAT IS SUPPORTED FOR VIDEO VIEW");

                // reset the values so that when the user presses the wrong the thing it won't allow them to go forward.
                realPath = "";
                photoPath = "";

                Snackbar snackbar = Snackbar.make(mainDrawerLayout, R.string.UnsupportedImageContentWarning, Snackbar.LENGTH_LONG);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(this, R.color.QuoteMemeColorComplimentary));
                snackbar.show();
            }
        }
    }

    @Override
    public void onConsentNeedsToBeRequested(GDPRPreperationData gdprPreperationData) {
        // we need to get consent, so we show the dialog here
        GDPR.getInstance().showDialog(this, gdprSetup, gdprPreperationData.getLocation());
    }

    @Override
    public void onConsentInfoUpdate(GDPRConsentState gdprConsentState, boolean isNewState) {

        System.out.println("CONSENT WAS CHANGED IN MAIN ACTIVITY!! isNewState? " + isNewState);

        System.out.println("GDPR consent that we have retrieved = " + gdprConsentState.logString());

        SharedPreferences.Editor editor = preferences.edit();

        if (isNewState && finishedStartScreen) // if in a new state and the start screen has been shown, we need to then update the user's information.
        {
            GDPRConsent consent = gdprConsentState.getConsent();
            if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NO_CONSENT)) { // The user has chosen to get the paid version of the app instead.
                // todo: add a link to send the user to Viddy+
                Toast.makeText(this, "Viddy+ is under development and will be available soon!", Toast.LENGTH_SHORT).show();
                System.out.println("USER GAVE NO CONSENT.");

                editor.putString("Consent", Constants.GDPR_CONSENT.NO_CONSENT);
                editor.commit();
                finish(); // close the main activity, we cannot show any ads at all thus we need to redirect the user to viddy+ and thus this app is no longer functional.

                // TODO: handle this. If the user chooses this, we need to then close the app and prevent the user from opening it again unless they buy viddy+, maybe direct them to the website.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // The user has agreed to ads, but not the good kind.

                editor.putString("Consent", Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT);
                editor.commit();

                setupGeneralAd(false); // setup general ad without showing personal consent, very important.
                enableGeneralAd();

                // NOTE: we may be able to set the user's data manually by sending the information back to the MainActivity. This is pretty crucial since we want the activity to have access to this even after the start tutorial.
                System.out.println("USER GAVE NON-PERSONAL CONSENT.");
            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT)) { // if this is reached the user is likely not in the EU and thus GDPR does not matter to these users, tell them that the default settings have been selected for them.

                editor.putString("Consent", Constants.GDPR_CONSENT.AUTOMATIC_PERSONAL_CONSENT);
                editor.commit();

                setupGeneralAd(true); // show personalized ads
                enableGeneralAd(); // enable the ads.

                new MaterialDialog.Builder(this)
                        .title(R.string.DefaultConsentTitle)
                        .content(R.string.DefaultConsentContent)
                        .positiveText(R.string.DefaultConsentPositive)
                        .positiveColor(ContextCompat.getColor(this, R.color.QuoteMemeColorComplimentary))
                        .onPositive(new MaterialDialog.SingleButtonCallback() {

                            // closes the dialog and moves to the section permissions section.
                            @Override
                            public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {

                                dialog.dismiss();
                            }
                        })
                        .show(); // show the dialog to the user.

            } else if (consent.name().equalsIgnoreCase(Constants.GDPR_CONSENT.PERSONAL_CONSENT)) { // user gave personal consent we can show personalized ads.

                setupGeneralAd(true); // show personalized ads.
                enableGeneralAd(); // enable the ads
                editor.putString("Consent", Constants.GDPR_CONSENT.PERSONAL_CONSENT);
                editor.commit();

                // we may begin to do things as normal!
                System.out.println("USER GAVE PERSONAL CONSENT.");

            } else { // not sure what this would be.
                System.out.println("A DIFFERENT TYPE OF CONSENT IS PROVIDED HERE.");
                System.out.println(gdprConsentState.logString());
            }
        }
    }

    @Override
    public void onBackPressed() {

        if (isDrawerOpen) {
            mainDrawerLayout.closeDrawer(GravityCompat.START);
        }
        else if (canGoBack) { // go back when we are able to.
            notifyFragmentIndicator(0); // make the indicator go back to the first fragment.
            getSupportFragmentManager().popBackStack(); // go back to another fragment.
            canGoBack = false; // can longer go back. Kill the app if pressed twice!
            invalidateOptionsMenu(); // removes the trash icon.
        }
        else { // likely the user is on a fragment where going back is not possible. Thus, close the app.
            //super.onBackPressed();
            finish(); // kill the main activity.
        }
    }

}
