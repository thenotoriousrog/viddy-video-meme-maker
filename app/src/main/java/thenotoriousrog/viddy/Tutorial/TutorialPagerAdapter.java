package thenotoriousrog.viddy.Tutorial;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.List;

/*
    * The pager that we will be using to handle all of our fragments.
    * We everything is returned as a fragment therefore, StartScreen will have to be able to adjust the logic according to the instance that we are working with.
 */
public class TutorialPagerAdapter extends FragmentPagerAdapter  {

    private List<Fragment> fragList;

    public TutorialPagerAdapter(FragmentManager fm, List<Fragment> fragList)
    {
        super(fm);
        this.fragList = fragList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragList.get(position);
    }

    @Override
    public int getCount() {
        return fragList.size();
    }
}
