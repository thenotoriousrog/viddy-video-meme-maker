package thenotoriousrog.viddy.Tutorial;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import thenotoriousrog.viddy.Activities.StartScreenActivity;
import thenotoriousrog.viddy.R;

/*
    * The fragment that controls the setup for the AdPermissionFragment.
 */
public class AdPermissionFragment extends Fragment {

    private StartScreenActivity startScreenActivity; // the start screen activity that we need for the system.
    private int backgroundColor;
    private int dividerColor;

    public void setFields(StartScreenActivity startScreenActivity, int backgroundColor, int dividerColor) {
        this.startScreenActivity = startScreenActivity;
        this.backgroundColor = backgroundColor;
        this.dividerColor = dividerColor;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getDividerColor() {
        return dividerColor;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mainView = inflater.inflate(R.layout.ad_permission_fragment, container, false); // inflate the tutorial fragment
        mainView.setBackgroundColor(backgroundColor);

        Button privacyPolicy = mainView.findViewById(R.id.privacyPolicyButton);
        privacyPolicy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ppURL = "https://termsfeed.com/privacy-policy/a7cd3b6daa14f45d2f5f9fd2160fd62e"; // the link to our privacy policy.
                Intent ppIntent = new Intent(Intent.ACTION_VIEW);
                ppIntent.setData(Uri.parse(ppURL));
                try {
                    startActivity(ppIntent);
                } catch (ActivityNotFoundException ex) {
                    ex.printStackTrace();
                    Toast t = Toast.makeText(getActivity(), "We're having trouble. Try again later", Toast.LENGTH_SHORT);
                    t.getView().setBackgroundColor(ContextCompat.getColor(mainView.getContext(), R.color.colorComplimentary));
                    t.show();

                }
            }
        });

        // todo: determine if we need to set the other buttons or not.

        return mainView;
    }

}
