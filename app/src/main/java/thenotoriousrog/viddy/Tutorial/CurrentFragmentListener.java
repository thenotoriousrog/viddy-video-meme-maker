package thenotoriousrog.viddy.Tutorial;

/*
    * A simple interface for a listener to keep track for which fragment that we are on.
 */
public interface CurrentFragmentListener {
    void currentFragmentPosition(int position);
}
