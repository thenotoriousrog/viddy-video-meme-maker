package thenotoriousrog.viddy.Tutorial;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.github.ybq.parallaxviewpager.ParallaxViewPager;

/*
    * This class allows for me to have the power of the viewpager as well as the sweet Parallax animation found at https://github.com/ybq/ParallaxViewPager
    * I have chose to extend this to allow the view pager to no longer be able to be swiped when reaching important areas of the
 */
public class TutorialViewPager extends ParallaxViewPager {

    private boolean isPagingEnabled = true;

    public TutorialViewPager(Context context) {
        super(context);
    }

    public TutorialViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onTouchEvent(event);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        return this.isPagingEnabled && super.onInterceptTouchEvent(event);
    }

    public void disablePaging() {
        isPagingEnabled = false;
    }

    public void enablePaging() {
        isPagingEnabled = true;
    }

    // true if paging enabled, false if not.
    public boolean isCurrentlyPaging() {
        return isPagingEnabled;
    }

}
