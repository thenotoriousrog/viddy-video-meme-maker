package thenotoriousrog.viddy.Tutorial;

import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Activities.StartScreenActivity;
import thenotoriousrog.viddy.R;

/*
    * This is a general tutorial fragment where text, content, and summary can be used for the fragment.
    * Nothing special here except to show the users a little tutorial.
    * We can specify the title, content, text, set drawables, and other important variables for the tutorial.
 */
public class TutorialFragment extends Fragment {

    private int previewDrawable; // the drawable selected for the tutorial fragment.
    private String title; // title of the current tutorial
    private String content; // content of the current tutorial.
    private String summary; // summary of the current tutorial.
    private int backgroundColor; // the color of the background of the tutorial.
    private int dividerColor; // the color of the background of the tutorial.
    private StartScreenActivity startScreenActivity; // the start screen activity to report options back to the StartScreenActivity.

    public void setFields(int previewDrawable, String title, String content, String summary, int backgroundColor, int dividerColor, StartScreenActivity startScreenActivity)
    {
        this.previewDrawable = previewDrawable;
        this.title = title;
        this.content = content;
        this.summary = summary;
        this.backgroundColor = backgroundColor;
        this.dividerColor = dividerColor;
        this.startScreenActivity = startScreenActivity;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getDividerColor() {
        return dividerColor;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mainView = inflater.inflate(R.layout.tutorial_fragment, container, false); // inflate the tutorial fragment
        mainView.setBackgroundColor(backgroundColor);

        ImageView preview = mainView.findViewById(R.id.generalTutorialImage);
        if(previewDrawable == -1) {
            preview.setImageDrawable(null);
        } else {
            System.out.println("Is the start screen activity null?? " + startScreenActivity);
            preview.setImageDrawable(startScreenActivity.getDrawable(previewDrawable));
        }


        TextView tutTitle = mainView.findViewById(R.id.generalTutorialFragmentTitle);
        tutTitle.setText(title);
        TextView tutContent = mainView.findViewById(R.id.generalTutorialFragmentContent);
        tutContent.setText(content);
        TextView tutSummary = mainView.findViewById(R.id.generalTutorialFragmentSummary);
        tutSummary.setText(summary);

        return mainView;

    }



}
