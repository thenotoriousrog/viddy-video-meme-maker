package thenotoriousrog.viddy.Tutorial;

import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import thenotoriousrog.viddy.Activities.StartScreenActivity;
import thenotoriousrog.viddy.R;

/*
    * This fragment allows the user to review the permissions that are needed and why.
    * After this fragment the user will be able to
 */
public class PermissionsFragment extends Fragment {

    private StartScreenActivity startScreenActivity;
    private int backgroundColor;
    private int dividerColor;

    public void setFields(StartScreenActivity startScreenActivity, int backgroundColor, int dividerColor)
    {
        this.startScreenActivity = startScreenActivity;
        this.backgroundColor = backgroundColor;
        this.dividerColor = dividerColor;
    }

    public int getBackgroundColor() {
        return backgroundColor;
    }

    public int getDividerColor() {
        return dividerColor;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View mainView = inflater.inflate(R.layout.device_permissions, container, false); // inflate the tutorial fragment
        mainView.setBackgroundColor(backgroundColor);

        ImageView icon = mainView.findViewById(R.id.permissionsImage);
        icon.setImageDrawable(getActivity().getDrawable(R.drawable.school_material)); // set the icon

        // todo: setup button listeners

        return mainView;
    }
}
