package thenotoriousrog.viddy.StandardMeme;

import android.Manifest;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.michaelflisar.gdprdialog.GDPR;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.ViddyMeme.ViddyStep3Fragment;

public class StandardMemeStep2Fragment extends Fragment {

    private StandardMemeActivity standardMemeActivity;
    private String photoPath = "";
    private String memeText = ""; // this is the text of the veme that the user has written
    private String realPath = ""; // the real system path of the video itself.
    private String customWatermark; // holds the text of the custom watermark that the user will be using.
    private DiscreteSeekBar textIncreaser; // the slider that the user will use to increase the text of the veme
    private TextView memeTextView; // the text preview.
    private TextView customWatermarkTextView; // the text for the custom watermark.
    private TextView customWatermarkTextPlaceholder; // just holds the same look as the edit text field, but is only clickable.
    private TextInputEditText customWatermarkText; // the field that the user can add text too.
    private int textSizeVal = 10; // the starting value is always 10
    private boolean withWatermark = true; // add the watermark to the image.
    private boolean removeWatermarkAdWatched = false; // this will be set true upon watching the ad then they can add their watermark!
    private boolean addCustomWatermarkAdWatched = false; // tells us if the user has watched an add to add their own custom watermark!
    private RewardedVideoAd rewardedVideoAd; // the add that the user can watch in order to unlock features within this activity.
    private View mainView;
    private RelativeLayout mainContentLayout; // the layout that holds all of the goods.
    private boolean readyForStep3 = false; // tells the fragment that we are ready to go to step 3.
    private ImageView imageView; // the image view for the meme to show up on.
    private ProgressBar progressBar; // the progress bar to show that the app is running.
    private RelativeLayout preview; // the meme preview for the image that the user is looking at.
    private TextView viddyWatermarkText; // the viddy watermark itself.
    private boolean isRewarded = false; // tells the system if the user was rewarded or not!
    private String filename; // the filename that the user chooses for their meme.

    public void setFields(StandardMemeActivity standardMemeActivity, String photoPath, String realPath, String filename) {
        this.standardMemeActivity = standardMemeActivity;
        this.photoPath = photoPath;
        this.realPath = realPath;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    private void setupDiscreteSeekBar(View mainView)
    {
        textIncreaser = mainView.findViewById(R.id.textSizeIncreaser);
        textIncreaser.setScrubberColor(ContextCompat.getColor(getContext(), R.color.StandardMemeColorComplimentary));
        textIncreaser.setThumbColor(ContextCompat.getColor(getContext(), R.color.StandardMemeColorComplimentary), ContextCompat.getColor(getContext(), R.color.StandardMemeColorComplimentary));
        textIncreaser.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {

            // updates the value of the text as we begin sliding for the user.
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser)
            {
                textSizeVal = value; // set the size of the text the user has chosen
                memeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, value); // set the text size based on the value.
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) { }
        });
    }

    // hides the main content and shows the progress loader.
    public void showProgressBar()
    {
        mainContentLayout.setVisibility(View.GONE); // remove the main layout.
        progressBar.setVisibility(View.VISIBLE); // make the progress bar visible.
    }

    public void displayStandardMemeStep3Fragment(String standardMemePath, String errorMsg) {

        standardMemeActivity.changeCanGoBackState(false);
        StandardMemeStep3Fragment standardMemeStep3Fragment = new StandardMemeStep3Fragment();
        if (errorMsg.isEmpty()) { // if empty, no error to worry about. Send in an empty error which shows the correct file location.
            standardMemeStep3Fragment.setFields(standardMemeActivity, standardMemePath, "");
        } else {
            standardMemeStep3Fragment.setFields(standardMemeActivity, standardMemePath, errorMsg);
        }
        //standardMemeStep3Fragment.setFields(standardMemeActivity, standardMemePath, "");
        FragmentTransaction transaction = standardMemeActivity.getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.mainFrameLayout, standardMemeStep3Fragment);
        transaction.addToBackStack(null);
        transaction.commit();

        standardMemeActivity.hideEditFilenameIcon(); // hides the filename icon.
        standardMemeActivity.displayShareIcon(standardMemePath); // shows the share icon and sets the data needed for us to show the final product!
    }

    // sets up the listener to be able to watch the rewarded video ad.
    private void setupRewardedVideoAd(View mainView)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // get the context that we need in order to sort these lists.
        SharedPreferences.Editor editor = preferences.edit();

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getContext()); // set our ad.
        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewarded(RewardItem reward) {

                System.out.println("USER WAS REWARDED FOR WATCHING AN AD!!!!!");
                isRewarded = true; // the user is rewarded for watching the ad!
            }

            @Override
            public void onRewardedVideoAdLeftApplication() { }

            @Override
            public void onRewardedVideoAdClosed() {
                // Toast.makeText(getBaseContext(), "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();


                // If the user is rewarded then we need to give them the reward!
                if(isRewarded) {
                    System.out.println("THE USER CLOSED THE VIDEO BUT WE'RE READY TO CREATE THE MEME!!!");
                    Bundle args = new Bundle();
                    args.putString("PhotoPath", photoPath); // the path of the video that the user has selected.
                    args.putString("RealPath", realPath);
                    if(memeText == null) {
                        memeText = ""; // prevents a crash if a user doesn't add any text.
                    }
                    args.putString("VemeText", memeText); // put the meme text that the user has written.
                    args.putString("CustomWatermark", customWatermark); // send the custom watermark to the next activity.
                    args.putBoolean("WithWatermark", false); // we are going to make the meme without a watermark!
                    args.putInt("TextSize", textSizeVal);
                    //step3Intent.putExtras(args); // put the arguments for the activity.

                    Toast toast = new Toast(getContext());
                    View toastView = getLayoutInflater().inflate(R.layout.sm_watermark_not_added_toast, (ViewGroup) mainView.findViewById(R.id.watermarkNotAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
                    toast.setView(toastView);
                    toast.show();

                    viddyWatermarkText.setVisibility(View.GONE); // user watched an ad, we can remove our watermark now.

                    // create meme without the Viddy watermark.
                    StandardMemeBackgroundThread backgroundThread = new StandardMemeBackgroundThread(preview, photoPath, realPath, memeText, customWatermark, false, textSizeVal,
                            StandardMemeStep2Fragment.this, filename);
                    showProgressBar();
                    backgroundThread.startBackgroundThread();
                    standardMemeActivity.notifyFragmentIndicator(2); // show the third item to be highlighted.
                    standardMemeActivity.changeCanGoBackState(false); // the user cannot go back now.
                }else {
                    System.out.println("THE USER CLOSED THE AD AND DOES NOT GET THE REWARD!");
                }
            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                CoordinatorLayout snackbarLocation = mainView.findViewById(R.id.snackbarLocation);
                Snackbar snackbar = Snackbar.make(snackbarLocation, R.string.Ad_Not_Loading, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(getContext(), R.color.StandardMemeColorComplimentary));
                sbView.setElevation(5);
                snackbar.show();
            }

            // listens for when the ad is loaded.
            @Override
            public void onRewardedVideoAdLoaded() {
                rewardedVideoAd.show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                System.out.println("THE USER HAS OPENED THE AD REWARD THEM!!");
                isRewarded = true; // give the user the reward for opening the ad, that way if it's closed they get the reward no matter what!
            }

            @Override
            public void onRewardedVideoStarted() { }

            @Override
            public void onRewardedVideoCompleted() { }
        });
    }

    private void setupStep3Button(View mainView, String userConsent) {
        final Button step3Button = mainView.findViewById(R.id.step3Button);
        step3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // if they choose no, display a full screen ad, after the ad the activity will generate the meme without a watermark.
                // if they choose to keep the watermark then immediately generate the meme with the watermark of the app. Very important!
                String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                if(EasyPermissions.hasPermissions(getContext(), perms)) // permissions available, proceed as normal.
                {
                    if(filename == null || filename.isEmpty()) {
                        filename = "StandardMeme"; // a default name for the meme.
                    }

                    StandardMemeBackgroundThread backgroundThread = new StandardMemeBackgroundThread(preview, photoPath, realPath, memeText, customWatermark, false, textSizeVal,
                            StandardMemeStep2Fragment.this, filename);
                    showProgressBar();
                    backgroundThread.startBackgroundThread();
                    standardMemeActivity.notifyFragmentIndicator(2); // show the third item to be highlighted.
                    standardMemeActivity.changeCanGoBackState(false); // the user cannot go back now.

                } else { // permissions not granted, we need to get them as soon as possible.

                    String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}; // just need to read/write storage android.
                    EasyPermissions.requestPermissions(
                            new PermissionRequest.Builder(standardMemeActivity, Constants.ACTION.REQUEST_PERMISSIONS, permissions)
                                    .setRationale(R.string.PermissionsRationale)
                                    .setPositiveButtonText(R.string.PermissionRationaleOk)
                                    .setNegativeButtonText(R.string.PermissionRationaleCancel)
                                    .build());
                }
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    private void setupCustomWatermark() {
        // add text to the proper custom watermark view.
        customWatermarkText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            // the actual text magic happens now.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                customWatermark = s.toString(); // add to the string we will be sending to the user.
                customWatermarkTextView.setText(s.toString()); // show the watermark on the viddy.
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.standard_meme_step2_fragment, container, false); // inflate step1 fragment

        String userConsent = GDPR.getInstance().getConsentState().getConsent().name(); // get the consent type from the user.

        setupDiscreteSeekBar(mainView);
        setupRewardedVideoAd(mainView);
        setupStep3Button(mainView, userConsent);

        progressBar = mainView.findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE); // ensure that the progress bar is gone

        mainContentLayout = mainView.findViewById(R.id.standardMemeStep2FragmentMainLayout);
        preview = mainView.findViewById(R.id.memePreviewStep2); // the meme preview for the image.
        preview.setDrawingCacheEnabled(true);

        viddyWatermarkText = mainView.findViewById(R.id.viddyWatermarkText);

        imageView = mainView.findViewById(R.id.memeViewStep2);
        imageView.setImageURI(Uri.parse(photoPath));

        memeTextView = mainView.findViewById(R.id.memeTextStep2);
        customWatermarkTextView = mainView.findViewById(R.id.customWatermarkText); // the text that the user's watermark will go.
        customWatermarkText = mainView.findViewById(R.id.customWatermark); // the custom watermark text that we are working on.
        setupCustomWatermark();

        TextInputEditText editText = mainView.findViewById(R.id.editMemeText); // this is where the user will type.
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {} // not needed, auto-generated.

            @Override
            public void afterTextChanged(Editable s) {} // not needed, auto-generated

            // listens for text as its being entered
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                memeText = s.toString(); // update the veme text.

                if(memeTextView.getLineCount() != 4)
                {
                    memeTextView.setText(s.toString()); // set the text as the user updates it.
                }
            }
        });

        return mainView;
    }

}
