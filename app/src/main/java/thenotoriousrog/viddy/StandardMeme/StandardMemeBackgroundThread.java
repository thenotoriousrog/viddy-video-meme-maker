package thenotoriousrog.viddy.StandardMeme;

import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import thenotoriousrog.viddy.R;

/*
    * This thread is in charge of making the standard meme. It simply creates the meme and writes the file to the Viddy directory.
 */
public class StandardMemeBackgroundThread implements Runnable {

    private String photoPath;
    private String realPath;
    private String memeText;
    private String customWatermark;
    private String filename;
    private boolean withWatermark;
    private int textSize;
    private StandardMemeStep2Fragment standardMemeStep2Fragment; // used to start a new update for the new item in the feature
    private Runnable uiUpdater; // updates the UI on the mainUIThread.
    private Thread backgroundThread; // the thread that runs in the background.
    private RelativeLayout preview; // the meme preview. Very important to get right!

    public StandardMemeBackgroundThread(RelativeLayout preview, String photoPath, String realPath, String memeText, String customWatermark, boolean withWatermark, int textSize,
                                     StandardMemeStep2Fragment standardMemeStep2Fragment, String filename)
    {
        this.preview = preview;
        this.photoPath = photoPath;
        this.realPath = realPath;
        this.memeText = memeText;
        this.customWatermark = customWatermark;
        this.withWatermark = withWatermark;
        this.textSize = textSize;
        this.standardMemeStep2Fragment = standardMemeStep2Fragment;
        this.filename = filename;
        backgroundThread = new Thread(this);
    }

    public void startBackgroundThread() {
        backgroundThread.start();
    }


    // generates a bitmap from the photo path.
    // todo: more work may be needed in here.
    private Bitmap getBitmap()
    {
        Bitmap toReturn = null; // the bitmap from the file.
        BitmapFactory.Options options = new BitmapFactory.Options();
        InputStream stream = null; // the opened stream retrieved from the context.
        try {
            //FileInputStream inputStream = new FileInputStream(new File(realPath)); // create a stream from the file path in order to actually create the video.
            //stream = standardMemeStep2Fragment.getContext().openFileInput(Uri.decode(photoPath));
            Uri imageUri = Uri.parse(photoPath);
            toReturn = MediaStore.Images.Media.getBitmap(standardMemeStep2Fragment.getContext().getContentResolver(), imageUri); //BitmapFactory.decodeStream(inputStream);//BitmapFactory.decodeFile(realPath); // decode the file into a bitmap.
            //stream.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return toReturn;
    }


    /*
     * Saves a bitmap to internal storage.
     * Code retrieved from stackoverflow: https://stackoverflow.com/questions/17674634/saving-and-reading-bitmaps-images-from-internal-memory-in-android -> by Brijesh Thakur.
     * Modified to fit our use case.
     */
    private File saveToInternalStorage(Bitmap standardMeme) {
        File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/"); // a temporary folder to hold video frames.
        if (!root.exists()) {
            root.mkdirs(); // make all the directories.
        } else {
            root.delete(); // the directory exists... remove it
            root.mkdirs(); // remake the directories.
        }

        // Create standard meme path.
        File path = new File(root, filename + ".jpg"); // create the new file and append with .jpg so that the image is properly setup.

        FileOutputStream fos = null;
        try {

            // create the file if it doesnt exist otherwise delete the file and create the file.
            if(!path.exists()) {
                path.createNewFile();
            } else {
                path.delete();
                path.createNewFile();
            }

            fos = new FileOutputStream(path);

            //standardMeme = preview.getDrawingCache(); // this will draw the relative layout as a bitmap which in of itself creates their meme.
            // Use the compress method on the BitMap object to write image to the OutputStream
            standardMeme.compress(Bitmap.CompressFormat.JPEG, 100, fos); // we always want to write the image with 100% video quality.

        } catch (Exception e) { // todo: add error catching in here so that we can alert the user if something went wrong.
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return path; // return the path of the meme
    }

    @Override
    public void run()
    {

        Bitmap standardMeme = preview.getDrawingCache(); // build the layout using the drawing cache.
        File memeFile = saveToInternalStorage(standardMeme); // write the meme to internal storage.

        String errorMsg = ""; // no error msg at all.
        if (memeFile == null) { // an error occurred.
            errorMsg = standardMemeStep2Fragment.getString(R.string.StandardMemeNullMemeFile);
            standardMemeStep2Fragment.displayStandardMemeStep3Fragment(memeFile.getAbsolutePath(), errorMsg); // display the final fragment.
        } else { // no error
            standardMemeStep2Fragment.displayStandardMemeStep3Fragment(memeFile.getAbsolutePath(), errorMsg); // display the final fragment.
        }


    }
}
