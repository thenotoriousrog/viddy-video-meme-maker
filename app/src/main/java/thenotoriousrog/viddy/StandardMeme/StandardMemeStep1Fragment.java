package thenotoriousrog.viddy.StandardMeme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;

public class StandardMemeStep1Fragment extends Fragment {

    private ImageView imageView; // the imageview for the photo that the user has created.
    private String photoPath;
    private String realPath;
    private String filename; // the filename that the user selects for their meme.
    private StandardMemeActivity standardMemeActivity;

    public void setFields(StandardMemeActivity standardMemeActivity) {
        this.standardMemeActivity = standardMemeActivity;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    // sets the image for the user to see.
    public void setImageViewImage(Uri imageUri) {
        imageView.setImageURI(imageUri);
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public void setRealPath(String realPath) {
        this.realPath = realPath;
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    private void setupFAB(View mainView)
    {
        CoordinatorLayout fabCoordLayout = mainView.findViewById(R.id.fabCoordLayout);
        CoordinatorLayout.Behavior behavior = new CoordinatorLayout.Behavior() {
            @Override
            public void onAttachedToLayoutParams(@NonNull CoordinatorLayout.LayoutParams params) {
                super.onAttachedToLayoutParams(params);

                params.dodgeInsetEdges = Gravity.BOTTOM;
            }
        };

        FloatingActionButton fab = mainView.findViewById(R.id.createStandardMemeButton);
        fab.setRippleColor(ViddyUtils.getInstance().getColor(getContext(), R.color.white));
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent openFileManagerIntent = new Intent(Intent.ACTION_GET_CONTENT);
                openFileManagerIntent.setType("*/*"); // allow a file of any type. todo: change this to only be m3u
                standardMemeActivity.startActivityForResult(openFileManagerIntent, Constants.INTENT.PICK_FILE);
            }
        });
    }

    private void setupStep2Button(View mainView)
    {
        final Button step2Button = mainView.findViewById(R.id.step2Button);
        step2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(photoPath != null) // the uri isn't null thus we can make use of the video.
                {
                    //Intent step2Intent = new Intent(mainActivity, Step2Activity.class); // the intent to start step2
                    Bundle args = new Bundle();


                    args.putString("PhotoPath", photoPath); // put the video Uri for the activity to use in order to display the video again.
                    args.putString("RealPath", realPath); // set the real path to be used throughout the app.
                    //step2Intent.putExtras(args); // set the args for the activity to make use of on the app itself.
                    standardMemeActivity.displayStandardMemeStep2Fragment(filename);
                    standardMemeActivity.notifyFragmentIndicator(1); // make the second indicator light now.
                    standardMemeActivity.displayEditFilenameIcon(); // shows the edit text field.
                    //startActivity(step2Intent); // start the step 2 activity.
                }
                else { // the user pressed proceed to step 2 without selecting a video, ask them to pick a video first, very important!

                    // todo: we need to handle this error here. The user pressed step 2 before they have actually selected a video to use.
                }

                // TODO: setup the behavior to start a new activity for step 2 where the user can select the text of their video.
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.standard_meme_step1_fragment, container, false); // inflate step1 fragment

        imageView = mainView.findViewById(R.id.memeView); // the image view for the meme preview
        setupFAB(mainView);
        setupStep2Button(mainView);

        return mainView;
    }

}
