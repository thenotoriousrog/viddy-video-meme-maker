package thenotoriousrog.viddy.Listeners;

import android.graphics.Rect;
import android.graphics.RectF;
import android.view.ScaleGestureDetector;

import thenotoriousrog.viddy.CustomViews.DraggableEditText;

/*
    * The scale listener listens for the user to scale their view with a gesture and they can then increase and decrease the size of their text for their view.
 */
public class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {

    private float scaleFactor; // the scale factor to consider in our listener.
    private DraggableEditText draggableEditText; // the view we are listening for scale behavior on.
    private RectF currentViewport;
    private Rect contentRect;

    public ScaleListener(DraggableEditText draggableEditText, float scaleFactor) {
        this.draggableEditText = draggableEditText;
        this.scaleFactor = scaleFactor;
    }


    @Override
    public boolean onScale(ScaleGestureDetector detector) {
        scaleFactor *= detector.getScaleFactor();

        // Don't let the object get too small or too large.
        scaleFactor = Math.max(8.0f, Math.min(scaleFactor, 40.0f));

        // todo: decide if there is any other behavior that needs to happen here.

        draggableEditText.setScaleFactor(scaleFactor);
        draggableEditText.invalidate();
        return true;
    }

}
