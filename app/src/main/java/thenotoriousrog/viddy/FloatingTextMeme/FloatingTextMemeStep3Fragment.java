package thenotoriousrog.viddy.FloatingTextMeme;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;

import java.io.File;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;
import thenotoriousrog.viddy.StandardMeme.StandardMemeActivity;
import thenotoriousrog.viddy.ViddyMeme.RippleLayout;

public class FloatingTextMemeStep3Fragment extends Fragment {

    private FloatingTextMemeActivity floatingTextMemeActivity;
    private String floatingTextMemePath;
    private String errorMsg;
    private RippleLayout rippleLayout;
    private ImageView moreInfo;
    private Button makeNewMemeButton;

    public void setFields(FloatingTextMemeActivity floatingTextMemeActivity, String floatingTextMemePath, String errorMsg) {
        this.floatingTextMemeActivity = floatingTextMemeActivity;
        this.floatingTextMemePath = floatingTextMemePath;
        this.errorMsg = errorMsg;
        floatingTextMemeActivity.hideEditFilenameIcon();
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
    }

    private void setupMoreInfo(View mainView) {
        moreInfo = mainView.findViewById(R.id.moreInfoImage);
        moreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (errorMsg.isEmpty()) // no error to show, thus show the location of their meme.
                {
                    new MaterialDialog.Builder(getContext())
                            .title(R.string.CompletedStandardMemeTitle)
                            .content(floatingTextMemePath)
                            .positiveText(R.string.Close)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    rippleLayout.stopRippleAnimation();
                                }
                            })
                            .show();
                }
                else
                {
                    new MaterialDialog.Builder(getContext())
                            .title(R.string.FloatingTextMemeErrorTitle)
                            .content(errorMsg)
                            .positiveText(R.string.Close)
                            .onPositive(new MaterialDialog.SingleButtonCallback() {
                                @Override
                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which) {
                                    dialog.dismiss();
                                    rippleLayout.stopRippleAnimation();
                                }
                            })
                            .show();
                }
            }
        });

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.floating_text_meme_step3fragment, container, false); // inflate step1 fragment

        rippleLayout = mainView.findViewById(R.id.rippleLayout);
        rippleLayout.setColor("Success");
        rippleLayout.startRippleAnimation(); // make sure the ripple layout is working properly.

        setupMoreInfo(mainView);
        makeNewMemeButton = mainView.findViewById(R.id.makeNewMemeButton);
        makeNewMemeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ViddyUtils.getInstance().restartViddyUtils(); // restarts the viddy utils to be brand new.
                floatingTextMemeActivity.finish();
                Intent restartActivity = new Intent(floatingTextMemeActivity, FloatingTextMemeActivity.class);
                startActivity(restartActivity); // restart the main activity.
            }
        });

        ImageView memeImageView = mainView.findViewById(R.id.memeView);
        memeImageView.setImageURI(Uri.fromFile(new File(floatingTextMemePath))); // sets the path and a URI that the imageview can use.

        return mainView;
    }

}
