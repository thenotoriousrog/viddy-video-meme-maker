package thenotoriousrog.viddy.ViddyMeme;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.media.MediaExtractor;
import android.media.MediaFormat;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.jcodec.api.android.AndroidSequenceEncoder;
import org.jcodec.common.io.NIOUtils;
import org.jcodec.common.io.SeekableByteChannel;
import org.jcodec.common.model.Rational;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import nl.bravobit.ffmpeg.FFcommandExecuteResponseHandler;
import nl.bravobit.ffmpeg.FFmpeg;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;


/*
    This class is the background thread where the video is going to be ripped. It also handles sending updates to the player service where the UI will be updated.
 */
public class VidRipperBackgroundThread implements Runnable {

    private VidRipperService vidRipperService; // a copy of the vid ripper service that will be used to communicate the proper updates to the UI and notification.
    private Thread backgroundThread; // the background thread itself.
    private boolean isStopped = false; // this tells the thread to check to make sure that the user has not stopped the thread by pressing cancel.
    private String videoPath; // path of teh video.
    private String realPath; // the real system path of the video.
    private String vemeText = ""; // the text of the veme that the user has written.
    private String customWatermark = ""; // the user's custom watermark.
    private String filename;
    private int textSize; // size of the text that the user has chosen.
    private boolean withWatermark = true; // tells the system whether or not to build the meme with or without the watermark.
    private volatile boolean ffmpegCommandFinished = true; // tells us if the ffmpeg command is finished or not. Made volatile so that the thread reads it from memory as opposed to from the cache.

    public VidRipperBackgroundThread(VidRipperService vidRipperService, String videoPath, String realPath, String vemeText, String customWatermark,
                                     int textSize, boolean withWatermark, String filename)
    {
        this.vidRipperService = vidRipperService;
        this.backgroundThread = new Thread(this); // implement the background thread.
        this.videoPath = videoPath;
        this.realPath = realPath;
        this.vemeText = vemeText;
        this.customWatermark = customWatermark;
        this.textSize = textSize;
        this.withWatermark = withWatermark;
        this.filename = filename;
    }

    // this will tell the thread to stop when the thread has reached it's next checkpoint.
    public void stopBackgroundThread() {
        isStopped = true;
    }

    public void startBackgroundThread()
    {
        backgroundThread.start();
    }

    // a method that simply throws our processing message when used from a background thread.
    private void throwProcessingException(String message) throws ProcessingException {
        throw new ProcessingException(vidRipperService, message);
    }

    // finds the nearest multiple of 2.
    private int findMultipleOf2(int number)
    {
        int result = number;
        if(number % 100 != 0)
        {
            int div = (number / 100) +1;
            result = div + 100;
        }

        return result;
    }

    // this method takes a bitmap and applies a border and text around it...
    private Bitmap applyBordersAndText(Bitmap frame)
    {
        System.out.println("Original frame width: " + frame.getWidth());
        System.out.println("Original frame height: " + frame.getWidth());

        DisplayMetrics displayMetrics = vidRipperService.getResources().getDisplayMetrics();
        Resources res = vidRipperService.getResources();
        int phoneWidth = displayMetrics.widthPixels; // get the width of the phone pixels.


        // new implementation below!
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        RelativeLayout newFrame = new RelativeLayout(vidRipperService.getBaseContext());
        newFrame.setBackgroundColor(ContextCompat.getColor(vidRipperService.getBaseContext(), R.color.white));
        newFrame.setLayoutParams(layoutParams);

        // Configure vemetext constraints
        TextView vemeTextView = new TextView(vidRipperService.getBaseContext());
        vemeTextView.setId(View.generateViewId()); // generate the id for the veme text.
        vemeTextView.setText(vemeText); // set the text that the
        System.out.println("VEME TEXT WE ARE WRITING: " + vemeText);
        //int size = DimensionConverters.spToPx(textSize, vidRipperService.getBaseContext()); // get the size of the text
        vemeTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSize);
        System.out.println("Text size = " + textSize);
        vemeTextView.setTextColor(ContextCompat.getColor(vidRipperService.getBaseContext(), R.color.Black));
        vemeTextView.setGravity(Gravity.CENTER_HORIZONTAL);
        vemeTextView.setTextAlignment(View.TEXT_ALIGNMENT_GRAVITY);

        RelativeLayout.LayoutParams vemeTextLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        vemeTextLayoutParams.setMarginEnd(5); // 30 margin to the right
        //vemeTextLayoutParams.setMarginStart(5); // 30 margin to the left.
        vemeTextView.setLayoutParams(vemeTextLayoutParams);

        // configure image view constraints...
        ImageView editedFrame = new ImageView(vidRipperService.getBaseContext());
        // Note: when doing padding the height and the width must be a multiple of two. A nice example is 70+30 = 100/2 = 50, but 80+30 = 110/2 = 55 <- not a multiple of two. Keep this in mind.
        editedFrame.setId(View.generateViewId());
        editedFrame.setPadding(30,30,30,0); // padding of 30 around the whole view.
        editedFrame.setImageBitmap(frame); // set the frame to be that of the actual background.
        RelativeLayout.LayoutParams frameLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        frameLayoutParams.addRule(RelativeLayout.CENTER_HORIZONTAL); // place frames in center of the view.
        frameLayoutParams.addRule(RelativeLayout.BELOW, vemeTextView.getId()); // make view below the current view.
        //frameLayoutParams.addRule(RelativeLayout.ABOVE, watermark.getId()); // ensure the video is above the watermark.

        // configure watermark constraints
        TextView watermark = new TextView(vidRipperService.getBaseContext());
        watermark.setText("Made with Viddy"); // when we want to update the text for the watermark it does here.
        watermark.setTextColor(ContextCompat.getColor(vidRipperService.getBaseContext(), R.color.LightGrey));
        watermark.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16);
        watermark.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC));
        watermark.setPadding(0,0, 30,0);
        RelativeLayout.LayoutParams watermarkLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        watermarkLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_END);
        watermarkLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        watermarkLayoutParams.addRule(RelativeLayout.BELOW, editedFrame.getId()); // make view below the current view.
        watermark.setLayoutParams(watermarkLayoutParams);

        // configure customWatermark constraints, i.e. to the bottom left (or start) of the view.
        TextView customWatermarkView = new TextView(vidRipperService.getBaseContext());
        if ( customWatermark != null && !customWatermark.isEmpty()) {
            customWatermarkView.setText(customWatermark); // when we want to update the text for the watermark it does here.
        }
        customWatermarkView.setTextColor(ContextCompat.getColor(vidRipperService.getBaseContext(), R.color.LightGrey));
        customWatermarkView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        //customWatermarkView.setTypeface(Typeface.create(Typeface.DEFAULT, Typeface.ITALIC)); // keep standard typeface, no need to make it italic.
        customWatermarkView.setPadding(30,0, 0,0);
        RelativeLayout.LayoutParams customWatermarkLayoutParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        customWatermarkLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_START);
        customWatermarkLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        customWatermarkLayoutParams.addRule(RelativeLayout.BELOW, editedFrame.getId()); // make view below the current view.
        customWatermarkView.setLayoutParams(customWatermarkLayoutParams);

        editedFrame.setLayoutParams(frameLayoutParams);

        // add the views to the relative layout.
        newFrame.addView(vemeTextView);
        newFrame.addView(editedFrame);
        if (withWatermark) { // if true make the viddy with the watermark.
            newFrame.addView(watermark);
        }
        if ( customWatermark != null && !customWatermark.isEmpty()) { // if the custom watermark isn't null and isn't empty, add the view with the custom watermark.
            newFrame.addView(customWatermarkView);
        }
        // draw the layout
        newFrame.setDrawingCacheEnabled(true);

        System.out.println("phoneWidth = " + phoneWidth);
        newFrame.measure(View.MeasureSpec.makeMeasureSpec(((frame.getWidth()+99)/100)*100, View.MeasureSpec.AT_MOST), // setting 2000 at most says that the viddy will not be wider than at most 2000 pixels (divided by 2 later on)
                 View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));
        newFrame.layout(0,0, newFrame.getMeasuredWidth(), newFrame.getMeasuredHeight());
        newFrame.buildDrawingCache(); // allows the relative layout to be drawn.

        // get the preconfigured height and width.
        int height = newFrame.getHeight();
        int width = newFrame.getWidth();

        int total = width + height; // the total width and height must be divisible by 2.
        // attempts to correct the sizing of the veme by ensuring that the width + height is a multiple of 2
        while( (total % 2) != 0)
        {
            height += 1; // add one to the height.
            total = width + height; // reset the height.
        }

        System.out.println("width before processing: " + width);
        System.out.println("height before processing: " + height);

        // multH and multW will ensure that no matter what the height and the width are always a square.
        int multH = ((newFrame.getHeight()+99)/100)*100;
        int multW = ((newFrame.getWidth()+99)/100)*100; // ensures that the width is no larger than that on their phone.
        int multT = ((total+99)/100)*100;
        System.out.println("The multiple of 2 for height = " + multH);
        System.out.println("The multiple of 2 for width = " + multW);
        System.out.println("The multiple of 2 for total = " + multT);

        // todo: test this out. If for any reason the video is not a square the app cannot create the viddy. This is a problem.
        height = multH; // only the height needs to be accounted for in terms of figuring out the difference between the width and the height.
        width = multW;

        newFrame.removeAllViews();
        newFrame = new RelativeLayout(vidRipperService.getBaseContext());
        newFrame.setBackgroundColor(ContextCompat.getColor(vidRipperService.getBaseContext(), R.color.white));
       // DisplayMetrics displayMetrics = vidRipperService.getResources().getDisplayMetrics();
       // int phoneWidth = displayMetrics.widthPixels; // get the width of the phone pixels.
        newFrame.measure(View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(2000, View.MeasureSpec.AT_MOST)); // by setting at 2000 we are limiting the size of the video to be no more than 1000 pixels after dividing by 2. This should fix the bad video quality.
        newFrame.layout(0,0, width, height); // redraw the frame with width and height fixed.

        System.out.println("Phone width = " + phoneWidth);
        System.out.println("new frame width = " + newFrame.getWidth());
        System.out.println("new frame height = " + newFrame.getHeight());

        newFrame.setLayoutParams(layoutParams);
        newFrame.addView(vemeTextView);
        newFrame.addView(editedFrame);
        if (withWatermark) { // if true make the viddy with the watermark.
            newFrame.addView(watermark);
        }
        if ( customWatermark != null && !customWatermark.isEmpty()) { // if the custom watermark isn't null and isn't empty, add the view with the custom watermark.
            newFrame.addView(customWatermarkView);
        }

        newFrame.buildDrawingCache(); // rebuild the UI.

        // todo: determine if the commented code below is needed. It solves the problem of the bitmap being null.
        Bitmap frameToReturn = Bitmap.createBitmap(newFrame.getWidth(), newFrame.getHeight(), Bitmap.Config.RGB_565);
        Canvas c = new Canvas(frameToReturn);
        newFrame.draw(c);

        System.out.println("Width of the new frame: " + frameToReturn.getWidth());
        System.out.println("Height of the new frame: " + frameToReturn.getHeight());

        return frameToReturn;

    }

    /*
     * Saves a bitmap to internal storage. Likely can be modified to u
     * Code retrieved from stackoverflow: https://stackoverflow.com/questions/17674634/saving-and-reading-bitmaps-images-from-internal-memory-in-android -> by Brijesh Thakur.
     * Modified to fit our use case.
     */
    private File saveToInternalStorage(Bitmap frame, String frameFileName) {
        File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/.edited"); // a temporary folder to hold video frames.
        if (!root.exists()) {
            root.mkdirs(); // make all the directories.
        } else {
            root.delete(); // the directory exists... remove it
            root.mkdirs(); // remake the directories.
        }

        // Create imageDir
        File path=new File(root, "edited-" + frameFileName); // created a new file that has been edited.

        FileOutputStream fos = null;
        try {

            // create the file if it doesnt exist otherwise delete the file and create the file.
            if(!path.exists()) {
                path.createNewFile();
            } else {
                path.delete();
                path.createNewFile();
            }

            fos = new FileOutputStream(path);

            Bitmap reduced = Bitmap.createScaledBitmap(frame, frame.getWidth()/2,frame.getHeight()/2, false);
            //Bitmap reduced = Bitmap.createScaledBitmap(frame, frame.getWidth()/2,700, false);

            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(vidRipperService.getBaseContext());
            int vidQuality = preferences.getInt("VideoQuality", 35); // gets the video quality. The default is set to 35% in case the user has not selected a default video quality anywhere.

            // Use the compress method on the BitMap object to write image to the OutputStream
            reduced.compress(Bitmap.CompressFormat.JPEG, vidQuality, fos); // todo: the setting for controlling quality should be here.

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return root;
    }

    // goes through each of the video frames
    private File remakeVideo(File editedFramesDir, int frameRate)
    {
        SeekableByteChannel out = null;
        File viddy = null;
        try {
            File frames[] = editedFramesDir.listFiles(); // get all of the edited frames.

            File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/video"); // where all of the user's viddies will go.
            if(!root.exists()) {
                root.mkdirs(); // make all the directories.
            } else {
                root.delete();
                root.mkdirs();
            }

            viddy = new File(root,"Viddy.mp4"); // todo: this needs to not be hard coded!!!

            out = NIOUtils.writableFileChannel(viddy.getPath()); // get the path of the video.
            AndroidSequenceEncoder encoder = new AndroidSequenceEncoder(out, Rational.R(frameRate*1000, 1001));

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inMutable = true;
            Bitmap.Config config = Bitmap.Config.RGB_565; // this will ensure that the image is taken in in the least amount of space as possible.
            options.inPreferredConfig = config;

            double total = frames.length;
            for(double i = 0; i < frames.length; i++)
            {
                // if background thread is stopped, immediately break the loop
                if(isStopped) {
                    return null;
                }

                File frame = frames[(int)i]; // get a frame

                System.out.println("edited frame i = " + i);

                double percentage = (i/total) * 100; // creates percentage of completion of the processing of the video.
                vidRipperService.sendUIProgressUpdate(Integer.toString((int)percentage) + " %"); // update the progress on the main UI to update the progress from within the app itself.

                // if the frame exists, we can process it to make a new video.
                if(frame.exists())
                {
                    Bitmap image = BitmapFactory.decodeFile(frame.getAbsolutePath(), options); // get the image

                    System.out.println("Width of bitmap: " + image.getWidth());
                    System.out.println("Height of bitmap: " + image.getHeight());

                    encoder.encodeImage(image); // encode the image into an actual video.
                }
            }

            encoder.finish();

        } catch (FileNotFoundException ex) {
            System.out.println("File not found exception while making viddy");
            ex.printStackTrace();
        } catch (IOException ex) {
            System.out.println("Caught IO exception when trying to make viddy");
            ex.printStackTrace();
        } finally {
            NIOUtils.closeQuietly(out);
        }

        return viddy; // return the viddy without sound.
    }

    // Takes the file where the frames are and then remakes the video returns the directory where all of the edited frames are.
    private File editFrames(File framesDir)
    {
        File editedFramesDir = new File(Environment.getExternalStorageDirectory(), "/Viddy/.edited"); // a temporary folder to hold video frames.
        File frames[] = framesDir.listFiles(); // get all files in this directory.


        if(!editedFramesDir.exists()) {
            editedFramesDir.mkdirs();
        } else {
            editedFramesDir.delete();
            editedFramesDir.mkdirs();
        }

        double total = frames.length;

        // iterate through each of the frames and recreate the frames one by one.
        for(double i = 0; i < frames.length; i++)
        {
            // if background thread stopped immediately break the loop.
            if(isStopped) {
                return null;
            }

            System.out.println("frame i = " + i);
            File frame = frames[(int)i]; // get the file that we are iterating.

            double percentage = (i/total) * 100; // creates percentage of completion of the processing of the video.
            vidRipperService.sendUIProgressUpdate(Integer.toString((int)percentage) + " %"); // update the progress on the main UI to update the progress from within the app itself.


            if(frame.exists()) // if the frame exists in the file directory.
            {
                Bitmap image = BitmapFactory.decodeFile(frame.getAbsolutePath()); // get the image
                image = applyBordersAndText(image); // apply the borders and text to the bitmap itself
                editedFramesDir = saveToInternalStorage(image, frame.getName()); // save the image to internal storage.
            }
        }

        return editedFramesDir;

    }

    // Rips frames from the video
    private File ripFrames(File video)
    {
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/.frames"); // a temporary folder to hold video audio.
            if(!root.exists()) {
                root.mkdirs(); // make all the directories.
            } else {
                root.delete();
                root.mkdirs();
            }

            File videoFile = new File(realPath);
            System.out.println("Video file name = " + videoFile.getName());

            System.out.println("Root path = " + root.getAbsolutePath());

            String[] cmd = {"-i", realPath, root.getAbsolutePath() + "/frame-%03d.jpg"}; // command to rip all frames.

            FFmpeg.getInstance(vidRipperService).execute(cmd, new FFcommandExecuteResponseHandler() {

                @Override
                public void onStart() {
                    ffmpegCommandFinished = false;
                }

                @Override
                public void onSuccess(String message)
                {
                    System.out.println("SUCCESSFULLY completed ffmpeg command!");
                    System.out.println("Message: " + message);
                }

                @Override
                public void onProgress(String message)
                {
//                    System.out.println("FFmpeg in progress...");
//                    System.out.println("Message: " + message);

                }

                @Override
                public void onFailure(String message)
                {
                    System.out.println("FFmpeg rip frames COMMAND FAILED...");
                    System.out.println("Message: " + message);
                    ffmpegCommandFinished = true;

                    try {
                        throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.RipFramesError));
                    } catch (ProcessingException ex) {
                        System.out.println("ProcessingException was caught in VidRipperBackgroundThread");
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFinish()
                {
                    System.out.println("FFmpeg rip frames IS FINISHED!");
                    ffmpegCommandFinished = true;
                }
            });

            return root; // returns the frames directory.

        } finally {}
    }


    // takes in the two videos and begins to extract the sound from the old video and places it in the new videos! Very important!
    // returns the file of the video.
    private File ripAudio(File oldVideo)
    {
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "/Viddy/.audio"); // a temporary folder to hold video audio.
            if(!root.exists()) {
                root.mkdirs(); // make all the directories.
            } else {
                root.delete();
                root.mkdirs();
            }

            File audio = new File(root, "audio.mp3"); // simply grab the audio from the video itself.
            if(!audio.exists()) {
                audio.createNewFile();
            } else {
                audio.delete();
                audio.createNewFile();
            }

            System.out.println("Document Path for video: " + videoPath);
            System.out.println("Name of the video the user has chosen: " + realPath);
            System.out.println("Path of the audio file that we want to store properly: " + audio.getPath());

            File videoFile = new File(realPath);
            System.out.println("Video file name " + videoFile.getName());
            final String[] ffmpegCmd = {"-y", "-i", realPath, "-codec:a", "libmp3lame", "-qscale:a", "0", audio.getAbsolutePath()}; // should create a new file after retrieving audio from the file.
           // final String[] ffmpegCmd = {"-y", "-i", realPath, "-map", "01", "-vn", "-acodec", "copy", audio.getCanonicalPath()}; // test example to see if we can get the proper audio channel.

            // TODO: test this out on multiple phones. The ffmpegCmd that's currently commented out was used before testing. If the audio is off this needs to be fixed.
            FFmpeg.getInstance(vidRipperService).execute(ffmpegCmd, new FFcommandExecuteResponseHandler() {

                    @Override
                    public void onStart() {
                        ffmpegCommandFinished = false;
                    }

                    @Override
                    public void onSuccess(String message)
                    {
                        System.out.println("SUCCESSFULLY completed ffmpeg command!");
                        System.out.println("Message: " + message);
                    }

                    @Override
                    public void onProgress(String message)
                    {
//                        System.out.println("FFmpeg in progress...");
//                        System.out.println("Message: " + message);

                    }

                    @Override
                    public void onFailure(String message)
                    {
                        System.out.println("FFmpeg rip audio COMMAND FAILED...");
                        System.out.println("Message: " + message);
                        ffmpegCommandFinished = true;

                        try {
                            throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.RipAudioError));
                        } catch (ProcessingException ex) {
                            System.out.println("ProcessingException was caught in VidRipperBackgroundThread");
                            ex.printStackTrace();
                        }

                    }

                @Override
                    public void onFinish()
                    {
                        System.out.println("FFmpeg rip audio IS FINISHEd!");
                        ffmpegCommandFinished = true;
                    }
            });

            return audio; // return the audio file.

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Exception attempting to create a new file....");
            return null;
        }

    }

    // This is the final step in terms of making the actual Viddy!
    private File makeViddy(File veme, File audio)
    {
        try
        {
            File root = new File(Environment.getExternalStorageDirectory(), "/Viddy"); // a temporary folder to hold video audio.
            if(!root.exists()) {
                root.mkdirs(); // make all the directories.
            } else {
                root.delete();
                root.mkdirs();
            }


            File viddy = new File(root, filename + ".mp4"); // simply grab the audio from the video itself.
            if(!viddy.exists()) {
                viddy.createNewFile();
            } else {
                viddy.delete();
                viddy.createNewFile();
            }

            File videoFile = new File(realPath);
            System.out.println("Video file name " + videoFile.getName());


            // copies the stream from the audio source into the video
            final String[] ffmpegCmd = {"-y", "-i", veme.getAbsolutePath(), "-i", audio.getAbsolutePath(), "-c:v", "copy",
                    "-c:a", "aac", "-strict", "experimental", viddy.getAbsolutePath()}; // ** replace the -shortest if needed, replace the -crf framerate if the framerate is off.

            // todo: may need to set the framerate
            //final String[] ffmpegCmd = {"-y", "-i", veme.getAbsolutePath(), "-i", audio.getAbsolutePath(), "-c", "copy", "-map", "0:0", "-map", "1:0", viddy.getAbsolutePath()};

            FFmpeg.getInstance(vidRipperService).execute(ffmpegCmd, new FFcommandExecuteResponseHandler() {

                @Override
                public void onStart() {
                    ffmpegCommandFinished = false;
                }

                @Override
                public void onSuccess(String message)
                {
                    System.out.println("SUCCESSFULLY completed ffmpeg command!");
                    System.out.println("Message: " + message);
                }

                @Override
                public void onProgress(String message)
                {
//                    System.out.println("FFmpeg in progress...");
//                    System.out.println("Message: " + message);

                }

                @Override
                public void onFailure(String message)
                {
                    System.out.println("FFmpeg COMMAND FAILED...");
                    System.out.println("Message: " + message);
                    ffmpegCommandFinished = true;

                    try {
                        throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.MakeViddyError));
                    } catch (ProcessingException ex) {
                        System.out.println("ProcessingException was caught in VidRipperBackgroundThread");
                        ex.printStackTrace();
                    }
                }

                @Override
                public void onFinish()
                {
                    System.out.println("FFmpeg IS FINISHEd!");
                    ffmpegCommandFinished = true;
                }
            });

            return viddy; // return the viddy

        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Exception attempting to create a new file....");
            return null;
        }

    }

    // iterates through the entire directory and deletes all of the files.
    private void cleanEditedFramesDir(File editedFramesDir)
    {
        if(editedFramesDir == null) {
            return;
        }

        File[] files = editedFramesDir.listFiles();

        if(files == null) {
            return;
        }

        for (int i = 0; i < files.length; i++)
        {
            if(files[i].exists())
            {
                files[i].delete(); // delete the file.
            }
        }

        if(editedFramesDir.exists()) {
            editedFramesDir.delete(); // delete the directory itself.
        }
    }

    // removes all files from the frames directory.
    private void cleanFramesDir(File framesDir)
    {
        if (framesDir == null) {
            return;
        }

        File[] files = framesDir.listFiles();

        if(files == null) {
            return;
        }

        for (int i = 0; i < files.length; i++)
        {
            if (files[i].exists())
            {
                files[i].delete();
            }
        }

        if (framesDir.exists()) {
            framesDir.delete();
        }
    }

    // removes all files from the video directory.
    private void cleanVideoDir(File videoDir)
    {
        if(videoDir == null) {
            return;
        }

        File[] files = videoDir.listFiles();

        if(files == null) {
            return;
        }

        for (int i = 0; i < files.length; i++)
        {
            if (files[i].exists())
            {
                files[i].delete();
            }
        }

        if (videoDir.exists()) {
            videoDir.delete();
        }
    }

    // removes all files from the frames directory.
    private void cleanAudioDir(File audioDir)
    {
        if(audioDir == null) {
            return;
        }

        File[] files = audioDir.listFiles();

        if(files == null) {
            return;
        }

        for (int i = 0; i < files.length; i++)
        {
            if (files[i].exists())
            {
                files[i].delete();
            }
        }

        if (audioDir.exists()) {
            audioDir.delete();
        }
    }

    // this is what is run when we start the thread. It's important that all vid ripping and updating and image processing occurs in this method.
    // NOTE: we should do the work in this thread in checkpoints to check to see if the user has canceled the meme. If so, the thread needs to be stopped.
    @Override
    public void run()
    {
        if(!isStopped)
        {
            ViddyUtils.getInstance().setFinishedCreatingViddyStatus(false); // starting viddy and thus not finished.
            ViddyUtils.getInstance().changeStep3State(true); // the service is indeed running.
            System.out.println("Video path: " + videoPath);
            System.out.println("Uri path = " + Uri.parse(videoPath).toString());

            String str = videoPath.substring(videoPath.length()-4, videoPath.length());
            System.out.println("str = " + str);


            // todo: this is a very big deal since not all phones will support this version. I will have to warn users that I will not be able to make their video with sound. User's need to know this!
            if(FFmpeg.getInstance(vidRipperService).isSupported()) {
                System.out.println("FFMPEG IS SUPPORTED");
            } else {
                System.out.println("FFMPEG IS NOT Supported!!");
            }

            MediaExtractor extractor = new MediaExtractor();
            int frameRate = 24;
            try {
                //Adjust data source as per the requirement if file, URI, etc.
                extractor.setDataSource(realPath); // FixME: this is breaking now with the real path being passed in from the main activity.
                int numTracks = extractor.getTrackCount();
                for (int i = 0; i < numTracks; ++i) {
                    MediaFormat format = extractor.getTrackFormat(i);
                    String mime = format.getString(MediaFormat.KEY_MIME);
                    if (mime.startsWith("video/")) {
                        if (format.containsKey(MediaFormat.KEY_FRAME_RATE)) {
                            frameRate = format.getInteger(MediaFormat.KEY_FRAME_RATE);
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }finally {
                //Release stuff
                extractor.release();
            }

            System.out.println("REAL VIDEO FRAMERATE = " + frameRate);

            System.out.println("RIPPING VIDEO FRAMES NOW");
            vidRipperService.sendUIMessageUpdate("Processing video frames..."); // fixme: I need to update this to use string so that translation can take place.
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Processing video frames...");

            // IF the user has chosen to stop making viddy, immediately return, no clean up is necessary.
            if(isStopped){
                System.out.println("STOPPING BACKGROUND THREAD...");
                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                //vidRipperService.cancelNotification();
                vidRipperService.displayFinishedNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false);
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                return;
            }

            File framesDir = ripFrames(new File(videoPath));

            System.out.println("Test1: Did the ffmpeg command finish? " + ffmpegCommandFinished);
            while(!ffmpegCommandFinished) {
                //System.out.println("Is the ffmpeg command finished yet? " + ffmpegCommandFinished);

            } // wait for the ffmpeg command to finish

            System.out.println("Test 2: Did the ffmpeg command finish? " + ffmpegCommandFinished);


            // if null, likely the thread was stopped or something back happened, thus the thread needs to return.
            // No cleanup needed of the directories needed since this is the first directory and it wasn't created.
            // if the user has chosen to stop the viddy, then we need to show the message that tells them that they stopped their viddy and no error occurred.
            if (framesDir == null && !isStopped) {

                try {
                    throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.FramesDirNullError));
                } catch (ProcessingException ex) {
                    // no need to catch the exception because the exception handles everything for us.

                }  finally { // this ensures that the rest of the thread finished allowing us to show the proper information to the user.

                    vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage));
                    vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                   // vidRipperService.cancelNotification();
                   // vidRipperService.displayFinishedNotification();
                   // vidRipperService.stopService();
                    ViddyUtils.getInstance().changeStep3State(false);
                    ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                }

                return;
            }

            // if stopped cleanup the directory and kill the thread
            if (isStopped) {
                System.out.println("STOPPING BACKGROUND THREAD...");
                cleanFramesDir(framesDir);
                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                //vidRipperService.cancelNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false);
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                return;
            }

            System.out.println("Making video now!");

            vidRipperService.sendUIMessageUpdate("Starting your viddy..."); // fixme: I need to update this to use string so that translation can take place.
            vidRipperService.sendUIProgressUpdate("1/5"); // remove
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Creating your veme...");
            //File newVideo = remakeVideo(videoFrames, frameRate); // remake the video.
            File editedFrames = editFrames(framesDir); // begin editing frames.

            // if null, likely the thread was stopped by the user or something happened like the directory was deleted. Exist the thread immediately.
            // if the user chose to stop the viddy themselves then we must show the message that they stopped the viddy and not show an error.
            if(editedFrames == null && !isStopped) {

                try {
                    throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.EditedFramesNullError));
                } catch (ProcessingException ex) {
                    // no need to catch the exception because the exception handles everything for us.

                }  finally { // this ensures that the rest of the thread finished allowing us to show the proper information to the user.

                    //vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.NewVideoNullError)); // updates the UI if at all possible.
                    cleanFramesDir(framesDir); // this directory needs to be cleaned up because it likely was created.
                    vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage));
                    vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                    //vidRipperService.cancelNotification();
                    //vidRipperService.stopService();
                    ViddyUtils.getInstance().changeStep3State(false);
                    ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                }


                return;
            }

            // if stopped clean up directories and immediately leave the thread.
            if (isStopped) {
                System.out.println("STOPPING BACKGROUND THREAD...");
                cleanFramesDir(framesDir);
                cleanEditedFramesDir(editedFrames);
                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
               // vidRipperService.cancelNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false);
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                return;
            }

            vidRipperService.sendUIMessageUpdate("Remaking video...");
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Remaking video...");
            File newVideo = remakeVideo(editedFrames, frameRate); // remake the video with the edited frames.

            // clean directories and leave the thread if stopped.
            if (isStopped) {
                cleanFramesDir(framesDir);
                cleanEditedFramesDir(editedFrames);
                cleanVideoDir(newVideo);
                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
               // vidRipperService.cancelNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false);
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                return;
            }

            //while(!ffmpegCommandFinished) {} // wait for the ffmpeg command to finish

            System.out.println("Video is done! Ripping audio from old video...");

            // todo: need to check for the file
            File audioFile = ripAudio(new File(videoPath)); // send in the file of the old video.

            // if the user stopped the viddy then we must not show a new viddy. This is very very very important.
            if(newVideo == null && !isStopped) {
                System.out.println("New video null, quitting...");

                // this try catch is used strictly for throwing our exceptions
                try {
                    throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.NewVideoNullError));
                } catch (ProcessingException ex) {
                    // no need to catch the exception because the exception handles everything for us.

                }  finally { // this ensures that the rest of the thread finished allowing us to show the proper information to the user.

                    //vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.NewVideoNullError)); // updates the UI if at all possible.
                    vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage)); // tells the user that there was an error.
                    vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                    //vidRipperService.cancelNotification();
                   // vidRipperService.stopService();
                    ViddyUtils.getInstance().changeStep3State(false); // viddy is no longer running.
                    ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                }

                return; // finish the thread.
            }

            // check to make sure the user didn't stop the viddy before showing the error message.
            if(audioFile == null && !isStopped) {
                System.out.println("Audio file is null, warn the user and ask if they want to keep the sound free meme or try again...");
               // vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.AudioNullError)); // updates the UI if at all possible.

                try {
                    throw new ProcessingException(vidRipperService, vidRipperService.getString(R.string.AudioNullError));
                } catch (ProcessingException ex) {
                    // no need to catch the exception because the exception handles everything for us.

                }  finally { // this ensures that the rest of the thread finished allowing us to show the proper information to the user.

                    //vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.NewVideoNullError)); // updates the UI if at all possible.
                    vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage)); // tells the user that there was an error.
                    vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                    //vidRipperService.cancelNotification();
                    //vidRipperService.stopService();
                    ViddyUtils.getInstance().changeStep3State(false); // viddy is no longer running.
                    ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                }

            }
            // TODO: be sure to delete the directory after the video has been made!
            //mmd.release(); // release the memory.

            vidRipperService.sendUIMessageUpdate("Re-adding audio to your veme..."); // fixme: I need to update this to use string so that translation can take place.
            vidRipperService.sendUIProgressUpdate("2/5");
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Re-adding audio to your veme...");
            ViddyUtils.getInstance().updateStep3ProgressText("2/5");

            System.out.println("Waiting for audio rip command to complete.");
            while(!ffmpegCommandFinished) {} // wait for audio rip command to complete.

            if (isStopped) {
                System.out.println("STOPPING BACKGROUND THREAD...");
                cleanVideoDir(newVideo);
                cleanFramesDir(framesDir);
                cleanEditedFramesDir(editedFrames);
                cleanAudioDir(audioFile);
                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
               // vidRipperService.cancelNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false); // viddy is no longer running.
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.

                //vidRipperService.cancelNotification();
                vidRipperService.stopService();
                return;
            }

            File viddy = makeViddy(newVideo, audioFile); // make the final viddy.

            vidRipperService.sendUIProgressUpdate("3/5");
            vidRipperService.sendUIMessageUpdate("Creating your Viddy..."); // fixme: I need to update this to use string so that translation can take place.
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Creating your Viddy...");
            ViddyUtils.getInstance().updateStep3ProgressText("3/5");
            System.out.println("waiting for viddy create command to complete.");
            while (!ffmpegCommandFinished) {} // wait for viddy to be created.

            vidRipperService.sendUIMessageUpdate("4/5");
            vidRipperService.sendUIMessageUpdate("Cleaning up...");
            ViddyUtils.getInstance().updateStep3ProgressMessageText("Cleaning up...");
            ViddyUtils.getInstance().updateStep3ProgressText("4/5");

            // if stopped at this point the viddy will be created no matter what. So we must delete the viddy but don't stop the thread; let the thread finish clean-up below!
            if(isStopped) {
                System.out.println("STOPPING BACKGROUND THREAD...");
                if(viddy.exists()) {
                    viddy.delete();
                }

                // clean up the directories now that the video has been completed.
                cleanAudioDir(audioFile);
                cleanEditedFramesDir(editedFrames);
                cleanFramesDir(framesDir);
                cleanVideoDir(newVideo);

                vidRipperService.sendViddyStoppedMessage(vidRipperService.getString(R.string.UserStoppedViddyError)); // updates the UI if at all possible.

                vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.StoppingMessage));
                vidRipperService.sendUIProgressUpdate(""); // send an empty string to show nothing.
                vidRipperService.cancelNotification();
                vidRipperService.stopService();
                ViddyUtils.getInstance().changeStep3State(false); // viddy is no longer running.
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.


            } else { // do operations as normal

                // clean up the directories now that the video has been completed.
                cleanAudioDir(audioFile);
                cleanEditedFramesDir(editedFrames);
                cleanFramesDir(framesDir);
                cleanVideoDir(newVideo);

                System.out.println("DONE THE VIDDY SHOULD BE COMPLETED!");
                vidRipperService.sendUIMessageUpdate("Done!"); // fixme: I need to update this to use string so that translation can take place.
                vidRipperService.sendUIProgressUpdate("5/5");
                ViddyUtils.getInstance().updateStep3ProgressMessageText("Done!");
                ViddyUtils.getInstance().updateStep3ProgressText("5/5");

                vidRipperService.displayFinishedNotification(); // show the finished notification for the users to see.
                vidRipperService.stopService();

                ViddyUtils.getInstance().setCreatedViddy(viddy); // set the viddy that is used by the user.
                ViddyUtils.getInstance().changeStep3State(false); // viddy is no longer running.
                ViddyUtils.getInstance().setFinishedCreatingViddyStatus(true); // truly finished with creating the viddy.
                vidRipperService.sendStep3FinishedDetailsMessage(); // sends the message to tell the step 3 activity to show the finished details of the current instance.
            }
        }
    }

}
