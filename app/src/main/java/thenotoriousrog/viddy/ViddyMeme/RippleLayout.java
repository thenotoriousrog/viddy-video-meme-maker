package thenotoriousrog.viddy.ViddyMeme;

import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.RelativeLayout;

import thenotoriousrog.viddy.R;

/*
    * Code heavily inspired by this repo: https://github.com/gaurav414u/android-ripple-pulse-animation/blob/master/ripplepulsebackground/src/main/java/com/gauravbhola/ripplepulsebackground/RipplePulseLayout.java
    * Modified to fit the Viddy implementation.
    * Oddities: Something that is not immediately obvious here is the use of Strings to change the color of the ripple. I had a problem that using the color wouldn't work the way we imagined. So I had to change it using strings.
 */
public class RippleLayout extends RelativeLayout {
    public static final int DEFAULT_DURATION = 2000;
    public static final String RIPPLE_TYPE_FILL = "0";
    public static final String RIPPLE_TYPE_STROKE = "1";
    public static final String TAG = "RipplePulseLayout";

    private Paint mPaint;
    private AnimatorSet mAnimatorSet;
    private boolean mIsAnimating;
    private RippleView mRippleView;
    private int color;
    private Context context;
    private AttributeSet attrs;
    private String colorVal = "";


    public RippleLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        this.attrs = attrs;
        init(context, attrs);
    }

    public RippleLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        this.attrs = attrs;
        init(context, attrs);
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    public RippleLayout(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.context = context;
        this.attrs = attrs;
        init(context, attrs);
    }

    /**
     * Initializes the required ripple views
     * @param context
     * @param attrs
     */
    private void init(Context context, AttributeSet attrs) {
//        if (isInEditMode()) {
//            return;
//        }
        //reading the attributes
        TypedArray attrValues = context.obtainStyledAttributes(attrs, R.styleable.RippleLayout);

        // only use the default color if no color is present.
        if (colorVal.isEmpty()) {
            color = ContextCompat.getColor(getContext(), R.color.colorComplimentary);//attrValues.getColor(R.styleable.RippleLayout_rippleColor, ContextCompat.getColor(getContext(), R.color.colorComplimentary));
        } else {

            if (colorVal.equalsIgnoreCase("Error")) { // Something is wrong when processing alert the user what happened so that they know.
                this.color = ContextCompat.getColor(getContext(), R.color.Red);
            } else if (colorVal.equalsIgnoreCase("Success")) { // the viddy was completed.
                this.color = ContextCompat.getColor(getContext(), R.color.green);
            } else if (colorVal.equalsIgnoreCase("Stopped")) { // the stopped color is not an error but not a success either.
                this.color = ContextCompat.getColor(getContext(), R.color.orange);
            } else { // if some weird error occurs show the color complimentary color.
                this.color = ContextCompat.getColor(getContext(), R.color.colorComplimentary);
            }
        }

        float startRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 0, context.getResources().getDisplayMetrics());;//attrValues.getDimension(R.styleable.RippleLayout_startRadius, getMeasuredWidth());
        float endRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 30, context.getResources().getDisplayMetrics());//attrValues.getDimension(R.styleable.RippleLayout_endRadius, getMeasuredWidth() * 2);
        float strokeWidth = 4.0f;//attrValues.getDimension(R.styleable.RippleLayout_strokeWidth, 4);
        int duration = 800;//attrValues.getInteger(R.styleable.RippleLayout_duration, DEFAULT_DURATION);
        String rippleType = "fill"; //attrValues.getString(R.styleable.RippleLayout_rippleType);
        if (TextUtils.isEmpty(rippleType)) {
            rippleType = RIPPLE_TYPE_FILL;
        }
        //initialize stuff
        initializePaint(color, rippleType, strokeWidth);
        initializeRippleView(endRadius, startRadius, strokeWidth);
        initializeAnimators(startRadius, endRadius, duration);
    }

    private void initializeRippleView(float endRadius, float startRadius, float strokeWidth) {
        mRippleView = new RippleView(getContext(), mPaint, startRadius);
        LayoutParams params = new LayoutParams(2 * (int)(endRadius + strokeWidth), 2 * (int)(endRadius + strokeWidth));
        params.addRule(CENTER_IN_PARENT, TRUE);
        addView(mRippleView, params);
        mRippleView.setVisibility(INVISIBLE);
    }

    private void initializeAnimators(float startRadius, float endRadius, int duration) {
        mAnimatorSet = new AnimatorSet();
        ObjectAnimator scaleXAnimator = ObjectAnimator.ofFloat(mRippleView, "radius", startRadius, endRadius);
        scaleXAnimator.setRepeatCount(ValueAnimator.INFINITE);

        ObjectAnimator alphaAnimator = ObjectAnimator.ofFloat(mRippleView, "alpha", 1f, 0f);
        alphaAnimator.setRepeatCount(ValueAnimator.INFINITE);

        mAnimatorSet.setDuration(duration);
        mAnimatorSet.setInterpolator(new AccelerateDecelerateInterpolator());
        mAnimatorSet.playTogether(scaleXAnimator, alphaAnimator);
    }

    /**
     * Starts the ripple animation
     */
    public void startRippleAnimation() {
        if (mIsAnimating) {
            //already animating
            return;
        }
        mRippleView.setVisibility(View.VISIBLE);
        mAnimatorSet.start();
        mIsAnimating = true;
    }

    /**
     * Stops the ripple animation
     */
    public void stopRippleAnimation() {
        //if (!mIsAnimating) {
            //already not animating
          //  return;
        //}
        //mAnimatorSet.end();
        mAnimatorSet.cancel();
        mRippleView.setVisibility(View.INVISIBLE);
        mIsAnimating = false;
    }

    /*
        * This method alerts the layout what color that we should be displaying. This can be either success, error, or stopped to show the different colors.
     */
    public void setColor(String colorVal) {
        this.colorVal = colorVal;
        init(context, attrs);
        invalidate();
    }

    private void initializePaint(int color, String rippleType, float strokeWidth) {
        mPaint = new Paint();
        mPaint.setColor(color);
        mPaint.setAntiAlias(true);
        if (rippleType.equals(RIPPLE_TYPE_STROKE)) {
            mPaint.setStyle(Paint.Style.STROKE);
            mPaint.setStrokeWidth(strokeWidth);
        } else {
            mPaint.setStyle(Paint.Style.FILL);
            mPaint.setStrokeWidth(0);
        }
    }

    private static class RippleView extends View {
        private Paint mPaint;
        private float mRadius;

        public float getRadius() {
            return mRadius;
        }

        public void setRadius(float radius) {
            mRadius = radius;
            invalidate();
        }

        public RippleView(Context context, Paint p, float radius) {
            super(context);
            mPaint = p;
            mRadius = radius;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            int centerX = getWidth()/2;
            int centerY = getHeight()/2;
            canvas.drawCircle(centerX, centerY, mRadius, mPaint);
        }
    }
}
