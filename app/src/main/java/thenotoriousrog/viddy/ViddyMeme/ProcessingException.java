package thenotoriousrog.viddy.ViddyMeme;

import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsVidRipperService;
import thenotoriousrog.viddy.R;

/*
    * This exception is meant to handle the behavior for if their is some processing error on the side of FFMPEG. This results in sending a failed message to the user via the UI and
    * the notifications. This will allow for maximum grabbing of errors and displaying that information to the user.
 */
public class ProcessingException extends Exception {

    // this constructor is used for when we use the vid ripper service for Viddy.
    public ProcessingException(VidRipperService vidRipperService, String exceptionMsg) {
        super(exceptionMsg); // makes a call to the Exception superclass.

        vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage)); // shows a simple "ERROR" in whatever language the user is using.
        vidRipperService.sendUIProgressUpdate(""); // shows nothing.
        vidRipperService.sendViddyStoppedMessage(exceptionMsg); // sends the stopped signal for the user to view and why the error occurred.
        vidRipperService.updateNotificationProgress(vidRipperService.getString(R.string.NotificationErrorMessage)); // tells the user to click the notification to figure out what happened.
        vidRipperService.updateNotificationUpdateMessage(vidRipperService.getString(R.string.ErrorMessage)); // simply shows ERROR for the user to make use of. This is very important.
        vidRipperService.stopService(); // have the service be stopped by the user.
    }

    // This constructor will handle the behavior if a breaking news vid ripper service is used.
    public ProcessingException(BreakingNewsVidRipperService vidRipperService, String exceptionMsg) {
        super(exceptionMsg);

        vidRipperService.sendUIMessageUpdate(vidRipperService.getString(R.string.ErrorMessage)); // shows a simple "ERROR" in whatever language the user is using.
        vidRipperService.sendUIProgressUpdate(""); // shows nothing.
        vidRipperService.sendViddyStoppedMessage(exceptionMsg); // sends the stopped signal for the user to view and why the error occurred.
        vidRipperService.updateNotificationProgress(vidRipperService.getString(R.string.NotificationErrorMessage)); // tells the user to click the notification to figure out what happened.
        vidRipperService.updateNotificationUpdateMessage(vidRipperService.getString(R.string.ErrorMessage)); // simply shows ERROR for the user to make use of. This is very important.
        vidRipperService.stopService(); // have the service be stopped by the user.

    }

}
