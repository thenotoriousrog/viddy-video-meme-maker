package thenotoriousrog.viddy.ViddyMeme.Handlers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.ViddyMeme.ViddyStep3Fragment;

/*
    * This class is in charge of sending updates to the UI
 */
public class UIHandler extends Handler {

    private ViddyStep3Fragment viddyStep3Fragment; // the fragment where we will be sending updates too.
    private static UIHandler instance = null;

    public UIHandler(ViddyStep3Fragment viddyStep3Fragment, Looper looper)
    {
        super(looper);
        this.viddyStep3Fragment = viddyStep3Fragment;
        instance = this;
    }

    public static UIHandler getInstance() {
        return instance;
    }

    // updates the UI progress counter in the UIHandler.
    private void updateUIProgressCounter(Message msg)
    {
        String progress = msg.getData().getString("Progress");
        viddyStep3Fragment.setProgressCounterText(progress); // set the progress text.
    }

    private void updateUIProgressMessage(Message msg)
    {
        String message = msg.getData().getString("Message");
        viddyStep3Fragment.setProgressMessage(message);
    }

    // updates the UI causing the next activity to be shown!
    private void finishStep3()
    {
        viddyStep3Fragment.finishStep3();
    }

    // makes the step3Activity show the finished details.
    private void showFinishedDetails() {
        viddyStep3Fragment.showFinishedDetails();
    }

    // tells the step 3 activity that we have stopped their viddy and handles it accordingly.
    private void showStoppedDetails(Message msg) {
        String message = msg.getData().getString("ErrorMessage");
        viddyStep3Fragment.showStoppedDetails(message); // tells the step3Activity to show the stopped behavior instead of the finished behavior.
    }

    // All messages will be in here.
    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.arg1)
        {
            case Constants.UI.UI_UPDATE_PROGRESS:
                updateUIProgressCounter(msg);
                break;

            case Constants.UI.UI_UPDATE_MESSAGE:
                updateUIProgressMessage(msg);
                break;

            case Constants.UI.UI_FINISH_STEP3:
                finishStep3();
                break;

            case Constants.UI.UI_SHOW_STEP3_FINISHED_DETAILS:
                showFinishedDetails();
                break;

            case Constants.UI.UI_SHOW_VIDDY_STOPPED_MESSAGE:
                showStoppedDetails(msg);
                break;

        }
    }

}
