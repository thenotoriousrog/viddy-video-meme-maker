package thenotoriousrog.viddy.ViddyMeme.Handlers;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.ViddyMeme.VidRipperService;

/*
    * This handler communicates UI actions back to the vid ripper service when needed.
    * Mostly used to send the message back to the
 */
public class VidRipperHandler extends Handler {

    private VidRipperService vidRipperService;
    private static VidRipperHandler instance = null;

    public VidRipperHandler(VidRipperService vidRipperService, Looper looper) {
        super(looper); // sends the looper to Handler superclass.
        this.vidRipperService = vidRipperService;
        instance = this;
    }

    public static VidRipperHandler getInstance() {
        return instance;
    }

    // sends the signal to stop the background thread in the viddy service.
    private void sendStopBackgroundThreadSignal() {
        vidRipperService.sendStopVidRipperBackgroundThreadSignal();
    }

    // Handles all of the messages from the messages that need to be sent to the vid ripper service.
    @Override
    public void handleMessage(Message msg)
    {
        switch (msg.arg1)
        {
            case Constants.ACTION.STOP_VID_RIPPER_THREAD_SIGNAL:
                sendStopBackgroundThreadSignal();
                break;
        }
    }

}
