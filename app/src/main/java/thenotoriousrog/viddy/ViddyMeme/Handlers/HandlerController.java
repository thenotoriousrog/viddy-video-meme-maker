package thenotoriousrog.viddy.ViddyMeme.Handlers;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;

import thenotoriousrog.viddy.Backend.Constants;

/*
    * This class is responsible for sending the proper messages between the service and the UI.
 */
public class HandlerController extends Handler {

    private UIHandler uiHandler; // an instance of the uiHandler.
    private VidRipperHandler vidRipperHandler; // as instance of the vid ripper handler to handle callbacks from the UI.

    public HandlerController(Looper looper)
    {
        super(looper);
    }

    // updates the progress of the UI.
    private void sendUIUpdateProgress(Message m)
    {
        Message msg = uiHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_UPDATE_PROGRESS; // tell the handler what type of update this is.
        String progress = m.getData().getString("Progress"); // get's the progress percentage.
        Bundle b = new Bundle();
        b.putString("Progress", progress); // put the progress string.

        msg.setData(b);

        uiHandler.sendMessage(msg); // send the message to the UIHandler.
    }

    // updates the message to the ui immediatly.
    private void sendUIUpdateMessage(Message m)
    {
        Message msg = new Message();//uiHandler.obtainMessage();
        msg.arg1 = Constants.UI.UI_UPDATE_MESSAGE; // tell the handler what type of update this is.
        String message = m.getData().getString("Message"); // get's the progress percentage.
        Bundle b = new Bundle();
        b.putString("Message", message); // put the progress string.
        msg.setData(b);

        uiHandler.sendMessage(msg); // send the message to the UIHandler.
    }

    // tells the system to start the step 4 activity. This is easily done thanks to the ViddyUtils.
    private void sendUIStep3FinishedDetailsMessage()
    {
        Message msg = new Message();
        msg.arg1 = Constants.UI.UI_SHOW_STEP3_FINISHED_DETAILS;
        uiHandler.sendMessage(msg);
    }

    // tells the UI to display the error checkmark and to tell the user what exactly happened.
    private void sendUIViddyStoppedMessage(Message m)
    {
        Message msg = new Message();
        msg.arg1 = Constants.UI.UI_SHOW_VIDDY_STOPPED_MESSAGE;
        String message = m.getData().getString("ErrorMessage"); // gets the error message and attempts to send it to the ui to handle.
        Bundle b = new Bundle();
        b.putString("ErrorMessage", message);
        msg.setData(b);
        uiHandler.sendMessage(msg);
    }

    // sends a message to the vid ripper handler to tell the vid ripper service to stop the background thread whenever possible.
    private void sendStopBackgroundThreadSignalMessage() {
        Message msg = new Message();
        msg.arg1 = Constants.ACTION.STOP_VID_RIPPER_THREAD_SIGNAL;
        vidRipperHandler.sendMessage(msg);
    }

    // All messages will be in here.
    @Override
    public void handleMessage(Message msg)
    {
        uiHandler = UIHandler.getInstance(); // get an instance of the UIHandler.
        vidRipperHandler = VidRipperHandler.getInstance(); // get the instance of the vidRipperHandler.

        switch (msg.arg1)
        {
            case Constants.UI.UI_UPDATE_PROGRESS:
                sendUIUpdateProgress(msg);
                break;

            case Constants.UI.UI_UPDATE_MESSAGE:
                sendUIUpdateMessage(msg);
                break;

            case Constants.UI.UI_SHOW_STEP3_FINISHED_DETAILS:
                sendUIStep3FinishedDetailsMessage();
                break;

            case Constants.UI.UI_SHOW_VIDDY_STOPPED_MESSAGE:
                sendUIViddyStoppedMessage(msg);
                break;

            case Constants.ACTION.STOP_VID_RIPPER_THREAD_SIGNAL:
                sendStopBackgroundThreadSignalMessage();
                break;
        }

    }

}
