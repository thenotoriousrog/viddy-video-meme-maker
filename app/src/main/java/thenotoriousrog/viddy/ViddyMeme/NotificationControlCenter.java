package thenotoriousrog.viddy.ViddyMeme;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.R;

/*
    * This class is in charge of updating and handling the intents from the VidRipperService.
    * It's also in charge of creating the notification itself needed for the foreground service.
 */
public class NotificationControlCenter {

    private VidRipperService vidRipperService; // the vid ripper service we will be using to make calls to.
    private NotificationManager notificationManager; // controls the building of the notifications.
    private NotificationCompat.Builder builder; // builder for the notification compat.
    private NotificationCompat.Action actionCancel;  // cancels the service, stopping the veme from being made

    public NotificationControlCenter(VidRipperService vidRipperService)
    {
        this.vidRipperService = vidRipperService;
        initializeNotification(); // creates the notifications.
    }

    // displays the notification for the user.
    public void displayNotification()
    {
        notificationManager.notify(0001, builder.build()); // begin displaying the notification.
    }

    // builds the notification and returns it. This is likely passed back to our Vid Ripper Service.
    public Notification getNotification() {
        return builder.build();
    }

    // cancels all of the notifications.
    public void cancelNotification(){
        notificationManager.cancelAll();
    }

    // updates the progress of the notification itself.
    public void updateProgress(String progress)
    {
        builder.setContentText(progress);
        notificationManager.notify(0001, builder.build());
    }

    // updates the message of the notification to match the UI
    public void updateMessage(String message)
    {
        builder.setContentTitle(message);
        notificationManager.notify(0001, builder.build());
    }

    // Displays a new notification that the user will be able to see showing that their viddy is finished!
    public void displayedFinishedNotification()
    {
        // TODO: When step4Activity is finished, we need to use Step4Activity not step3Activity so that step4Activity is grabbed!
        Intent openAppIntent = new Intent(vidRipperService, MainActivity.class); // open up the step 3 activity with the loading view.
        openAppIntent.setAction("Main");
        openAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP ); // **Note: this may have to change if the behavior isn't what we were striving for.

        PendingIntent pendingOpenAppIntent = PendingIntent.getActivity(vidRipperService, 0, openAppIntent, 0); // opens the app when the notification is touched.

        builder = new NotificationCompat.Builder(vidRipperService.getBaseContext(), "Progress Notification")
                .setSmallIcon(R.drawable.official_icon_med) // fixme: remove this and change it to be our custom icon for the app.
                .setContentTitle("Done!")
                .setContentText("Your Viddy was created successfully!")
                .setOnlyAlertOnce(true)
                .setOngoing(false) // users cannot remove this notification while it's in progress.
                .setContentIntent(pendingOpenAppIntent)
                .setAutoCancel(true)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        notificationManager.notify(0002, builder.build()); // a new id will show our new notification that can be swiped away.

    }

    // begins setting and initializing the notification to be used for the foreground service.
    private void initializeNotification()
    {
        notificationManager = (NotificationManager) vidRipperService.getBaseContext().getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) // only in oreo and above is there notification channels to worry about.
        {
            String description = "Displays progress of the meme";
            NotificationChannel channel = new NotificationChannel("Progress Notification", "Progress Notification", NotificationManager.IMPORTANCE_LOW);
            channel.setDescription(description);
            channel.enableLights(false);
            channel.enableVibration(false);

            // Register the channel with the system
            notificationManager.createNotificationChannel(channel);
        }

        // Retrieve all of the values needed for the notification to launch the activity.
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(vidRipperService.getBaseContext());
        String savedVidPath = preferences.getString("VideoPath", null);
        String savedVemeText = preferences.getString("VemeText", null);
        boolean withWatermark = preferences.getBoolean("WithWatermark", true); // default is true.
        int savedTextSize =  preferences.getInt("TextSize", 30); // default value is 30sp

        Intent openAppIntent = new Intent(vidRipperService, MainActivity.class); // open up the step 3 activity with the loading view.
        openAppIntent.setAction("Main");
        openAppIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP ); // **Note: this may have to change if the behavior isn't what we were striving for.
        Bundle args = new Bundle();
        args.putString("VideoPath", savedVidPath); // the path of the video that the user has selected.
        args.putString("VemeText", savedVemeText); // put the meme text that the user has written.
        args.putBoolean("WithWatermark",withWatermark); // make the meme with the watermark, default value is true.
        args.putFloat("TextSize", savedTextSize);
        //args.putBoolean("Step3Started", true); // always true is touching the nofication, this will ensure that the thread is not started again.

        openAppIntent.putExtras(args); // put the arguments for the Intent to reopen the app.

        Intent cancelIntent = new Intent(vidRipperService, VidRipperService.class);
        cancelIntent.setAction("Cancel");

        PendingIntent pendingOpenAppIntent = PendingIntent.getActivity(vidRipperService, 0, openAppIntent, 0); // opens the app when the notification is touched.
        PendingIntent pendingCancelIntent = PendingIntent.getService(vidRipperService, 4545, cancelIntent, PendingIntent.FLAG_CANCEL_CURRENT); // cancels the Veme being made and stops the service.

        actionCancel = new NotificationCompat.Action.Builder(R.drawable.ic_block_black_24dp, "CANCEL", pendingCancelIntent).build();

        // create the notification but know that the is not displayed nor is it built yet.
        builder = new NotificationCompat.Builder(vidRipperService.getBaseContext(), "Progress Notification")
                .setSmallIcon(R.drawable.official_icon_med) // fixme: remove this and change it to be our custom icon for the app.
                .setContentTitle("Progress...")
                .setContentText("")
                .addAction(actionCancel) // set the action to cancel the notification.
                .setOnlyAlertOnce(true)
                .setOngoing(true) // users cannot remove this notification while it's in progress.
                .setContentIntent(pendingOpenAppIntent)
                .setDeleteIntent(pendingCancelIntent)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);

        // TODO: set the behavior for the service being canceled!

    }

}
