package thenotoriousrog.viddy.ViddyMeme;

import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.ViddyMeme.Handlers.HandlerController;
import thenotoriousrog.viddy.ViddyMeme.Handlers.VidRipperHandler;


/*
    This class is in charge of ripping the video and displaying the proper notification to the app itself.
 */
public class VidRipperService extends Service {

    private static final String TAG = "VidRipperService"; // tag for our logcat.
    private boolean speakWithUI = true; // this tells the system to actively send updates to the UI as we are creating the video. Very important to get this right!
    private NotificationControlCenter notificationControlCenter; // the notification control center to control the updates to the notification.
    private IBinder serviceBinder = new ServiceBinder(); // a copy of the service binder.
    private String videoPath; // path of the video that the user selected.
    private String realPath; // the real system path to the video file.
    private String vemeText = ""; // the text that the user wrote for the meme.
    private String customWatermark = "";
    private String filename = ""; // the filename that the user has given for their file.
    private boolean withWatermark = true; // whether or not we are generating this meme with the watermark.
    private int textSize; // the size of the text that the user chose
    private HandlerController handlerController; // the controller that will control the messages between the UI and the VidRipperService.
    private VidRipperBackgroundThread vidRipperBackgroundThread; // the background thread for the service.
    private boolean finishedNotificationDisplayed = false; // tells us if the final notification is displayed.
    private VidRipperHandler vidRipperHandler; // the handler that we are creating to be used from within the vidRipperService.

    // to be used to bind with step3 activity. Essentially, we will stop updating the UI when the activity is not bounded.
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        speakWithUI = true; // the activity is alive, start sending updates to the UI!
        return serviceBinder;
    }

    // This is called when the activity is unbounded i.e. when the user or the OS kills the app.
    @Override
    public boolean onUnbind(Intent intent) {
        speakWithUI = false; // activity is dead, stop speaking with the UI.
        if (finishedNotificationDisplayed)
        {
            cancelNotification(); // should cancel all notifications.
        }

        return true;
    }

    // Service Binder class to allow to make a call to the new binder.
    public class ServiceBinder extends Binder
    {
        public VidRipperService getService()
        {
            return VidRipperService.this; // retrieve the instance of this service.
        }
    }

    public void setSpeakWithUIState(boolean state) {
        this.speakWithUI = state;
    }

    // The service is created here, we should do all instantiation of the Notification and everything for this in here. However, we get no information from the activity here.
    @Override
    public void onCreate()
    {
        super.onCreate();
        notificationControlCenter = new NotificationControlCenter(this); // create a new notification control center.
        handlerController = new HandlerController(getMainLooper());
        vidRipperHandler = new VidRipperHandler(this, Looper.getMainLooper()); // creates the vid ripper handler to be used throughout the application.

        startForegroundNotification(); // begins displaying notifications now.
    }

    private void startForegroundNotification()
    {
        startForeground(1, notificationControlCenter.getNotification()); // displays the foreground service with the notification to show updates for the user.

        notificationControlCenter.displayNotification(); // displays the notification. **note: may not need this and/or may cause problem if we try to use it.
    }

    // used to update the update message in the notification.
    public void updateNotificationUpdateMessage(String message) {
        notificationControlCenter.updateMessage(message);
    }

    // used to update the progress of the viddy. This is primarily used for error processing.
    public void updateNotificationProgress(String message) {
        notificationControlCenter.updateProgress(message);
    }

    public void sendUIProgressUpdate(String percentage)
    {
        notificationControlCenter.updateProgress(percentage); // update the progress on the notification as well.

        if(speakWithUI) // if speaking with UI update the UI progress.
        {
            Message msg = handlerController.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_PROGRESS;
            Bundle b = new Bundle();
            b.putString("Progress", percentage);
            msg.setData(b);
            handlerController.sendMessage(msg); // send the message to the handler.
        }
    }

    public void sendUIMessageUpdate(String message)
    {
        notificationControlCenter.updateMessage(message);

        if(speakWithUI) // if speaking with UI update the UI progress.
        {
            Message msg = new Message();//handlerController.obtainMessage();
            msg.arg1 = Constants.UI.UI_UPDATE_MESSAGE;
            Bundle b = new Bundle();
            b.putString("Message", message);
            msg.setData(b);
            handlerController.sendMessage(msg); // send the message to the handler.
        }
    }

    // this will completely remove the notification. This is called when the user cancels the viddy.
    public void cancelNotification() {
        notificationControlCenter.cancelNotification(); // kill the notification.
    }

    // displays a notification that the user's viddy is finished!
    public void displayFinishedNotification() {
        this.finishedNotificationDisplayed = true; // displaying the final notification thus must be true.
        notificationControlCenter.displayedFinishedNotification();
    }

    // sends the signal to activate the activity for step 4.
    public void sendStep3FinishedDetailsMessage() {

        if(speakWithUI) { // if speaking with the UI begin the step4Activity for the user.
            Message msg = new Message();
            msg.arg1 = Constants.UI.UI_SHOW_STEP3_FINISHED_DETAILS; // send the argument to start the step 4 activity.
            handlerController.sendMessage(msg);
        }
    }

    // This method is called whenever the viddy was stopped and sends the error message for whatever that may be.
    public void sendViddyStoppedMessage(String errorMessage)
    {
        if(speakWithUI) { // send stopped message if we are speaking with the UI
            Message msg = new Message();
            msg.arg1 = Constants.UI.UI_SHOW_VIDDY_STOPPED_MESSAGE; // tells the UI to display the proper information for the user to view.
            Bundle b = new Bundle();
            b.putString("ErrorMessage", errorMessage);
            msg.setData(b);
            handlerController.sendMessage(msg);
        }
    }

    // tells the vidRipperBackgroundThread to stop whenever possible. This is very very very important.
    public void sendStopVidRipperBackgroundThreadSignal() {
        vidRipperBackgroundThread.stopBackgroundThread();
    }

    // begins shutting down the service
    public void stopService()
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        SharedPreferences.Editor editor =  preferences.edit(); // create an editor to make changes to what is saved to main memory.
        editor.putInt("CurrentActivity", Constants.ACTIVITY.NO_ACTIVITY); // put the ID for the
       // editor.putBoolean("Step3Started", false); // since step 3 has finished. We need to change the state to false.
        editor.commit();

        if(speakWithUI) {
            Message msg = new Message();
            msg.arg1 = Constants.UI.UI_FINISH_STEP3;
            handlerController.sendMessage(msg); // send the finish step3ID
            stopForeground(true); // remove the notification. If we stopped while in the app, the notification can go away.
        } else {
            stopForeground(false); // is not speaking with the UI keep the notification.
        }
        stopSelf(); // calling this will begin the shutdown process of the vidRipperService.
    }

    // When the service is started, this method is called to begin the service itself.
    @Override
    public int onStartCommand(Intent intent, int flags, int startId)
    {
        if(intent == null) { // if the intent is null this is likely because the service was recalled but not from an active activity.

            speakWithUI = false; // the activity is dead, we must stop updating the ui.
        } else { // the service was called from the activity, begin parsing the video now. Very important

            String str = intent.getAction();

            if(str.equalsIgnoreCase("Cancel")) // if cancel the user wants to stop the background thread.
            {
                System.out.println("CANCEL WAS CLICKED!");
                sendUIMessageUpdate("Stopping..."); // this will send a message update to tell the user that we are stopping the background thread.
                //cancelNotification(); // kill the notification since cancel was clicked.
                vidRipperBackgroundThread.stopBackgroundThread(); // stop the background thread at the earliest checkpoint.
               // stopService(); // immediately begins destroying this service

            } else {
                System.out.println("Action from the INTENT == " + str);
                System.out.println("flags from intent = " + flags);

                speakWithUI = true;

                Bundle args = intent.getExtras();
                videoPath = args.getString("VideoPath");
                realPath = args.getString("RealPath");
                vemeText = args.getString("VemeText");
                customWatermark = args.getString("CustomWatermark");
                textSize = args.getInt("TextSize");
                withWatermark = args.getBoolean("WithWatermark", true); // default value is true to ensure the watermark is generated if something happens.
                filename = args.getString("filename", "VIDDY"); // default name of VIDDY for the user
                // alreadyStarted = args.getBoolean("Step3Started", false); // default value is false to ensure that the thread is started.

                if(!ViddyUtils.getInstance().isStep3Running()) // if not started already, restart the thread.
                {

                    System.out.println("THE VID RIPPER BACKGROUND THREAD IS NOT RUNNING, CREATING A NEW THREAD NOW");
                    //Log.i(TAG, "THE VID RIPPER BACKGROUND THREAD IS NOT RUNNING, CREATING A NEW THREAD NOW");
                    ViddyUtils.getInstance().changeStep3State(true); // change the state to be running now.
                    vidRipperBackgroundThread = new VidRipperBackgroundThread(this, videoPath, realPath, vemeText, customWatermark, textSize, withWatermark, filename);
                    vidRipperBackgroundThread.startBackgroundThread(); // start the background thread.

                    System.out.println("Finished");
                }
                else {
                    System.out.println("VID RIPPER BACKGROUND IS RUNNING, NOT CREATING A NEW THREAD NOW!");
                    //Log.i(TAG, "VID RIPPER BACKGROUND IS RUNNING, NOT CREATING A NEW THREAD NOW!");
                }
            }
        }


        return START_NOT_STICKY; // this means if the service is killed the activity is lost forever.
    }

    // When this is called the service is being destroyed and we need to release all of the resources that could cause memory leaks.
    @Override
    public void onDestroy()
    {
        // todo: unbind everything and stop the service...

        // handle the gentle destroying of this activity now.
        super.onDestroy();
    }
}
