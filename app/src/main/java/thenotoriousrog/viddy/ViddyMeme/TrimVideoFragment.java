package thenotoriousrog.viddy.ViddyMeme;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.appyvet.materialrangebar.RangeBar;

import java.util.concurrent.TimeUnit;

import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.BreakingNewsViddy.BNVTrimVidBackgroundThread;
import thenotoriousrog.viddy.BreakingNewsViddy.BreakingNewsViddyActivity;
import thenotoriousrog.viddy.R;

/*
    * This class controls the cutting of a video as well as reporting it back to calling activity.
    * MainActivity and BreakingNewsViddyActivity are the only two activities that should be actually creating activities.
 */
public class TrimVideoFragment extends Fragment {

    AppCompatActivity instance; // this holds the instance to whatever calling activity has called the class.
    String videoPath; // the string form of the uri.
    String realPath; // the real path where the video is.
    View mainView; // the main view for the rest of the fragment.
    VideoView videoView;
    SeekBar seekBar; // used to control the range of the video.
    SeekBar minSeekBar;
    SeekBar maxSeekBar;
    Handler videoWatchHandler = new Handler(); // simply watches the video every second and makes sure that that the duration doesn't exceed what the user sets.
    TextView startTimeCounter;
    TextView endTimeCounter;
    int startTime = 0; // holds the start time for the user's video.
    int endTime = 0; // holds the end time for the user's video.
    String videoClipName; // the name of the clip that the user has chosen.
    Button cutVideoButton; // the button that will cut the video and show the the user that something is loading.
    LinearLayout loadingView; // the view that shows that something is loading so that the user isn't looking at nothing.

    /*
        * Sets the fields necessary for this fragment. We are also able to create the instance needed for this fragment as well as the video path for editing.
        * the instance is used to set the proper
     */
    public void setFields(AppCompatActivity instance, String videoPath, String realPath) {
        this.instance = instance;
        this.videoPath = videoPath;
        this.realPath = realPath;
    }

    // sets the video clip name usually when the user
    public void setVideoClipName(String videoClipName) {
        this.videoClipName = videoClipName;
    }

    public String getVideoClipName() {
        return videoClipName;
    }

    // this runnable checks the time of the video and makes sure that it doesn't exceed what the user set as the limit.
    Runnable videoTimeChecker = new Runnable() {
        @Override
        public void run() {

           // System.out.println("Current video position: " + videoView.getCurrentPosition()/1000);
           // System.out.println("Current selected end time: " + endTime);

            if(videoView.getCurrentPosition()/1000 >= endTime) { // the video has reached the new maximum time that the user has set.
                //videoView.pause();
                videoView.seekTo(startTime*1000); // restart the video view to the start time that the user set.
                if(seekBar != null) {
                    seekBar.setProgress(startTime);
                }
            }

            if (seekBar != null) {
                int progress = seekBar.getProgress()+1;
                seekBar.setProgress(progress);
            }

            videoWatchHandler.postDelayed(videoTimeChecker, 1000); // have the handler run this check once every second
        }
    };

    // used to display a warning to the user that a trimmed video will not be displayed since they have not trimmed their video!
    public boolean preventTrimFileName() {
        if(startTime == 0 && endTime == 0) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // restarts the video view to a seek time that the user wanted.
    private void restartVideoView(int startTime) {
        videoView.setOnPreparedListener(null); // remove the on prepared listener so that the seekbar's are not restarted.
        System.out.println("Restarting video with a startTime of " + startTime);
        videoView.stopPlayback();
        videoView.setVideoURI(Uri.parse(videoPath));
        videoView.start();
        videoView.seekTo(startTime*1000);
        seekBar.setProgress(0);
        seekBar.setMax(endTime - startTime); // the new max is the start time minus the end time.
        videoView.setOnCompletionListener(null); // remove the old completion listener.
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // stop playback, reset vid path, start the video again.
                videoView.stopPlayback();
                videoView.setVideoURI(Uri.parse(videoPath));
                videoView.start();
                videoView.seekTo(startTime*1000);
                seekBar.setProgress(0);
            }
        });

    }

    // converts time to real seconds in the background thread and updates the proper textView.
    private String convertToRealTime(int milliseconds) {

        long minutes = TimeUnit.MILLISECONDS.toMinutes(milliseconds);

        //System.out.println("The number of minutes in this video = " + minutes);

        long seconds = TimeUnit.MILLISECONDS.toSeconds(milliseconds) % 60; // get the number of seconds and mod by 60 to remove minutes.

        //System.out.println("The number of seconds in this video = " + seconds);

        if(seconds < 10) { // keeps a x:0x look for the seconds which looks better to me lol
            return minutes + ":" + 0 + seconds;
        }


            return minutes + ":" + seconds;

    }

    private void setupSeekbar() {

        seekBar.setMax(endTime);
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
    }

    // controls the behavior for when the users want to
    private void setupMinSeekbar() {

        minSeekBar.setMax( (videoView.getDuration()/1000)); // set the max to be the entire video duration in seconds minus 1.
        startTimeCounter.setText(convertToRealTime(startTime)); // set the start time to be 0 i.e. 0:0
        minSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            // update the new start variable above
            @Override
            public void onProgressChanged(SeekBar minSeekBar, int progress, boolean fromUser) {

                // ensure that the progress bar is always 1 second short of the end time.
                if ( progress >= endTime) {
                    startTime = endTime-1;
                    startTimeCounter.setText(convertToRealTime((progress*1000) - 1));
                    minSeekBar.setProgress(startTime);

                } else {
                   // if(progress != startTime) {
                        startTime = progress; // update the min as the user chooses.
                        startTimeCounter.setText(convertToRealTime(progress*1000));
                       // minSeekBar.setProgress(startTime);
                    //}

                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) { seekBar.requestFocus(); }

            // when the user lets go of the slider, update the video to reflect the new min.
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                restartVideoView(startTime); // restart the video with the new start time
                minSeekBar.setProgress(startTime);
            }
        });
    }

    private void setupMaxSeekbar() {

        maxSeekBar.setMax(videoView.getDuration()/1000); // set the max to be the entire video duration in seconds.
        endTime = videoView.getDuration()/1000;
        maxSeekBar.setProgress(endTime);
        endTimeCounter.setText(convertToRealTime(videoView.getDuration())); // set the starting end time to be of the real seconds in the video here.
        maxSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar maxSeekBar, int progress, boolean fromUser) {

                // the max cannot be less the current start time plus 1 otherwise we will set the endtime to be starttime + 1
                if(progress < startTime + 1) {
                    endTime = startTime + 1;
                    endTimeCounter.setText(convertToRealTime(endTime*1000));
                    maxSeekBar.setProgress(endTime);
                   // seekBar.setMax(endTime);
                } else {
                    endTime = progress;
                    endTimeCounter.setText(convertToRealTime(endTime*1000));
                    //maxSeekBar.setProgress(endTime);
                    //seekBar.setMax(endTime);
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar maxSeekBar) {
                maxSeekBar.requestFocus();
            }

            @Override
            public void onStopTrackingTouch(SeekBar maxSeekBar) {
                seekBar.setMax(endTime - startTime);
                maxSeekBar.setProgress(endTime);
            }
        });
    }

    private void setupCutVideoButton() {

        cutVideoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadingView.setVisibility(View.VISIBLE);

                // remove the click ability from the background view so that the user can't interact with the meme anymore.
                loadingView.setClickable(true);
                loadingView.setFocusable(true);

                if (startTime == 0 && endTime == videoView.getDuration()/1000) {

                    if(instance instanceof MainActivity) { // if an instance of main activity, skip background thread display whole video.
                        MainActivity mainActivity = (MainActivity) instance;
                        mainActivity.displayViddyStep2Fragment(null); // send a null file forcing the main activity to behave normally.
                        mainActivity.notifyFragmentIndicator(2); // show the 4th button to be highlighted instead of the 3rd button.
                    } else {
                        BreakingNewsViddyActivity bnvActivity = (BreakingNewsViddyActivity) instance;
                        bnvActivity.displayViddyStep2Fragment(null);
                        bnvActivity.notifyFragmentIndicator(2);
                    }
                } else { // if time is altered in any way, create and start the brackground thread.

                    if(instance instanceof MainActivity) {
                        System.out.println("Normal Viddy instance!!!!!");
                        ViddyTrimVidBackgroundThread trimBackgroundThread = new ViddyTrimVidBackgroundThread((MainActivity) instance, startTime*1000, endTime*1000, videoPath, videoClipName);
                        trimBackgroundThread.startBackgroundThead();
                    } else {
                        System.out.println("Breaking news Viddy instance!!!");
                        BNVTrimVidBackgroundThread trimBackgroundThread = new BNVTrimVidBackgroundThread((BreakingNewsViddyActivity) instance, startTime*1000,
                                endTime*1000, videoPath, videoClipName);
                        trimBackgroundThread.startBackgroundThead();
                    }

                }
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.vid_trimmer_layout, container, false); // inflate the vid trimmer layout.

        seekBar = mainView.findViewById(R.id.currentPlayingSeekbar);

        minSeekBar = mainView.findViewById(R.id.minSeekbar);
        maxSeekBar = mainView.findViewById(R.id.maxSeekbar);
        startTimeCounter = mainView.findViewById(R.id.startTimeCounter);
        endTimeCounter = mainView.findViewById(R.id.endTimeCounter);

        videoView = mainView.findViewById(R.id.videoViewTrim);
        videoView.setVideoURI(Uri.parse(videoPath));
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                videoView.start();
                TrimVideoFragment.this.setupMinSeekbar();
                TrimVideoFragment.this.setupMaxSeekbar();
                TrimVideoFragment.this.setupSeekbar(); // the seek bar for the user to control the video view.
                videoView.setOnPreparedListener(null); // immediately remove the on prepared listener after it's called once.
            }
        });
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                // stop playback, reset vid path, start the video again.
                videoView.stopPlayback();
                videoView.setVideoURI(Uri.parse(videoPath));
                videoView.start();
                seekBar.setProgress(0);
            }
        });

        videoWatchHandler.postDelayed(videoTimeChecker, 0); // start the handler right away!

        loadingView = mainView.findViewById(R.id.loadingView);
        cutVideoButton = mainView.findViewById(R.id.cutVideoButton);
        setupCutVideoButton();


        return mainView;
    }

}
