package thenotoriousrog.viddy.ViddyMeme;

import android.Manifest;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.reward.RewardItem;
import com.google.android.gms.ads.reward.RewardedVideoAd;
import com.google.android.gms.ads.reward.RewardedVideoAdListener;
import com.michaelflisar.gdprdialog.GDPR;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

import java.io.File;

import pub.devrel.easypermissions.EasyPermissions;
import pub.devrel.easypermissions.PermissionRequest;
import thenotoriousrog.viddy.Activities.MainActivity;
import thenotoriousrog.viddy.Backend.Constants;
import thenotoriousrog.viddy.Backend.ViddyUtils;
import thenotoriousrog.viddy.R;

public class ViddyStep2Fragment extends Fragment {

    private MainActivity mainActivity; // the main activity that we are using.
    private String videoPath = ""; // the video Uri passed in from MainActivity.
    private String memeText = ""; // this is the text of the veme that the user has written
    private String realPath = ""; // the real system path of the video itself.
    private String customWatermark; // holds the text of the custom watermark that the user will be using.
    private DiscreteSeekBar textIncreaser; // the slider that the user will use to increase the text of the veme
    private TextView vemeText; // the text preview.
    private TextView customWatermarkTextView; // the text for the custom watermark.
    private TextView customWatermarkTextPlaceholder; // just holds the same look as the edit text field, but is only clickable.
    private TextInputEditText customWatermarkText; // the field that the user can add text too.
    private int textSizeVal = 10; // the starting value is always 10
    private boolean removeWatermarkAdWatched = false; // this will be set true upon watching the ad then they can add their watermark!
    private boolean addCustomWatermarkAdWatched = false; // tells us if the user has watched an add to add their own custom watermark!
    private RewardedVideoAd rewardedVideoAd; // the add that the user can watch in order to unlock features within this activity.
    private View mainView;
    private VideoView videoView;
    private boolean readyForStep3 = false; // tells the fragment that we are ready to go to step 3.
    private boolean isRewarded = false; // tells the system if the user has finished watching an ad or not.
    private String filename = null; // the filename that the user chooses.

    private void setupDiscreteSeekBar(View mainView)
    {
        textIncreaser = mainView.findViewById(R.id.textSizeIncreaser);
        textIncreaser.setScrubberColor(ContextCompat.getColor(getContext(), R.color.colorComplimentary));
        textIncreaser.setThumbColor(ContextCompat.getColor(getContext(), R.color.colorComplimentary), ContextCompat.getColor(getContext(), R.color.colorComplimentary));
        textIncreaser.setOnProgressChangeListener(new DiscreteSeekBar.OnProgressChangeListener() {

            // updates the value of the text as we begin sliding for the user.
            @Override
            public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser)
            {
                textSizeVal = value; // set the size of the text the user has chosen
                vemeText.setTextSize(TypedValue.COMPLEX_UNIT_SP, value); // set the text size based on the value.
            }

            @Override
            public void onStartTrackingTouch(DiscreteSeekBar seekBar) { }

            @Override
            public void onStopTrackingTouch(DiscreteSeekBar seekBar) { }
        });
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public void setFields(MainActivity mainActivity, String videoPath, String realPath) {
        this.mainActivity = mainActivity;
        this.videoPath = videoPath;
        this.realPath = realPath;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    // sets up the listener to be able to watch the rewarded video ad.
    private void setupRewardedVideoAd(View mainView)
    {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getContext()); // get the context that we need in order to sort these lists.
        SharedPreferences.Editor editor = preferences.edit();

        rewardedVideoAd = MobileAds.getRewardedVideoAdInstance(getContext()); // set our ad.
        rewardedVideoAd.setRewardedVideoAdListener(new RewardedVideoAdListener() {
            @Override
            public void onRewarded(RewardItem reward) {

                isRewarded = true;
            }

            @Override
            public void onRewardedVideoAdLeftApplication() {

            }

            @Override
            public void onRewardedVideoAdClosed() {
                // Toast.makeText(getBaseContext(), "onRewardedVideoAdClosed", Toast.LENGTH_SHORT).show();

                if(isRewarded) {
                    Bundle args = new Bundle();
                    args.putString("VideoPath", videoPath); // the path of the video that the user has selected.
                    args.putString("RealPath", realPath);
                    if(memeText == null) {
                        memeText = ""; // prevents a crash if a user doesn't add any text.
                    }
                    args.putString("VemeText", memeText); // put the meme text that the user has written.
                    args.putString("CustomWatermark", customWatermark); // send the custom watermark to the next activity.
                    args.putBoolean("WithWatermark", false); // we are going to make the meme without a watermark!
                    args.putInt("TextSize", textSizeVal);
                    args.putString("filename", filename); // send in the filename that the user chose for their meme.

                    editor.putString("VemeText", memeText);
                    editor.putString("CustomWatermark", customWatermark);
                    editor.putBoolean("WithWatermark", false);
                    editor.putInt("TextSize", textSizeVal);
                    editor.putString("filename", filename);
                    editor.apply();

                    Toast toast = new Toast(getContext());
                    View toastView = getLayoutInflater().inflate(R.layout.watermark_not_added_toast, (ViewGroup) mainView.findViewById(R.id.watermarkNotAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
                    toast.setView(toastView);
                    toast.show();

                    ViddyStep3Fragment viddyStep3Fragment = new ViddyStep3Fragment();
                    viddyStep3Fragment.setFields(mainActivity, videoPath, realPath, memeText, customWatermark, textSizeVal, false, filename);
                    FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                    transaction.replace(R.id.mainFrameLayout, viddyStep3Fragment);
                    transaction.commit();
                    mainActivity.notifyFragmentIndicator(3); // make the third icon lightup.
                    mainActivity.hideEditFilenameIcon();
                    mainActivity.invalidateOptionsMenu(); // force the UI to redraw.
                }

            }

            @Override
            public void onRewardedVideoAdFailedToLoad(int errorCode) {
                CoordinatorLayout snackbarLocation = mainView.findViewById(R.id.snackbarLocation);
                Snackbar snackbar = Snackbar.make(snackbarLocation, R.string.Ad_Not_Loading, Snackbar.LENGTH_SHORT);
                View sbView = snackbar.getView();
                sbView.setBackgroundColor(ViddyUtils.getInstance().getColor(getContext(), R.color.colorComplimentary));
                sbView.setElevation(5);
                snackbar.show();
            }

            // listens for when the ad is loaded.
            @Override
            public void onRewardedVideoAdLoaded() {
                rewardedVideoAd.show();
            }

            @Override
            public void onRewardedVideoAdOpened() {
                isRewarded = true; // reward the user for opening the ad.
            }

            @Override
            public void onRewardedVideoStarted() { }

            @Override
            public void onRewardedVideoCompleted() {
                if(removeWatermarkAdWatched) { // if this is true and the user finishes the ad we can proceed to step 3.
                    readyForStep3 = true;
                }
            }
        });
    }

    private void setupStep3Button(View mainView, String userConsent) {
        final Button step3Button = mainView.findViewById(R.id.step3Button);
        step3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                // if they choose no, display a full screen ad, after the ad the activity will generate the meme without a watermark.
                // if they choose to keep the watermark then immediately generate the meme with the watermark of the app. Very important!

                String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};

                if(EasyPermissions.hasPermissions(getContext(), perms)) // permissions available, proceed as normal.
                {

                    //Intent step3Intent = new Intent(Step2Activity.this, Step3Activity.class);
                    Bundle args = new Bundle();
                    args.putString("VideoPath", videoPath); // the path of the video that the user has selected.
                    args.putString("RealPath", realPath);
                    if(memeText == null) {
                        memeText = ""; // prevents crash if a user doesn't add any text
                    }
                    args.putString("VemeText", memeText); // put the meme text that the user has written.
                    args.putString("CustomWatermark", customWatermark); // send a null watermark so we know not to add the user's custom watermark.
                    args.putBoolean("WithWatermark", true); // make the meme with the watermark.
                    args.putInt("TextSize", textSizeVal);
                    args.putString("filename", filename); // send in the filename that the user chose for their meme.

                    //step3Intent.putExtras(args); // put the arguments for the activity.

                    // displays our custom toast view which is awesome.
//                    Toast toast = new Toast(getContext());
//                    View toastView = getLayoutInflater().inflate(R.layout.added_watermark_toast, (ViewGroup) mainView.findViewById(R.id.watermarkAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
//                    toast.setView(toastView);
//                    toast.show();

                    // replace this fragment with the new fragment.
                    ViddyStep3Fragment viddyStep3Fragment = new ViddyStep3Fragment();
                    viddyStep3Fragment.setFields(mainActivity, videoPath, realPath, memeText, customWatermark, textSizeVal, false, filename);
                    FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.mainFrameLayout, viddyStep3Fragment);
                    transaction.commit();
                    mainActivity.notifyFragmentIndicator(3); // make the third icon lightup.
                    mainActivity.hideEditFilenameIcon();
                    mainActivity.invalidateOptionsMenu(); // force the UI to redraw.

//                    // builds the dialog for the application itself.
//                    new MaterialDialog.Builder(getContext())
//                            .title(R.string.No_Watermark_Dialog_Title)
//                            .content(R.string.No_Watermark_Dialog_Content)
//                            .positiveText(R.string.No_Watermark_Dialog_Yes)
//                            .onPositive(new MaterialDialog.SingleButtonCallback() {
//
//                                // on click starts the new activity without the watermark.
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
//                                {
//                                    // todo: make the call to the main activity to show the step 3 fragment. Very very very important.
//                                    videoView.stopPlayback(); // stop playback since the add is starting
//
//                                    // uncomment the line below to use our real ad.
//                                    //removeWatermarkAd.loadAd("ca-app-pub-1014366431079942/9380662273", new AdRequest.Builder().build());//new AdRequest.Builder().build()); // display the remove watermark ad.
//                                    removeWatermarkAdWatched = true; // the user has chosen to watch the remove watermark feature for the app itself.
//                                    if (userConsent.equalsIgnoreCase(Constants.GDPR_CONSENT.NON_PERSONAL_CONSENT)) { // no consent, only show non personalized ads.
//                                        Bundle extras = new Bundle();
//                                        extras.putString("npa", "1");
//
//                                        // build request with the report to admob that they must not be personalized.
//                                        AdRequest request = new AdRequest.Builder()
//                                                .addNetworkExtrasBundle(AdMobAdapter.class, extras)
//                                                .build();
//
//                                        rewardedVideoAd.loadAd("ca-app-pub-1014366431079942/9380662273", request); // video ad test, without personal ads.
//                                    }
//                                    else { // the user must have chosen to use personal ads or it was automatically determined by the user.
//                                        rewardedVideoAd.loadAd("ca-app-pub-1014366431079942/9380662273", new AdRequest.Builder().build()); // video ad test, with personal ads.
//                                    }
//                                }
//                            })
//                            .negativeText(R.string.No_Watermark_Dialog_No)
//                            .onNegative(new MaterialDialog.SingleButtonCallback() {
//
//                                // starts the new activity with the watermark.
//                                @Override
//                                public void onClick(@NonNull MaterialDialog dialog, @NonNull DialogAction which)
//                                {
//                                    //Intent step3Intent = new Intent(Step2Activity.this, Step3Activity.class);
//                                    Bundle args = new Bundle();
//                                    args.putString("VideoPath", videoPath); // the path of the video that the user has selected.
//                                    args.putString("RealPath", realPath);
//                                    if(memeText == null) {
//                                        memeText = ""; // prevents crash if a user doesn't add any text
//                                    }
//                                    args.putString("VemeText", memeText); // put the meme text that the user has written.
//                                    args.putString("CustomWatermark", customWatermark); // send a null watermark so we know not to add the user's custom watermark.
//                                    args.putBoolean("WithWatermark", true); // make the meme with the watermark.
//                                    args.putInt("TextSize", textSizeVal);
//                                    args.putString("filename", filename); // send in the filename that the user chose for their meme.
//
//                                    //step3Intent.putExtras(args); // put the arguments for the activity.
//
//                                    // displays our custom toast view which is awesome.
//                                    Toast toast = new Toast(getContext());
//                                    View toastView = getLayoutInflater().inflate(R.layout.added_watermark_toast, (ViewGroup) mainView.findViewById(R.id.watermarkAddedToast) );//mainLayout.findViewById(R.id.watermarkAddedToast);
//                                    toast.setView(toastView);
//                                    toast.show();
//
//                                    // replace this fragment with the new fragment.
//                                    ViddyStep3Fragment viddyStep3Fragment = new ViddyStep3Fragment();
//                                    viddyStep3Fragment.setFields(mainActivity, videoPath, realPath, memeText, customWatermark, textSizeVal, true, filename);
//                                    FragmentTransaction transaction = mainActivity.getSupportFragmentManager().beginTransaction();
//                                    transaction.replace(R.id.mainFrameLayout, viddyStep3Fragment);
//                                    transaction.commit();
//                                    mainActivity.notifyFragmentIndicator(3); // make the third icon lightup.
//                                    mainActivity.hideEditFilenameIcon();
//                                    mainActivity.invalidateOptionsMenu(); // force the UI to redraw.
//
//                                    //startActivity(step3Intent); // starts the activity.
//                                }
//                            })
//                            .show(); // displays the the material dialog.
                } else { // permissions not granted, we need to get them as soon as possible.

                    String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}; // just need to read/write storage android.
                    EasyPermissions.requestPermissions(
                            new PermissionRequest.Builder(mainActivity, Constants.ACTION.REQUEST_PERMISSIONS, permissions)
                                    .setRationale(R.string.PermissionsRationale)
                                    .setPositiveButtonText(R.string.PermissionRationaleOk)
                                    .setNegativeButtonText(R.string.PermissionRationaleCancel)
                                    .build());
                }
            }
        });
    }

    private void setupCustomWatermark() {
        // add text to the proper custom watermark view.
        customWatermarkText.addTextChangedListener(new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            // the actual text magic happens now.
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                customWatermark = s.toString(); // add to the string we will be sending to the user.
                customWatermarkTextView.setText(s.toString()); // show the watermark on the viddy.
            }

            @Override
            public void afterTextChanged(Editable s) { }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.viddy_step2fragment, container, false); // inflate step1 fragment
        this.mainView = mainView;

        String userConsent = GDPR.getInstance().getConsentState().getConsent().name(); // get the consent type from the user.

        setupDiscreteSeekBar(mainView);
        setupRewardedVideoAd(mainView);
        setupStep3Button(mainView, userConsent);

        videoView = mainView.findViewById(R.id.videoViewStep2);
        videoView.setVideoURI(Uri.parse(videoPath));
        videoView.start(); // start the video.
        videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            // is in charge of restarting the video over and over again.
            @Override
            public void onCompletion(MediaPlayer mp)
            {
                // stop playback, reset vid path, start the video again.
                videoView.stopPlayback(); // stop the playback if completed.
                // videoView.setVideoURI(Uri.parse(videoPath));
                videoView.setVideoURI(Uri.fromFile(new File(videoPath)));
                videoView.setVideoPath(videoPath);
                videoView.start();
            }
        });

        vemeText = mainView.findViewById(R.id.vemeTextStep2); // the text where the user will edit their text for the meme.
        customWatermarkTextView = mainView.findViewById(R.id.customWatermarkText); // the text that the user's watermark will go.
        customWatermarkText = mainView.findViewById(R.id.customWatermark); // the custom watermark text that we are working on.
        setupCustomWatermark();


        TextInputEditText editText = mainView.findViewById(R.id.editMemeText); // this is where the user will type.
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {} // not needed, auto-generated.

            @Override
            public void afterTextChanged(Editable s) {} // not needed, auto-generated

            // listens for text as its being entered
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count)
            {
                memeText = s.toString(); // update the veme text.

                if(vemeText.getLineCount() != 4)
                {
                    vemeText.setText(s.toString()); // set the text as the user updates it.
                }
            }
        });

        return mainView;
    }

}
